package de.uniks.abalone.gui;

import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * The {@code RulesDialog} gives a short summary of the game rules and how the program can be used.
 */
public class RulesDialog extends Dialog<Object> {
    public RulesDialog() {
        // Init stage
        Image icon = new Image(this.getClass().getResource("/gfx/rules.png").toString());
        Stage stage = (Stage) this.getDialogPane().getScene().getWindow();
        stage.getIcons().add(icon);
        this.setTitle("Rules");
        this.getDialogPane().getButtonTypes().add(ButtonType.OK);

        // Build content
        ScrollPane rootPane = new ScrollPane();
        rootPane.setPrefWidth(450);
        rootPane.setPrefHeight(350);
        rootPane.setHbarPolicy(ScrollBarPolicy.NEVER);
        rootPane.setVbarPolicy(ScrollBarPolicy.ALWAYS);

        VBox textBox = new VBox();
        textBox.maxWidthProperty().bind(rootPane.widthProperty().subtract(30));
        textBox.minWidthProperty().bind(rootPane.widthProperty().subtract(30));
        textBox.setSpacing(5);

        // Style
        String head = "-fx-font-size: 16; -fx-font-weight: bold";
        String body = "-fx-font-size: 12";

        // Rules
        Label rulesHeading = new Label("Game Rules");
        rulesHeading.setStyle(head);

        Label rules1 = new Label("Abalone is a strategic board game for two players. "
                + "Each player starts with 14 balls on a hexagonal board. The goal of "
                + "Abalone is to push six enemy balls over the edge of the board.");
        rules1.setStyle(body);
        rules1.setWrapText(true);

        Label rules2 = new Label("When it is a player's turn, he can move up to three"
                + " of his own balls in one of the six directions. He can either move"
                + " the balls one field along the line or shift the whole line parallel"
                + " to adjacent fields. In the last case the target fields have to be empty.");
        rules2.setStyle(body);
        rules2.setWrapText(true);

        Label rules3 = new Label("When the player moves his balls along their alignment he"
                + " can also move enemy balls. However in this case he must have the majority."
                + " For example, if he wants to move one enemy ball, he needs at least two of"
                + " his own. And for two enemy balls he needs three of his own balls.");
        rules3.setStyle(body);
        rules3.setWrapText(true);

        // How to play
        Label howToPlayHeading = new Label("How to play");
        howToPlayHeading.setStyle(head);

        Label howToPlay1 = new Label("At first a player must select the balls which he wants"
                + " to move. He can only select such combinations, that are allowed to be moved"
                + " together. Afterwards he drags the selected balls in the desired direction."
                + " If this was a valid move, it will be executed and it is the next player's turn.");
        howToPlay1.setStyle(body);
        howToPlay1.setWrapText(true);

        Label howToPlay2 = new Label("In the 'Game' menu the current game can be restarted or a player"
                + " can start a completely new one. When the active player is not an AI it is possible"
                + " to save the game for a later continuation. Saved games can be loaded from the start"
                + " screen. At last a player can exit the game. Closing the game screen will show the start screen again.");
        howToPlay2.setStyle(body);
        howToPlay2.setWrapText(true);

        Label howToPlay3 = new Label("In the 'Settings' menu a player can open the game rules again or"
                + " inform himself about the program. He also can change each player's color and/or"
                + " the strength of eventual AIs.");
        howToPlay3.setStyle(body);
        howToPlay3.setWrapText(true);

        // The AIs
        Label aiHeading = new Label("The AI modes");
        aiHeading.setStyle(head);

        Label ai1 = new Label("The MiniMax-AI just calculates every possible move with every possible enemy"
                + " reaction. The level of this AI is the number of moves which it computes in advance.");
        ai1.setStyle(body);
        ai1.setWrapText(true);

        Label ai2 = new Label("The AlphaBeta-AI also calculates every possible move and all the enemy"
                + " reactions. Due to some clever extensions, it can drastically reduce the total number of"
                + " moves which are necessary for the best result. Similar to MiniMax the level is the number"
                + " of moves which can be computed in advance.");
        ai2.setStyle(body);
        ai2.setWrapText(true);

        Label ai3 = new Label("The Iterative Deepening-AI uses breadth-first instead of depth-first"
                + " search to calculate the best move. The time determines the limit for every turn until the"
                + " AI has to come up with a result. By pressing the space bar, a player can interrupt the AI at an earlier point.");
        ai3.setStyle(body);
        ai3.setWrapText(true);

        textBox.getChildren().addAll(rulesHeading, rules1, rules2, rules3, howToPlayHeading, howToPlay1, howToPlay2,
                howToPlay3, aiHeading, ai1, ai2, ai3);

        rootPane.setContent(textBox);

        this.getDialogPane().setContent(rootPane);
    }
}