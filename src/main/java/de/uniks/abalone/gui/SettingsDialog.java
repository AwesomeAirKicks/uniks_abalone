package de.uniks.abalone.gui;

import java.util.ArrayList;
import java.util.Arrays;

import de.uniks.abalone.model.AIPlayer;
import de.uniks.abalone.model.Board;
import de.uniks.abalone.model.Logic;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Player;
import de.uniks.ai.IterativeDeepening;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * With the {@code SettingsDialog} it is possible to change the color of the players and the strength of eventual AIs.
 */
public class SettingsDialog extends Dialog<ArrayList<String>> {
    private ColorPicker colorP1 = new ColorPicker();

    private ColorPicker colorP2 = new ColorPicker();

    private Slider strengthP1 = new Slider();

    private Slider strengthP2 = new Slider();

    public SettingsDialog(Player... players) {
        Player player1 = players[0];
        Player player2 = players[1];

        // Initialize the dialog
        this.setTitle("Settings");
        Image icon = new Image(this.getClass().getResource("/gfx/settings.png").toString());
        Stage stage = (Stage) this.getDialogPane().getScene().getWindow();
        stage.getIcons().add(icon);
        ButtonType confirm = new ButtonType("OK", ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
        this.getDialogPane().getButtonTypes().addAll(confirm, cancel);

        // Build the content
        VBox rootBox = new VBox();
        rootBox.setAlignment(Pos.CENTER);
        rootBox.setPadding(new Insets(10));
        rootBox.setSpacing(10);

        for(Player player: players) {
            VBox playerArea = new VBox();
            playerArea.setAlignment(Pos.TOP_CENTER);
            playerArea.setPadding(new Insets(5, 5, 5, 5));
            playerArea.setSpacing(7);
            playerArea.setPrefWidth(250);
            playerArea.setStyle("-fx-border-color: black; -fx-border-width: 2");

            // Name
            Label nameLabel = new Label(player.getName());
            nameLabel.setStyle("-fx-font-size: 24; -fx-font-weight: bold");

            // Color
            HBox colorArea = new HBox();
            colorArea.setAlignment(Pos.CENTER_LEFT);

            Label colorLabel = new Label("Color");
            HBox.setMargin(colorLabel, new Insets(0, 5, 0, 5));
            colorLabel.setPadding(new Insets(5));

            ColorPicker color = player.equals(player1) ? this.colorP1 : this.colorP2;
            color.setPrefWidth(178);
            color.setValue(Color.web(player.getColor()));

            colorArea.getChildren().addAll(colorLabel, color);

            playerArea.getChildren().addAll(nameLabel, colorArea);

            // Strength (only for AI players)
            if(player instanceof AIPlayer) {
                HBox strengthArea = new HBox();
                strengthArea.setAlignment(Pos.CENTER_LEFT);

                Label strengthLabel = new Label("Level");
                HBox.setMargin(strengthLabel, new Insets(0, 5, 0, 5));
                strengthLabel.setPadding(new Insets(5));

                Slider strength = player.equals(player1) ? this.strengthP1 : this.strengthP2;
                strength.setMin(1);
                strength.setMax(10);
                strength.setBlockIncrement(1);
                strength.setMajorTickUnit(1);
                strength.setMinorTickCount(1);
                strength.setSnapToTicks(true);
                strength.setValue(((AIPlayer) player).getStrength());

                Label strengthValue = new Label(Integer.toString(((AIPlayer) player).getStrength()));
                strengthValue.setAlignment(Pos.CENTER_RIGHT);
                HBox.setMargin(strengthValue, new Insets(0, 10, 0, 10));

                // Interpret the strength value as timeLimit, if the AI is an instance of IterativeD
                if(((AIPlayer) player).getAi() instanceof IterativeDeepening) {
                    strengthLabel.setText("Time");
                    long timeLimit = ((IterativeDeepening<Logic, Board, Move, Player>) ((AIPlayer) player).getAi())
                            .getTimeLimit();
                    strength.setValue(timeLimit / 1000);
                    strengthValue.setText(Long.toString(timeLimit / 1000) + "s");
                    strength.setMax(30);
                    strength.setMin(0);
                }
                strength.valueProperty().addListener((ChangeListener<Number>) (observable, oldValue, newValue) -> {
                    int value = newValue.intValue();
                    strengthValue.setText(Integer.toString(value)
                            + ((((AIPlayer) player).getAi() instanceof IterativeDeepening) ? "s" : ""));
                });
                strengthArea.getChildren().addAll(strengthLabel, strength, strengthValue);
                playerArea.getChildren().add(strengthArea);
            }
            rootBox.getChildren().add(playerArea);
        }
        this.getDialogPane().setContent(rootBox);

        // Define the result type
        this.setResultConverter(dialogButton -> {
            if(dialogButton == confirm) {
                String newColor1 = this.colorP1.getValue().toString();
                String newColor2 = this.colorP2.getValue().toString();
                int temp1 = (int) this.strengthP1.getValue();
                int temp2 = (int) this.strengthP2.getValue();
                String newStrength1 = player1 instanceof AIPlayer ? Integer.toString(temp1) : "-1";
                String newStrength2 = player2 instanceof AIPlayer ? Integer.toString(temp2) : "-1";
                return new ArrayList<>(Arrays.asList(newColor1, newStrength1, newColor2, newStrength2));
            } else {
                return null;
            }
        });
    }
}