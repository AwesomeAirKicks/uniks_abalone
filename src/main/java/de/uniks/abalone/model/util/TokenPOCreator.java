package de.uniks.abalone.model.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;

import de.uniks.abalone.model.Token;
import de.uniks.networkparser.IdMap;

public class TokenPOCreator extends PatternObjectCreator {
    @Override
    public Object getSendableInstance(boolean reference) {
        if(reference) {
            return new TokenPO(new Token[] {});
        } else {
            return new TokenPO();
        }
    }

    public static IdMap createIdMap(String sessionID) {
        return de.uniks.abalone.model.util.CreatorCreator.createIdMap(sessionID);
    }
}
