package de.uniks.abalone.model.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;

import de.uniks.abalone.model.Board;
import de.uniks.networkparser.IdMap;

public class BoardPOCreator extends PatternObjectCreator {
    @Override
    public Object getSendableInstance(boolean reference) {
        if(reference) {
            return new BoardPO(new Board[] {});
        } else {
            return new BoardPO();
        }
    }

    public static IdMap createIdMap(String sessionID) {
        return de.uniks.abalone.model.util.CreatorCreator.createIdMap(sessionID);
    }
}
