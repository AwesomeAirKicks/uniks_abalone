/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model.util;

import java.util.Collection;
import java.util.Collections;

import de.uniks.abalone.model.AIPlayer;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;
import de.uniks.networkparser.interfaces.Condition;
import de.uniks.networkparser.list.NumberList;
import de.uniks.networkparser.list.ObjectSet;
import de.uniks.networkparser.list.SimpleSet;
import de.uniks.abalone.model.util.GameSet;
import de.uniks.abalone.model.util.TokenSet;

public class PlayerSet extends SimpleSet<Player> {
    @Override
    protected Class<?> getTypClass() {
        return Player.class;
    }

    public PlayerSet() {
        // empty
    }

    public PlayerSet(Player... objects) {
        for(Player obj: objects) {
            this.add(obj);
        }
    }

    public PlayerSet(Collection<Player> objects) {
        this.addAll(objects);
    }

    public static final PlayerSet EMPTY_SET = new PlayerSet().withFlag(PlayerSet.READONLY);

    public PlayerPO createPlayerPO() {
        return new PlayerPO(this.toArray(new Player[size()]));
    }

    public String getEntryType() {
        return "de.uniks.abalone.model.Player";
    }

    @Override
    public PlayerSet getNewList(boolean keyValue) {
        return new PlayerSet();
    }

    @Override
    public PlayerSet filter(Condition<Player> condition) {
        PlayerSet filterList = new PlayerSet();
        filterItems(filterList, condition);
        return filterList;
    }

    @SuppressWarnings("unchecked")
    public PlayerSet with(Object value) {
        if(value == null) {
            return this;
        } else if(value instanceof java.util.Collection) {
            this.addAll((Collection<Player>) value);
        } else if(value != null) {
            this.add((Player) value);
        }
        return this;
    }

    public PlayerSet without(Player value) {
        this.remove(value);
        return this;
    }

    /**
     * Loop through the current set of Player objects and collect a list of the name attribute values.
     *
     * @return List of String objects reachable via name attribute
     */
    public ObjectSet getName() {
        ObjectSet result = new ObjectSet();
        for(Player obj: this) {
            result.add(obj.getName());
        }
        return result;
    }

    /**
     * Loop through the current set of Player objects and collect those Player objects where the name attribute matches
     * the parameter value.
     *
     * @param value Search value
     *
     * @return Subset of Player objects that match the parameter
     */
    public PlayerSet filterName(String value) {
        PlayerSet result = new PlayerSet();
        for(Player obj: this) {
            if(value.equals(obj.getName())) {
                result.add(obj);
            }
        }
        return result;
    }

    /**
     * Loop through the current set of Player objects and collect those Player objects where the name attribute is
     * between lower and upper.
     *
     * @param lower Lower bound
     * @param upper Upper bound
     *
     * @return Subset of Player objects that match the parameter
     */
    public PlayerSet filterName(String lower, String upper) {
        PlayerSet result = new PlayerSet();
        for(Player obj: this) {
            if(lower.compareTo(obj.getName()) <= 0 && obj.getName().compareTo(upper) <= 0) {
                result.add(obj);
            }
        }
        return result;
    }

    /**
     * Loop through the current set of Player objects and assign value to the name attribute of each of it.
     *
     * @param value New attribute value
     *
     * @return Current set of Player objects now with new attribute values.
     */
    public PlayerSet withName(String value) {
        for(Player obj: this) {
            obj.setName(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Player objects and collect a list of the color attribute values.
     *
     * @return List of String objects reachable via color attribute
     */
    public ObjectSet getColor() {
        ObjectSet result = new ObjectSet();
        for(Player obj: this) {
            result.add(obj.getColor());
        }
        return result;
    }

    /**
     * Loop through the current set of Player objects and collect those Player objects where the color attribute matches
     * the parameter value.
     *
     * @param value Search value
     *
     * @return Subset of Player objects that match the parameter
     */
    public PlayerSet filterColor(String value) {
        PlayerSet result = new PlayerSet();
        for(Player obj: this) {
            if(value.equals(obj.getColor())) {
                result.add(obj);
            }
        }
        return result;
    }

    /**
     * Loop through the current set of Player objects and collect those Player objects where the color attribute is
     * between lower and upper.
     *
     * @param lower Lower bound
     * @param upper Upper bound
     *
     * @return Subset of Player objects that match the parameter
     */
    public PlayerSet filterColor(String lower, String upper) {
        PlayerSet result = new PlayerSet();
        for(Player obj: this) {
            if(lower.compareTo(obj.getColor()) <= 0 && obj.getColor().compareTo(upper) <= 0) {
                result.add(obj);
            }
        }
        return result;
    }

    /**
     * Loop through the current set of Player objects and assign value to the color attribute of each of it.
     *
     * @param value New attribute value
     *
     * @return Current set of Player objects now with new attribute values.
     */
    public PlayerSet withColor(String value) {
        for(Player obj: this) {
            obj.setColor(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Player objects and collect a list of the scores attribute values.
     *
     * @return List of int objects reachable via scores attribute
     */
    public NumberList getScores() {
        NumberList result = new NumberList();
        for(Player obj: this) {
            result.add(obj.getScores());
        }
        return result;
    }

    /**
     * Loop through the current set of Player objects and collect those Player objects where the scores attribute
     * matches the parameter value.
     *
     * @param value Search value
     *
     * @return Subset of Player objects that match the parameter
     */
    public PlayerSet filterScores(int value) {
        PlayerSet result = new PlayerSet();
        for(Player obj: this) {
            if(value == obj.getScores()) {
                result.add(obj);
            }
        }
        return result;
    }

    /**
     * Loop through the current set of Player objects and collect those Player objects where the scores attribute is
     * between lower and upper.
     *
     * @param lower Lower bound
     * @param upper Upper bound
     *
     * @return Subset of Player objects that match the parameter
     */
    public PlayerSet filterScores(int lower, int upper) {
        PlayerSet result = new PlayerSet();
        for(Player obj: this) {
            if(lower <= obj.getScores() && obj.getScores() <= upper) {
                result.add(obj);
            }
        }
        return result;
    }

    /**
     * Loop through the current set of Player objects and assign value to the scores attribute of each of it.
     *
     * @param value New attribute value
     *
     * @return Current set of Player objects now with new attribute values.
     */
    public PlayerSet withScores(int value) {
        for(Player obj: this) {
            obj.setScores(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Player objects and collect a set of the Game objects reached via game.
     *
     * @return Set of Game objects reachable via game
     */
    public GameSet getGame() {
        GameSet result = new GameSet();
        for(Player obj: this) {
            result.with(obj.getGame());
        }
        return result;
    }

    /**
     * Loop through the current set of Player objects and collect all contained objects with reference game pointing to
     * the object passed as parameter.
     *
     * @param value The object required as game neighbor of the collected results.
     *
     * @return Set of Game objects referring to value via game
     */
    public PlayerSet filterGame(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        PlayerSet answer = new PlayerSet();
        for(Player obj: this) {
            if(neighbors.contains(obj.getGame()) || (neighbors.isEmpty() && obj.getGame() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Player object passed as parameter to the Game
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their Game attributes.
     */
    public PlayerSet withGame(Game value) {
        for(Player obj: this) {
            obj.withGame(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Player objects and collect a set of the Game objects reached via currentGame.
     *
     * @return Set of Game objects reachable via currentGame
     */
    public GameSet getCurrentGame() {
        GameSet result = new GameSet();
        for(Player obj: this) {
            result.with(obj.getCurrentGame());
        }
        return result;
    }

    /**
     * Loop through the current set of Player objects and collect all contained objects with reference currentGame
     * pointing to the object passed as parameter.
     *
     * @param value The object required as currentGame neighbor of the collected results.
     *
     * @return Set of Game objects referring to value via currentGame
     */
    public PlayerSet filterCurrentGame(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        PlayerSet answer = new PlayerSet();
        for(Player obj: this) {
            if(neighbors.contains(obj.getCurrentGame()) || (neighbors.isEmpty() && obj.getCurrentGame() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Player object passed as parameter to the CurrentGame
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their CurrentGame attributes.
     */
    public PlayerSet withCurrentGame(Game value) {
        for(Player obj: this) {
            obj.withCurrentGame(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Player objects and collect a set of the Game objects reached via wonGame.
     *
     * @return Set of Game objects reachable via wonGame
     */
    public GameSet getWonGame() {
        GameSet result = new GameSet();
        for(Player obj: this) {
            result.with(obj.getWonGame());
        }
        return result;
    }

    /**
     * Loop through the current set of Player objects and collect all contained objects with reference wonGame pointing
     * to the object passed as parameter.
     *
     * @param value The object required as wonGame neighbor of the collected results.
     *
     * @return Set of Game objects referring to value via wonGame
     */
    public PlayerSet filterWonGame(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        PlayerSet answer = new PlayerSet();
        for(Player obj: this) {
            if(neighbors.contains(obj.getWonGame()) || (neighbors.isEmpty() && obj.getWonGame() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Player object passed as parameter to the WonGame
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their WonGame attributes.
     */
    public PlayerSet withWonGame(Game value) {
        for(Player obj: this) {
            obj.withWonGame(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Player objects and collect a set of the Token objects reached via tokens.
     *
     * @return Set of Token objects reachable via tokens
     */
    public TokenSet getTokens() {
        TokenSet result = new TokenSet();
        for(Player obj: this) {
            result.with(obj.getTokens());
        }
        return result;
    }

    /**
     * Loop through the current set of Player objects and collect all contained objects with reference tokens pointing
     * to the object passed as parameter.
     *
     * @param value The object required as tokens neighbor of the collected results.
     *
     * @return Set of Token objects referring to value via tokens
     */
    public PlayerSet filterTokens(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        PlayerSet answer = new PlayerSet();
        for(Player obj: this) {
            if(!Collections.disjoint(neighbors, obj.getTokens())) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Player object passed as parameter to the Tokens
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their Tokens attributes.
     */
    public PlayerSet withTokens(Token value) {
        for(Player obj: this) {
            obj.withTokens(value);
        }
        return this;
    }

    /**
     * Loop through current set of ModelType objects and remove the Player object passed as parameter from the Tokens
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now without the old neighbor.
     */
    public PlayerSet withoutTokens(Token value) {
        for(Player obj: this) {
            obj.withoutTokens(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Player objects and collect a set of the Player objects reached via previous.
     *
     * @return Set of Player objects reachable via previous
     */
    public PlayerSet getPrevious() {
        PlayerSet result = new PlayerSet();
        for(Player obj: this) {
            result.with(obj.getPrevious());
        }
        return result;
    }

    /**
     * Loop through the current set of Player objects and collect all contained objects with reference previous pointing
     * to the object passed as parameter.
     *
     * @param value The object required as previous neighbor of the collected results.
     *
     * @return Set of Player objects referring to value via previous
     */
    public PlayerSet filterPrevious(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        PlayerSet answer = new PlayerSet();
        for(Player obj: this) {
            if(neighbors.contains(obj.getPrevious()) || (neighbors.isEmpty() && obj.getPrevious() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Follow previous reference zero or more times and collect all reachable objects. Detect cycles and deal with them.
     *
     * @return Set of Player objects reachable via previous transitively (including the start set)
     */
    public PlayerSet getPreviousTransitive() {
        PlayerSet todo = new PlayerSet().with(this);
        PlayerSet result = new PlayerSet();
        while(!todo.isEmpty()) {
            Player current = todo.first();
            todo.remove(current);
            if(!result.contains(current)) {
                result.add(current);
                if(!result.contains(current.getPrevious())) {
                    todo.with(current.getPrevious());
                }
            }
        }
        return result;
    }

    /**
     * Loop through current set of ModelType objects and attach the Player object passed as parameter to the Previous
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their Previous attributes.
     */
    public PlayerSet withPrevious(Player value) {
        for(Player obj: this) {
            obj.withPrevious(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Player objects and collect a set of the Player objects reached via next.
     *
     * @return Set of Player objects reachable via next
     */
    public PlayerSet getNext() {
        PlayerSet result = new PlayerSet();
        for(Player obj: this) {
            result.with(obj.getNext());
        }
        return result;
    }

    /**
     * Loop through the current set of Player objects and collect all contained objects with reference next pointing to
     * the object passed as parameter.
     *
     * @param value The object required as next neighbor of the collected results.
     *
     * @return Set of Player objects referring to value via next
     */
    public PlayerSet filterNext(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        PlayerSet answer = new PlayerSet();
        for(Player obj: this) {
            if(neighbors.contains(obj.getNext()) || (neighbors.isEmpty() && obj.getNext() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Follow next reference zero or more times and collect all reachable objects. Detect cycles and deal with them.
     *
     * @return Set of Player objects reachable via next transitively (including the start set)
     */
    public PlayerSet getNextTransitive() {
        PlayerSet todo = new PlayerSet().with(this);
        PlayerSet result = new PlayerSet();
        while(!todo.isEmpty()) {
            Player current = todo.first();
            todo.remove(current);
            if(!result.contains(current)) {
                result.add(current);
                if(!result.contains(current.getNext())) {
                    todo.with(current.getNext());
                }
            }
        }
        return result;
    }

    /**
     * Loop through current set of ModelType objects and attach the Player object passed as parameter to the Next
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their Next attributes.
     */
    public PlayerSet withNext(Player value) {
        for(Player obj: this) {
            obj.withNext(value);
        }
        return this;
    }

    public AIPlayerSet instanceOfAIPlayer() {
        AIPlayerSet result = new AIPlayerSet();
        for(Object obj: this) {
            if(obj instanceof AIPlayer) {
                result.with(obj);
            }
        }
        return result;
    }
}
