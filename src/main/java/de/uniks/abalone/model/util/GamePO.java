package de.uniks.abalone.model.util;

import org.sdmlib.models.pattern.PatternObject;

import de.uniks.abalone.model.Board;
import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Logic;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;

public class GamePO extends PatternObject<GamePO, Game> {
    public GameSet allMatches() {
        setDoAllMatches(true);
        GameSet matches = new GameSet();
        while(getPattern().getHasMatch()) {
            matches.add(getCurrentMatch());
            getPattern().findMatch();
        }
        return matches;
    }

    public GamePO() {
        newInstance(null);
    }

    public GamePO(Game... hostGraphObject) {
        if(hostGraphObject == null || hostGraphObject.length < 1) {
            return;
        }
        newInstance(null, hostGraphObject);
    }

    public GamePO(String modifier) {
        setModifier(modifier);
    }

    public BoardPO createBoardPO() {
        BoardPO result = new BoardPO(new Board[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Game.PROPERTY_BOARD, result);
        return result;
    }

    public BoardPO createBoardPO(String modifier) {
        BoardPO result = new BoardPO(new Board[] {});
        result.setModifier(modifier);
        super.hasLink(Game.PROPERTY_BOARD, result);
        return result;
    }

    public GamePO createBoardLink(BoardPO tgt) {
        return hasLinkConstraint(tgt, Game.PROPERTY_BOARD);
    }

    public GamePO createBoardLink(BoardPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Game.PROPERTY_BOARD, modifier);
    }

    public Board getBoard() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getBoard();
        }
        return null;
    }

    public FieldPO createFieldsPO() {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Game.PROPERTY_FIELDS, result);
        return result;
    }

    public FieldPO createFieldsPO(String modifier) {
        FieldPO result = new FieldPO(new Field[] {});
        result.setModifier(modifier);
        super.hasLink(Game.PROPERTY_FIELDS, result);
        return result;
    }

    public GamePO createFieldsLink(FieldPO tgt) {
        return hasLinkConstraint(tgt, Game.PROPERTY_FIELDS);
    }

    public GamePO createFieldsLink(FieldPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Game.PROPERTY_FIELDS, modifier);
    }

    public FieldSet getFields() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getFields();
        }
        return null;
    }

    public TokenPO createTokensPO() {
        TokenPO result = new TokenPO(new Token[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Game.PROPERTY_TOKENS, result);
        return result;
    }

    public TokenPO createTokensPO(String modifier) {
        TokenPO result = new TokenPO(new Token[] {});
        result.setModifier(modifier);
        super.hasLink(Game.PROPERTY_TOKENS, result);
        return result;
    }

    public GamePO createTokensLink(TokenPO tgt) {
        return hasLinkConstraint(tgt, Game.PROPERTY_TOKENS);
    }

    public GamePO createTokensLink(TokenPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Game.PROPERTY_TOKENS, modifier);
    }

    public TokenSet getTokens() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getTokens();
        }
        return null;
    }

    public PlayerPO createPlayersPO() {
        PlayerPO result = new PlayerPO(new Player[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Game.PROPERTY_PLAYERS, result);
        return result;
    }

    public PlayerPO createPlayersPO(String modifier) {
        PlayerPO result = new PlayerPO(new Player[] {});
        result.setModifier(modifier);
        super.hasLink(Game.PROPERTY_PLAYERS, result);
        return result;
    }

    public GamePO createPlayersLink(PlayerPO tgt) {
        return hasLinkConstraint(tgt, Game.PROPERTY_PLAYERS);
    }

    public GamePO createPlayersLink(PlayerPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Game.PROPERTY_PLAYERS, modifier);
    }

    public PlayerSet getPlayers() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getPlayers();
        }
        return null;
    }

    public PlayerPO createCurrentPlayerPO() {
        PlayerPO result = new PlayerPO(new Player[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Game.PROPERTY_CURRENTPLAYER, result);
        return result;
    }

    public PlayerPO createCurrentPlayerPO(String modifier) {
        PlayerPO result = new PlayerPO(new Player[] {});
        result.setModifier(modifier);
        super.hasLink(Game.PROPERTY_CURRENTPLAYER, result);
        return result;
    }

    public GamePO createCurrentPlayerLink(PlayerPO tgt) {
        return hasLinkConstraint(tgt, Game.PROPERTY_CURRENTPLAYER);
    }

    public GamePO createCurrentPlayerLink(PlayerPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Game.PROPERTY_CURRENTPLAYER, modifier);
    }

    public Player getCurrentPlayer() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getCurrentPlayer();
        }
        return null;
    }

    public PlayerPO createWinnerPO() {
        PlayerPO result = new PlayerPO(new Player[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Game.PROPERTY_WINNER, result);
        return result;
    }

    public PlayerPO createWinnerPO(String modifier) {
        PlayerPO result = new PlayerPO(new Player[] {});
        result.setModifier(modifier);
        super.hasLink(Game.PROPERTY_WINNER, result);
        return result;
    }

    public GamePO createWinnerLink(PlayerPO tgt) {
        return hasLinkConstraint(tgt, Game.PROPERTY_WINNER);
    }

    public GamePO createWinnerLink(PlayerPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Game.PROPERTY_WINNER, modifier);
    }

    public Player getWinner() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getWinner();
        }
        return null;
    }

    public LogicPO createLogicPO() {
        LogicPO result = new LogicPO(new Logic[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Game.PROPERTY_LOGIC, result);
        return result;
    }

    public LogicPO createLogicPO(String modifier) {
        LogicPO result = new LogicPO(new Logic[] {});
        result.setModifier(modifier);
        super.hasLink(Game.PROPERTY_LOGIC, result);
        return result;
    }

    public GamePO createLogicLink(LogicPO tgt) {
        return hasLinkConstraint(tgt, Game.PROPERTY_LOGIC);
    }

    public GamePO createLogicLink(LogicPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Game.PROPERTY_LOGIC, modifier);
    }

    public Logic getLogic() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getLogic();
        }
        return null;
    }

    public MovePO createHistoryPO() {
        MovePO result = new MovePO(new Move[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Game.PROPERTY_HISTORY, result);

        return result;
    }

    public MovePO createHistoryPO(String modifier) {
        MovePO result = new MovePO(new Move[] {});

        result.setModifier(modifier);
        super.hasLink(Game.PROPERTY_HISTORY, result);

        return result;
    }

    public GamePO createHistoryLink(MovePO tgt) {
        return hasLinkConstraint(tgt, Game.PROPERTY_HISTORY);
    }

    public GamePO createHistoryLink(MovePO tgt, String modifier) {
        return hasLinkConstraint(tgt, Game.PROPERTY_HISTORY, modifier);
    }

    public MoveSet getHistory() {
        if(this.getPattern().getHasMatch()) {
            return ((Game) this.getCurrentMatch()).getHistory();
        }
        return null;
    }

}
