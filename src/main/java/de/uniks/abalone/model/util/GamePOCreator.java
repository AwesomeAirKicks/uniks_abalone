package de.uniks.abalone.model.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;

import de.uniks.abalone.model.Game;
import de.uniks.networkparser.IdMap;

public class GamePOCreator extends PatternObjectCreator {
    @Override
    public Object getSendableInstance(boolean reference) {
        if(reference) {
            return new GamePO(new Game[] {});
        } else {
            return new GamePO();
        }
    }

    public static IdMap createIdMap(String sessionID) {
        return de.uniks.abalone.model.util.CreatorCreator.createIdMap(sessionID);
    }
}
