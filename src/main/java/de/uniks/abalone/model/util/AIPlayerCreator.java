/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model.util;

import de.uniks.abalone.model.AIPlayer;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;
import de.uniks.networkparser.IdMap;
import de.uniks.networkparser.interfaces.SendableEntityCreator;

public class AIPlayerCreator implements SendableEntityCreator {
    private final String[] properties = new String[] {
            Player.PROPERTY_NAME,
            Player.PROPERTY_COLOR,
            Player.PROPERTY_SCORES,
            AIPlayer.PROPERTY_GAME,
            AIPlayer.PROPERTY_CURRENTGAME,
            AIPlayer.PROPERTY_WONGAME,
            AIPlayer.PROPERTY_TOKENS,
            AIPlayer.PROPERTY_PREVIOUS,
            AIPlayer.PROPERTY_NEXT,
            AIPlayer.PROPERTY_STRENGTH,
    };

    @Override
    public String[] getProperties() {
        return this.properties;
    }

    @Override
    public Object getSendableInstance(boolean reference) {
        return new AIPlayer();
    }

    @Override
    public Object getValue(Object target, String attrName) {
        int pos = attrName.indexOf('.');
        String attribute = attrName;
        if(pos > 0) {
            attribute = attrName.substring(0, pos);
        }
        if(Player.PROPERTY_NAME.equalsIgnoreCase(attribute)) {
            return ((Player) target).getName();
        }
        if(Player.PROPERTY_COLOR.equalsIgnoreCase(attribute)) {
            return ((Player) target).getColor();
        }
        if(Player.PROPERTY_SCORES.equalsIgnoreCase(attribute)) {
            return ((Player) target).getScores();
        }
        if(AIPlayer.PROPERTY_GAME.equalsIgnoreCase(attribute)) {
            return ((AIPlayer) target).getGame();
        }
        if(AIPlayer.PROPERTY_CURRENTGAME.equalsIgnoreCase(attribute)) {
            return ((AIPlayer) target).getCurrentGame();
        }
        if(AIPlayer.PROPERTY_WONGAME.equalsIgnoreCase(attribute)) {
            return ((AIPlayer) target).getWonGame();
        }
        if(AIPlayer.PROPERTY_TOKENS.equalsIgnoreCase(attribute)) {
            return ((AIPlayer) target).getTokens();
        }
        if(AIPlayer.PROPERTY_PREVIOUS.equalsIgnoreCase(attribute)) {
            return ((AIPlayer) target).getPrevious();
        }
        if(AIPlayer.PROPERTY_NEXT.equalsIgnoreCase(attribute)) {
            return ((AIPlayer) target).getNext();
        }

        if(AIPlayer.PROPERTY_STRENGTH.equalsIgnoreCase(attribute)) {
            return ((AIPlayer) target).getStrength();
        }

        return null;
    }

    @Override
    public boolean setValue(Object target, String attrName, Object value, String type) {
        if(AIPlayer.PROPERTY_STRENGTH.equalsIgnoreCase(attrName)) {
            ((AIPlayer) target).setStrength(Integer.parseInt(value.toString()));
            return true;
        }

        if(Player.PROPERTY_SCORES.equalsIgnoreCase(attrName)) {
            ((Player) target).setScores(Integer.parseInt(value.toString()));
            return true;
        }
        if(Player.PROPERTY_COLOR.equalsIgnoreCase(attrName)) {
            ((Player) target).setColor((String) value);
            return true;
        }
        if(Player.PROPERTY_NAME.equalsIgnoreCase(attrName)) {
            ((Player) target).setName((String) value);
            return true;
        }
        if(SendableEntityCreator.REMOVE.equals(type) && value != null) {
            attrName = attrName + type;
        }
        if(AIPlayer.PROPERTY_GAME.equalsIgnoreCase(attrName)) {
            ((AIPlayer) target).setGame((Game) value);
            return true;
        }
        if(AIPlayer.PROPERTY_CURRENTGAME.equalsIgnoreCase(attrName)) {
            ((AIPlayer) target).setCurrentGame((Game) value);
            return true;
        }
        if(AIPlayer.PROPERTY_WONGAME.equalsIgnoreCase(attrName)) {
            ((AIPlayer) target).setWonGame((Game) value);
            return true;
        }
        if(AIPlayer.PROPERTY_TOKENS.equalsIgnoreCase(attrName)) {
            ((AIPlayer) target).withTokens((Token) value);
            return true;
        }
        if((AIPlayer.PROPERTY_TOKENS + SendableEntityCreator.REMOVE).equalsIgnoreCase(attrName)) {
            ((AIPlayer) target).withoutTokens((Token) value);
            return true;
        }
        if(AIPlayer.PROPERTY_PREVIOUS.equalsIgnoreCase(attrName)) {
            ((AIPlayer) target).setPrevious((Player) value);
            return true;
        }
        if(AIPlayer.PROPERTY_NEXT.equalsIgnoreCase(attrName)) {
            ((AIPlayer) target).setNext((Player) value);
            return true;
        }
        return false;
    }

    public static IdMap createIdMap(String sessionID) {
        return de.uniks.abalone.model.util.CreatorCreator.createIdMap(sessionID);
    }

    // ==========================================================================
    public void removeObject(Object entity) {
        ((AIPlayer) entity).removeYou();
    }
}
