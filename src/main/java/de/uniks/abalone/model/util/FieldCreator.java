/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model.util;

import de.uniks.abalone.model.Board;
import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Token;
import de.uniks.networkparser.IdMap;
import de.uniks.networkparser.interfaces.SendableEntityCreator;

public class FieldCreator implements SendableEntityCreator {
    private final String[] properties = new String[] {
            Field.PROPERTY_X,
            Field.PROPERTY_Y,
            Field.PROPERTY_GAME,
            Field.PROPERTY_BOARD,
            Field.PROPERTY_TOKEN,
            Field.PROPERTY_RIGHT,
            Field.PROPERTY_LEFT,
            Field.PROPERTY_DOWNLEFT,
            Field.PROPERTY_TOPRIGHT,
            Field.PROPERTY_DOWNRIGHT,
            Field.PROPERTY_TOPLEFT,
            Field.PROPERTY_SELECTED,
            Field.PROPERTY_LASTMOVED,
    };

    @Override
    public String[] getProperties() {
        return properties;
    }

    @Override
    public Object getSendableInstance(boolean reference) {
        return new Field();
    }

    @Override
    public Object getValue(Object target, String attrName) {
        int pos = attrName.indexOf('.');
        String attribute = attrName;
        if(pos > 0) {
            attribute = attrName.substring(0, pos);
        }
        if(Field.PROPERTY_X.equalsIgnoreCase(attribute)) {
            return ((Field) target).getX();
        }
        if(Field.PROPERTY_Y.equalsIgnoreCase(attribute)) {
            return ((Field) target).getY();
        }
        if(Field.PROPERTY_GAME.equalsIgnoreCase(attribute)) {
            return ((Field) target).getGame();
        }
        if(Field.PROPERTY_BOARD.equalsIgnoreCase(attribute)) {
            return ((Field) target).getBoard();
        }
        if(Field.PROPERTY_TOKEN.equalsIgnoreCase(attribute)) {
            return ((Field) target).getToken();
        }
        if(Field.PROPERTY_RIGHT.equalsIgnoreCase(attribute)) {
            return ((Field) target).getRight();
        }
        if(Field.PROPERTY_LEFT.equalsIgnoreCase(attribute)) {
            return ((Field) target).getLeft();
        }
        if(Field.PROPERTY_DOWNLEFT.equalsIgnoreCase(attribute)) {
            return ((Field) target).getDownLeft();
        }
        if(Field.PROPERTY_TOPRIGHT.equalsIgnoreCase(attribute)) {
            return ((Field) target).getTopRight();
        }
        if(Field.PROPERTY_DOWNRIGHT.equalsIgnoreCase(attribute)) {
            return ((Field) target).getDownRight();
        }
        if(Field.PROPERTY_TOPLEFT.equalsIgnoreCase(attribute)) {
            return ((Field) target).getTopLeft();
        }

        if(Field.PROPERTY_SELECTED.equalsIgnoreCase(attribute)) {
            return ((Field) target).isSelected();
        }

        if(Field.PROPERTY_LASTMOVED.equalsIgnoreCase(attribute)) {
            return ((Field) target).isLastMoved();
        }
        return null;
    }

    @Override
    public boolean setValue(Object target, String attrName, Object value, String type) {
        if(Field.PROPERTY_LASTMOVED.equalsIgnoreCase(attrName)) {
            ((Field) target).setLastMoved((Boolean) value);
            return true;
        }

        if(Field.PROPERTY_SELECTED.equalsIgnoreCase(attrName)) {
            ((Field) target).setSelected((Boolean) value);
            return true;
        }

        if(Field.PROPERTY_Y.equalsIgnoreCase(attrName)) {
            ((Field) target).setY(Integer.parseInt(value.toString()));
            return true;
        }
        if(Field.PROPERTY_X.equalsIgnoreCase(attrName)) {
            ((Field) target).setX(Integer.parseInt(value.toString()));
            return true;
        }
        if(SendableEntityCreator.REMOVE.equals(type) && value != null) {
            attrName = attrName + type;
        }
        if(Field.PROPERTY_GAME.equalsIgnoreCase(attrName)) {
            ((Field) target).setGame((Game) value);
            return true;
        }
        if(Field.PROPERTY_BOARD.equalsIgnoreCase(attrName)) {
            ((Field) target).setBoard((Board) value);
            return true;
        }
        if(Field.PROPERTY_TOKEN.equalsIgnoreCase(attrName)) {
            ((Field) target).setToken((Token) value);
            return true;
        }
        if(Field.PROPERTY_RIGHT.equalsIgnoreCase(attrName)) {
            ((Field) target).setRight((Field) value);
            return true;
        }
        if(Field.PROPERTY_LEFT.equalsIgnoreCase(attrName)) {
            ((Field) target).setLeft((Field) value);
            return true;
        }
        if(Field.PROPERTY_DOWNLEFT.equalsIgnoreCase(attrName)) {
            ((Field) target).setDownLeft((Field) value);
            return true;
        }
        if(Field.PROPERTY_TOPRIGHT.equalsIgnoreCase(attrName)) {
            ((Field) target).setTopRight((Field) value);
            return true;
        }
        if(Field.PROPERTY_DOWNRIGHT.equalsIgnoreCase(attrName)) {
            ((Field) target).setDownRight((Field) value);
            return true;
        }
        if(Field.PROPERTY_TOPLEFT.equalsIgnoreCase(attrName)) {
            ((Field) target).setTopLeft((Field) value);
            return true;
        }
        return false;
    }

    public static IdMap createIdMap(String sessionID) {
        return de.uniks.abalone.model.util.CreatorCreator.createIdMap(sessionID);
    }

    // ==========================================================================
    public void removeObject(Object entity) {
        ((Field) entity).removeYou();
    }
}
