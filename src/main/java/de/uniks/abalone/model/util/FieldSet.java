/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model.util;

import java.util.Collection;

import de.uniks.abalone.model.Board;
import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Token;
import de.uniks.networkparser.interfaces.Condition;
import de.uniks.networkparser.list.BooleanList;
import de.uniks.networkparser.list.NumberList;
import de.uniks.networkparser.list.ObjectSet;
import de.uniks.networkparser.list.SimpleSet;
import de.uniks.abalone.model.util.GameSet;
import de.uniks.abalone.model.util.BoardSet;
import de.uniks.abalone.model.util.TokenSet;

public class FieldSet extends SimpleSet<Field> {
    @Override
    protected Class<?> getTypClass() {
        return Field.class;
    }

    public FieldSet() {
        // empty
    }

    public FieldSet(Field... objects) {
        for(Field obj: objects) {
            this.add(obj);
        }
    }

    public FieldSet(Collection<Field> objects) {
        this.addAll(objects);
    }

    public static final FieldSet EMPTY_SET = new FieldSet().withFlag(FieldSet.READONLY);

    public FieldPO createFieldPO() {
        return new FieldPO(this.toArray(new Field[size()]));
    }

    public String getEntryType() {
        return "de.uniks.abalone.model.Field";
    }

    @Override
    public FieldSet getNewList(boolean keyValue) {
        return new FieldSet();
    }

    @Override
    public FieldSet filter(Condition<Field> condition) {
        FieldSet filterList = new FieldSet();
        filterItems(filterList, condition);
        return filterList;
    }

    @SuppressWarnings("unchecked")
    public FieldSet with(Object value) {
        if(value == null) {
            return this;
        } else if(value instanceof java.util.Collection) {
            this.addAll((Collection<Field>) value);
        } else if(value != null) {
            this.add((Field) value);
        }
        return this;
    }

    public FieldSet without(Field value) {
        this.remove(value);
        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a list of the x attribute values.
     *
     * @return List of int objects reachable via x attribute
     */
    public NumberList getX() {
        NumberList result = new NumberList();
        for(Field obj: this) {
            result.add(obj.getX());
        }
        return result;
    }

    /**
     * Loop through the current set of Field objects and collect those Field objects where the x attribute matches the
     * parameter value.
     *
     * @param value Search value
     *
     * @return Subset of Field objects that match the parameter
     */
    public FieldSet filterX(int value) {
        FieldSet result = new FieldSet();
        for(Field obj: this) {
            if(value == obj.getX()) {
                result.add(obj);
            }
        }
        return result;
    }

    /**
     * Loop through the current set of Field objects and collect those Field objects where the x attribute is between
     * lower and upper.
     *
     * @param lower Lower bound
     * @param upper Upper bound
     *
     * @return Subset of Field objects that match the parameter
     */
    public FieldSet filterX(int lower, int upper) {
        FieldSet result = new FieldSet();
        for(Field obj: this) {
            if(lower <= obj.getX() && obj.getX() <= upper) {
                result.add(obj);
            }
        }
        return result;
    }

    /**
     * Loop through the current set of Field objects and assign value to the x attribute of each of it.
     *
     * @param value New attribute value
     *
     * @return Current set of Field objects now with new attribute values.
     */
    public FieldSet withX(int value) {
        for(Field obj: this) {
            obj.setX(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a list of the y attribute values.
     *
     * @return List of int objects reachable via y attribute
     */
    public NumberList getY() {
        NumberList result = new NumberList();
        for(Field obj: this) {
            result.add(obj.getY());
        }
        return result;
    }

    /**
     * Loop through the current set of Field objects and collect those Field objects where the y attribute matches the
     * parameter value.
     *
     * @param value Search value
     *
     * @return Subset of Field objects that match the parameter
     */
    public FieldSet filterY(int value) {
        FieldSet result = new FieldSet();
        for(Field obj: this) {
            if(value == obj.getY()) {
                result.add(obj);
            }
        }
        return result;
    }

    /**
     * Loop through the current set of Field objects and collect those Field objects where the y attribute is between
     * lower and upper.
     *
     * @param lower Lower bound
     * @param upper Upper bound
     *
     * @return Subset of Field objects that match the parameter
     */
    public FieldSet filterY(int lower, int upper) {
        FieldSet result = new FieldSet();
        for(Field obj: this) {
            if(lower <= obj.getY() && obj.getY() <= upper) {
                result.add(obj);
            }
        }
        return result;
    }

    /**
     * Loop through the current set of Field objects and assign value to the y attribute of each of it.
     *
     * @param value New attribute value
     *
     * @return Current set of Field objects now with new attribute values.
     */
    public FieldSet withY(int value) {
        for(Field obj: this) {
            obj.setY(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a set of the Game objects reached via game.
     *
     * @return Set of Game objects reachable via game
     */
    public GameSet getGame() {
        GameSet result = new GameSet();
        for(Field obj: this) {
            result.with(obj.getGame());
        }
        return result;
    }

    /**
     * Loop through the current set of Field objects and collect all contained objects with reference game pointing to
     * the object passed as parameter.
     *
     * @param value The object required as game neighbor of the collected results.
     *
     * @return Set of Game objects referring to value via game
     */
    public FieldSet filterGame(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        FieldSet answer = new FieldSet();
        for(Field obj: this) {
            if(neighbors.contains(obj.getGame()) || (neighbors.isEmpty() && obj.getGame() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Field object passed as parameter to the Game
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their Game attributes.
     */
    public FieldSet withGame(Game value) {
        for(Field obj: this) {
            obj.withGame(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a set of the Board objects reached via board.
     *
     * @return Set of Board objects reachable via board
     */
    public BoardSet getBoard() {
        BoardSet result = new BoardSet();
        for(Field obj: this) {
            result.with(obj.getBoard());
        }
        return result;
    }

    /**
     * Loop through the current set of Field objects and collect all contained objects with reference board pointing to
     * the object passed as parameter.
     *
     * @param value The object required as board neighbor of the collected results.
     *
     * @return Set of Board objects referring to value via board
     */
    public FieldSet filterBoard(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        FieldSet answer = new FieldSet();
        for(Field obj: this) {
            if(neighbors.contains(obj.getBoard()) || (neighbors.isEmpty() && obj.getBoard() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Field object passed as parameter to the Board
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their Board attributes.
     */
    public FieldSet withBoard(Board value) {
        for(Field obj: this) {
            obj.withBoard(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a set of the Token objects reached via token.
     *
     * @return Set of Token objects reachable via token
     */
    public TokenSet getToken() {
        TokenSet result = new TokenSet();
        for(Field obj: this) {
            result.with(obj.getToken());
        }
        return result;
    }

    /**
     * Loop through the current set of Field objects and collect all contained objects with reference token pointing to
     * the object passed as parameter.
     *
     * @param value The object required as token neighbor of the collected results.
     *
     * @return Set of Token objects referring to value via token
     */
    public FieldSet filterToken(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        FieldSet answer = new FieldSet();
        for(Field obj: this) {
            if(neighbors.contains(obj.getToken()) || (neighbors.isEmpty() && obj.getToken() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Field object passed as parameter to the Token
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their Token attributes.
     */
    public FieldSet withToken(Token value) {
        for(Field obj: this) {
            obj.withToken(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a set of the Field objects reached via right.
     *
     * @return Set of Field objects reachable via right
     */
    public FieldSet getRight() {
        FieldSet result = new FieldSet();
        for(Field obj: this) {
            result.with(obj.getRight());
        }
        return result;
    }

    /**
     * Loop through the current set of Field objects and collect all contained objects with reference right pointing to
     * the object passed as parameter.
     *
     * @param value The object required as right neighbor of the collected results.
     *
     * @return Set of Field objects referring to value via right
     */
    public FieldSet filterRight(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        FieldSet answer = new FieldSet();
        for(Field obj: this) {
            if(neighbors.contains(obj.getRight()) || (neighbors.isEmpty() && obj.getRight() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Follow right reference zero or more times and collect all reachable objects. Detect cycles and deal with them.
     *
     * @return Set of Field objects reachable via right transitively (including the start set)
     */
    public FieldSet getRightTransitive() {
        FieldSet todo = new FieldSet().with(this);
        FieldSet result = new FieldSet();
        while(!todo.isEmpty()) {
            Field current = todo.first();
            todo.remove(current);
            if(!result.contains(current)) {
                result.add(current);
                if(!result.contains(current.getRight())) {
                    todo.with(current.getRight());
                }
            }
        }
        return result;
    }

    /**
     * Loop through current set of ModelType objects and attach the Field object passed as parameter to the Right
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their Right attributes.
     */
    public FieldSet withRight(Field value) {
        for(Field obj: this) {
            obj.withRight(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a set of the Field objects reached via left.
     *
     * @return Set of Field objects reachable via left
     */
    public FieldSet getLeft() {
        FieldSet result = new FieldSet();
        for(Field obj: this) {
            result.with(obj.getLeft());
        }
        return result;
    }

    /**
     * Loop through the current set of Field objects and collect all contained objects with reference left pointing to
     * the object passed as parameter.
     *
     * @param value The object required as left neighbor of the collected results.
     *
     * @return Set of Field objects referring to value via left
     */
    public FieldSet filterLeft(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        FieldSet answer = new FieldSet();
        for(Field obj: this) {
            if(neighbors.contains(obj.getLeft()) || (neighbors.isEmpty() && obj.getLeft() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Follow left reference zero or more times and collect all reachable objects. Detect cycles and deal with them.
     *
     * @return Set of Field objects reachable via left transitively (including the start set)
     */
    public FieldSet getLeftTransitive() {
        FieldSet todo = new FieldSet().with(this);
        FieldSet result = new FieldSet();
        while(!todo.isEmpty()) {
            Field current = todo.first();
            todo.remove(current);
            if(!result.contains(current)) {
                result.add(current);
                if(!result.contains(current.getLeft())) {
                    todo.with(current.getLeft());
                }
            }
        }
        return result;
    }

    /**
     * Loop through current set of ModelType objects and attach the Field object passed as parameter to the Left
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their Left attributes.
     */
    public FieldSet withLeft(Field value) {
        for(Field obj: this) {
            obj.withLeft(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a set of the Field objects reached via downLeft.
     *
     * @return Set of Field objects reachable via downLeft
     */
    public FieldSet getDownLeft() {
        FieldSet result = new FieldSet();
        for(Field obj: this) {
            result.with(obj.getDownLeft());
        }
        return result;
    }

    /**
     * Loop through the current set of Field objects and collect all contained objects with reference downLeft pointing
     * to the object passed as parameter.
     *
     * @param value The object required as downLeft neighbor of the collected results.
     *
     * @return Set of Field objects referring to value via downLeft
     */
    public FieldSet filterDownLeft(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        FieldSet answer = new FieldSet();
        for(Field obj: this) {
            if(neighbors.contains(obj.getDownLeft()) || (neighbors.isEmpty() && obj.getDownLeft() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Follow downLeft reference zero or more times and collect all reachable objects. Detect cycles and deal with them.
     *
     * @return Set of Field objects reachable via downLeft transitively (including the start set)
     */
    public FieldSet getDownLeftTransitive() {
        FieldSet todo = new FieldSet().with(this);
        FieldSet result = new FieldSet();
        while(!todo.isEmpty()) {
            Field current = todo.first();
            todo.remove(current);
            if(!result.contains(current)) {
                result.add(current);
                if(!result.contains(current.getDownLeft())) {
                    todo.with(current.getDownLeft());
                }
            }
        }
        return result;
    }

    /**
     * Loop through current set of ModelType objects and attach the Field object passed as parameter to the DownLeft
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their DownLeft attributes.
     */
    public FieldSet withDownLeft(Field value) {
        for(Field obj: this) {
            obj.withDownLeft(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a set of the Field objects reached via topRight.
     *
     * @return Set of Field objects reachable via topRight
     */
    public FieldSet getTopRight() {
        FieldSet result = new FieldSet();
        for(Field obj: this) {
            result.with(obj.getTopRight());
        }
        return result;
    }

    /**
     * Loop through the current set of Field objects and collect all contained objects with reference topRight pointing
     * to the object passed as parameter.
     *
     * @param value The object required as topRight neighbor of the collected results.
     *
     * @return Set of Field objects referring to value via topRight
     */
    public FieldSet filterTopRight(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        FieldSet answer = new FieldSet();
        for(Field obj: this) {
            if(neighbors.contains(obj.getTopRight()) || (neighbors.isEmpty() && obj.getTopRight() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Follow topRight reference zero or more times and collect all reachable objects. Detect cycles and deal with them.
     *
     * @return Set of Field objects reachable via topRight transitively (including the start set)
     */
    public FieldSet getTopRightTransitive() {
        FieldSet todo = new FieldSet().with(this);
        FieldSet result = new FieldSet();
        while(!todo.isEmpty()) {
            Field current = todo.first();
            todo.remove(current);
            if(!result.contains(current)) {
                result.add(current);
                if(!result.contains(current.getTopRight())) {
                    todo.with(current.getTopRight());
                }
            }
        }
        return result;
    }

    /**
     * Loop through current set of ModelType objects and attach the Field object passed as parameter to the TopRight
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their TopRight attributes.
     */
    public FieldSet withTopRight(Field value) {
        for(Field obj: this) {
            obj.withTopRight(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a set of the Field objects reached via downRight.
     *
     * @return Set of Field objects reachable via downRight
     */
    public FieldSet getDownRight() {
        FieldSet result = new FieldSet();
        for(Field obj: this) {
            result.with(obj.getDownRight());
        }
        return result;
    }

    /**
     * Loop through the current set of Field objects and collect all contained objects with reference downRight pointing
     * to the object passed as parameter.
     *
     * @param value The object required as downRight neighbor of the collected results.
     *
     * @return Set of Field objects referring to value via downRight
     */
    public FieldSet filterDownRight(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        FieldSet answer = new FieldSet();
        for(Field obj: this) {
            if(neighbors.contains(obj.getDownRight()) || (neighbors.isEmpty() && obj.getDownRight() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Follow downRight reference zero or more times and collect all reachable objects. Detect cycles and deal with
     * them.
     *
     * @return Set of Field objects reachable via downRight transitively (including the start set)
     */
    public FieldSet getDownRightTransitive() {
        FieldSet todo = new FieldSet().with(this);
        FieldSet result = new FieldSet();
        while(!todo.isEmpty()) {
            Field current = todo.first();
            todo.remove(current);
            if(!result.contains(current)) {
                result.add(current);
                if(!result.contains(current.getDownRight())) {
                    todo.with(current.getDownRight());
                }
            }
        }
        return result;
    }

    /**
     * Loop through current set of ModelType objects and attach the Field object passed as parameter to the DownRight
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their DownRight attributes.
     */
    public FieldSet withDownRight(Field value) {
        for(Field obj: this) {
            obj.withDownRight(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a set of the Field objects reached via topLeft.
     *
     * @return Set of Field objects reachable via topLeft
     */
    public FieldSet getTopLeft() {
        FieldSet result = new FieldSet();
        for(Field obj: this) {
            result.with(obj.getTopLeft());
        }
        return result;
    }

    /**
     * Loop through the current set of Field objects and collect all contained objects with reference topLeft pointing
     * to the object passed as parameter.
     *
     * @param value The object required as topLeft neighbor of the collected results.
     *
     * @return Set of Field objects referring to value via topLeft
     */
    public FieldSet filterTopLeft(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        FieldSet answer = new FieldSet();
        for(Field obj: this) {
            if(neighbors.contains(obj.getTopLeft()) || (neighbors.isEmpty() && obj.getTopLeft() == null)) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Follow topLeft reference zero or more times and collect all reachable objects. Detect cycles and deal with them.
     *
     * @return Set of Field objects reachable via topLeft transitively (including the start set)
     */
    public FieldSet getTopLeftTransitive() {
        FieldSet todo = new FieldSet().with(this);
        FieldSet result = new FieldSet();
        while(!todo.isEmpty()) {
            Field current = todo.first();
            todo.remove(current);
            if(!result.contains(current)) {
                result.add(current);
                if(!result.contains(current.getTopLeft())) {
                    todo.with(current.getTopLeft());
                }
            }
        }
        return result;
    }

    /**
     * Loop through current set of ModelType objects and attach the Field object passed as parameter to the TopLeft
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their TopLeft attributes.
     */
    public FieldSet withTopLeft(Field value) {
        for(Field obj: this) {
            obj.withTopLeft(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a list of the selected attribute values.
     * 
     * @return List of boolean objects reachable via selected attribute
     */
    public BooleanList getSelected() {
        BooleanList result = new BooleanList();

        for(Field obj: this) {
            result.add(obj.isSelected());
        }

        return result;
    }

    /**
     * Loop through the current set of Field objects and collect those Field objects where the selected attribute
     * matches the parameter value.
     * 
     * @param value Search value
     * 
     * @return Subset of Field objects that match the parameter
     */
    public FieldSet filterSelected(boolean value) {
        FieldSet result = new FieldSet();

        for(Field obj: this) {
            if(value == obj.isSelected()) {
                result.add(obj);
            }
        }

        return result;
    }

    /**
     * Loop through the current set of Field objects and assign value to the selected attribute of each of it.
     * 
     * @param value New attribute value
     * 
     * @return Current set of Field objects now with new attribute values.
     */
    public FieldSet withSelected(boolean value) {
        for(Field obj: this) {
            obj.setSelected(value);
        }

        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a list of the lastMoved attribute values.
     * 
     * @return List of boolean objects reachable via lastMoved attribute
     */
    public BooleanList getLastMoved() {
        BooleanList result = new BooleanList();

        for(Field obj: this) {
            result.add(obj.isLastMoved());
        }

        return result;
    }

    /**
     * Loop through the current set of Field objects and collect those Field objects where the lastMoved attribute
     * matches the parameter value.
     * 
     * @param value Search value
     * 
     * @return Subset of Field objects that match the parameter
     */
    public FieldSet filterLastMoved(boolean value) {
        FieldSet result = new FieldSet();

        for(Field obj: this) {
            if(value == obj.isLastMoved()) {
                result.add(obj);
            }
        }

        return result;
    }

    /**
     * Loop through the current set of Field objects and assign value to the lastMoved attribute of each of it.
     * 
     * @param value New attribute value
     * 
     * @return Current set of Field objects now with new attribute values.
     */
    public FieldSet withLastMoved(boolean value) {
        for(Field obj: this) {
            obj.setLastMoved(value);
        }

        return this;
    }

}
