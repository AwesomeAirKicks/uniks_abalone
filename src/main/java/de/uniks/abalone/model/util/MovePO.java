package de.uniks.abalone.model.util;

import org.sdmlib.models.pattern.AttributeConstraint;
import org.sdmlib.models.pattern.Pattern;
import org.sdmlib.models.pattern.PatternObject;

import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Token;

public class MovePO extends PatternObject<MovePO, Move> {
    public MoveSet allMatches() {
        setDoAllMatches(true);
        MoveSet matches = new MoveSet();
        while(getPattern().getHasMatch()) {
            matches.add(getCurrentMatch());
            getPattern().findMatch();
        }
        return matches;
    }

    public MovePO() {
        newInstance(null);
    }

    public MovePO(Move... hostGraphObject) {
        if(hostGraphObject == null || hostGraphObject.length < 1) {
            return;
        }
        newInstance(null, hostGraphObject);
    }

    public MovePO(String modifier) {
        setModifier(modifier);
    }

    public MovePO createDirectionCondition(int value) {
        new AttributeConstraint()
                .withAttrName(Move.PROPERTY_DIRECTION)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(getPattern().getModifier())
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public MovePO createDirectionCondition(int lower, int upper) {
        new AttributeConstraint()
                .withAttrName(Move.PROPERTY_DIRECTION)
                .withTgtValue(lower)
                .withUpperTgtValue(upper)
                .withSrc(this)
                .withModifier(getPattern().getModifier())
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public MovePO createDirectionAssignment(int value) {
        new AttributeConstraint()
                .withAttrName(Move.PROPERTY_DIRECTION)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(getPattern());
        super.filterAttr();
        return this;
    }

    public int getDirection() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getDirection();
        }
        return 0;
    }

    public MovePO withDirection(int value) {
        if(getPattern().getHasMatch()) {
            getCurrentMatch().setDirection(value);
        }
        return this;
    }

    public TokenPO createTokensPO() {
        TokenPO result = new TokenPO(new Token[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Move.PROPERTY_TOKENS, result);
        return result;
    }

    public TokenPO createTokensPO(String modifier) {
        TokenPO result = new TokenPO(new Token[] {});
        result.setModifier(modifier);
        super.hasLink(Move.PROPERTY_TOKENS, result);
        return result;
    }

    public MovePO createTokensLink(TokenPO tgt) {
        return hasLinkConstraint(tgt, Move.PROPERTY_TOKENS);
    }

    public MovePO createTokensLink(TokenPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Move.PROPERTY_TOKENS, modifier);
    }

    public TokenSet getTokens() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getTokens();
        }
        return null;
    }

    public TokenPO createOpponentTokensPO() {
        TokenPO result = new TokenPO(new Token[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Move.PROPERTY_OPPONENTTOKENS, result);

        return result;
    }

    public TokenPO createOpponentTokensPO(String modifier) {
        TokenPO result = new TokenPO(new Token[] {});

        result.setModifier(modifier);
        super.hasLink(Move.PROPERTY_OPPONENTTOKENS, result);

        return result;
    }

    public MovePO createOpponentTokensLink(TokenPO tgt) {
        return hasLinkConstraint(tgt, Move.PROPERTY_OPPONENTTOKENS);
    }

    public MovePO createOpponentTokensLink(TokenPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Move.PROPERTY_OPPONENTTOKENS, modifier);
    }

    public TokenSet getOpponentTokens() {
        if(this.getPattern().getHasMatch()) {
            return ((Move) this.getCurrentMatch()).getOpponentTokens();
        }
        return null;
    }

    public FieldPO createPreviousPositionPO() {
        FieldPO result = new FieldPO(new Field[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Move.PROPERTY_PREVIOUSPOSITION, result);

        return result;
    }

    public FieldPO createPreviousPositionPO(String modifier) {
        FieldPO result = new FieldPO(new Field[] {});

        result.setModifier(modifier);
        super.hasLink(Move.PROPERTY_PREVIOUSPOSITION, result);

        return result;
    }

    public MovePO createPreviousPositionLink(FieldPO tgt) {
        return hasLinkConstraint(tgt, Move.PROPERTY_PREVIOUSPOSITION);
    }

    public MovePO createPreviousPositionLink(FieldPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Move.PROPERTY_PREVIOUSPOSITION, modifier);
    }

    public Field getPreviousPosition() {
        if(this.getPattern().getHasMatch()) {
            return ((Move) this.getCurrentMatch()).getPreviousPosition();
        }
        return null;
    }

}
