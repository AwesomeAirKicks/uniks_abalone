/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model.util;

import java.util.Collection;
import java.util.Collections;

import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Token;
import de.uniks.networkparser.interfaces.Condition;
import de.uniks.networkparser.list.NumberList;
import de.uniks.networkparser.list.ObjectSet;
import de.uniks.networkparser.list.SimpleSet;
import de.uniks.abalone.model.util.FieldSet;
import de.uniks.abalone.model.util.TokenSet;

public class MoveSet extends SimpleSet<Move> {
    @Override
    protected Class<?> getTypClass() {
        return Move.class;
    }

    public MoveSet() {
        // empty
    }

    public MoveSet(Move... objects) {
        for(Move obj: objects) {
            this.add(obj);
        }
    }

    public MoveSet(Collection<Move> objects) {
        this.addAll(objects);
    }

    public static final MoveSet EMPTY_SET = new MoveSet().withFlag(MoveSet.READONLY);

    public MovePO createMovePO() {
        return new MovePO(this.toArray(new Move[size()]));
    }

    public String getEntryType() {
        return "de.uniks.abalone.model.Move";
    }

    @Override
    public MoveSet getNewList(boolean keyValue) {
        return new MoveSet();
    }

    @Override
    public MoveSet filter(Condition<Move> condition) {
        MoveSet filterList = new MoveSet();
        filterItems(filterList, condition);
        return filterList;
    }

    @SuppressWarnings("unchecked")
    public MoveSet with(Object value) {
        if(value == null) {
            return this;
        } else if(value instanceof java.util.Collection) {
            this.addAll((Collection<Move>) value);
        } else if(value != null) {
            this.add((Move) value);
        }
        return this;
    }

    public MoveSet without(Move value) {
        this.remove(value);
        return this;
    }

    /**
     * Loop through the current set of Move objects and collect a list of the direction attribute values.
     *
     * @return List of int objects reachable via direction attribute
     */
    public NumberList getDirection() {
        NumberList result = new NumberList();
        for(Move obj: this) {
            result.add(obj.getDirection());
        }
        return result;
    }

    /**
     * Loop through the current set of Move objects and collect those Move objects where the direction attribute matches
     * the parameter value.
     *
     * @param value Search value
     *
     * @return Subset of Move objects that match the parameter
     */
    public MoveSet filterDirection(int value) {
        MoveSet result = new MoveSet();
        for(Move obj: this) {
            if(value == obj.getDirection()) {
                result.add(obj);
            }
        }
        return result;
    }

    /**
     * Loop through the current set of Move objects and collect those Move objects where the direction attribute is
     * between lower and upper.
     *
     * @param lower Lower bound
     * @param upper Upper bound
     *
     * @return Subset of Move objects that match the parameter
     */
    public MoveSet filterDirection(int lower, int upper) {
        MoveSet result = new MoveSet();
        for(Move obj: this) {
            if(lower <= obj.getDirection() && obj.getDirection() <= upper) {
                result.add(obj);
            }
        }
        return result;
    }

    /**
     * Loop through the current set of Move objects and assign value to the direction attribute of each of it.
     *
     * @param value New attribute value
     *
     * @return Current set of Move objects now with new attribute values.
     */
    public MoveSet withDirection(int value) {
        for(Move obj: this) {
            obj.setDirection(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Move objects and collect a set of the Token objects reached via tokens.
     *
     * @return Set of Token objects reachable via tokens
     */
    public TokenSet getTokens() {
        TokenSet result = new TokenSet();
        for(Move obj: this) {
            result.with(obj.getTokens());
        }
        return result;
    }

    /**
     * Loop through the current set of Move objects and collect all contained objects with reference tokens pointing to
     * the object passed as parameter.
     *
     * @param value The object required as tokens neighbor of the collected results.
     *
     * @return Set of Token objects referring to value via tokens
     */
    public MoveSet filterTokens(Object value) {
        ObjectSet neighbors = new ObjectSet();
        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }
        MoveSet answer = new MoveSet();
        for(Move obj: this) {
            if(!Collections.disjoint(neighbors, obj.getTokens())) {
                answer.add(obj);
            }
        }
        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Move object passed as parameter to the Tokens
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now with the new neighbor attached to their Tokens attributes.
     */
    public MoveSet withTokens(Token value) {
        for(Move obj: this) {
            obj.withTokens(value);
        }
        return this;
    }

    /**
     * Loop through current set of ModelType objects and remove the Move object passed as parameter from the Tokens
     * attribute of each of it.
     *
     * @return The original set of ModelType objects now without the old neighbor.
     */
    public MoveSet withoutTokens(Token value) {
        for(Move obj: this) {
            obj.withoutTokens(value);
        }
        return this;
    }

    /**
     * Loop through the current set of Move objects and collect a set of the Token objects reached via opponentTokens.
     * 
     * @return Set of Token objects reachable via opponentTokens
     */
    public TokenSet getOpponentTokens() {
        TokenSet result = new TokenSet();

        for(Move obj: this) {
            result.with(obj.getOpponentTokens());
        }

        return result;
    }

    /**
     * Loop through the current set of Move objects and collect all contained objects with reference opponentTokens
     * pointing to the object passed as parameter.
     * 
     * @param value The object required as opponentTokens neighbor of the collected results.
     * 
     * @return Set of Token objects referring to value via opponentTokens
     */
    public MoveSet filterOpponentTokens(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        MoveSet answer = new MoveSet();

        for(Move obj: this) {
            if(!Collections.disjoint(neighbors, obj.getOpponentTokens())) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Move object passed as parameter to the
     * OpponentTokens attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their OpponentTokens
     * attributes.
     */
    public MoveSet withOpponentTokens(Token value) {
        for(Move obj: this) {
            obj.withOpponentTokens(value);
        }

        return this;
    }

    /**
     * Loop through current set of ModelType objects and remove the Move object passed as parameter from the
     * OpponentTokens attribute of each of it.
     * 
     * @return The original set of ModelType objects now without the old neighbor.
     */
    public MoveSet withoutOpponentTokens(Token value) {
        for(Move obj: this) {
            obj.withoutOpponentTokens(value);
        }

        return this;
    }

    /**
     * Loop through the current set of Move objects and collect a set of the Field objects reached via previousPosition.
     * 
     * @return Set of Field objects reachable via previousPosition
     */
    public FieldSet getPreviousPosition() {
        FieldSet result = new FieldSet();

        for(Move obj: this) {
            result.with(obj.getPreviousPosition());
        }

        return result;
    }

    /**
     * Loop through the current set of Move objects and collect all contained objects with reference previousPosition
     * pointing to the object passed as parameter.
     * 
     * @param value The object required as previousPosition neighbor of the collected results.
     * 
     * @return Set of Field objects referring to value via previousPosition
     */
    public MoveSet filterPreviousPosition(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        MoveSet answer = new MoveSet();

        for(Move obj: this) {
            if(neighbors.contains(obj.getPreviousPosition())
                    || (neighbors.isEmpty() && obj.getPreviousPosition() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Move object passed as parameter to the
     * PreviousPosition attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their PreviousPosition
     * attributes.
     */
    public MoveSet withPreviousPosition(Field value) {
        for(Move obj: this) {
            obj.withPreviousPosition(value);
        }

        return this;
    }

}
