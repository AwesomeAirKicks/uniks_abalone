package de.uniks.abalone.model.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;

import de.uniks.abalone.model.Field;
import de.uniks.networkparser.IdMap;

public class FieldPOCreator extends PatternObjectCreator {
    @Override
    public Object getSendableInstance(boolean reference) {
        if(reference) {
            return new FieldPO(new Field[] {});
        } else {
            return new FieldPO();
        }
    }

    public static IdMap createIdMap(String sessionID) {
        return de.uniks.abalone.model.util.CreatorCreator.createIdMap(sessionID);
    }
}
