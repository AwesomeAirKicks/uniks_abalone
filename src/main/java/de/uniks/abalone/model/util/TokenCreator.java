/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model.util;

import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;
import de.uniks.networkparser.IdMap;
import de.uniks.networkparser.interfaces.SendableEntityCreator;

public class TokenCreator implements SendableEntityCreator {
    private final String[] properties = new String[] {
            Token.PROPERTY_GAME,
            Token.PROPERTY_FIELD,
            Token.PROPERTY_PLAYER,
    };

    @Override
    public String[] getProperties() {
        return this.properties;
    }

    @Override
    public Object getSendableInstance(boolean reference) {
        return new Token();
    }

    @Override
    public Object getValue(Object target, String attrName) {
        int pos = attrName.indexOf('.');
        String attribute = attrName;
        if(pos > 0) {
            attribute = attrName.substring(0, pos);
        }
        if(Token.PROPERTY_GAME.equalsIgnoreCase(attribute)) {
            return ((Token) target).getGame();
        }
        if(Token.PROPERTY_FIELD.equalsIgnoreCase(attribute)) {
            return ((Token) target).getField();
        }
        if(Token.PROPERTY_PLAYER.equalsIgnoreCase(attribute)) {
            return ((Token) target).getPlayer();
        }
        return null;
    }

    @Override
    public boolean setValue(Object target, String attrName, Object value, String type) {
        if(SendableEntityCreator.REMOVE.equals(type) && value != null) {
            attrName = attrName + type;
        }
        if(Token.PROPERTY_GAME.equalsIgnoreCase(attrName)) {
            ((Token) target).setGame((Game) value);
            return true;
        }
        if(Token.PROPERTY_FIELD.equalsIgnoreCase(attrName)) {
            ((Token) target).setField((Field) value);
            return true;
        }
        if(Token.PROPERTY_PLAYER.equalsIgnoreCase(attrName)) {
            ((Token) target).setPlayer((Player) value);
            return true;
        }
        return false;
    }

    public static IdMap createIdMap(String sessionID) {
        return de.uniks.abalone.model.util.CreatorCreator.createIdMap(sessionID);
    }

    // ==========================================================================
    public void removeObject(Object entity) {
        ((Token) entity).removeYou();
    }
}
