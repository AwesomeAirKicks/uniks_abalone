package de.uniks.abalone.model.util;

import org.sdmlib.models.pattern.PatternObject;

import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Logic;
import de.uniks.abalone.model.Move;

public class LogicPO extends PatternObject<LogicPO, Logic> {
    public LogicSet allMatches() {
        setDoAllMatches(true);
        LogicSet matches = new LogicSet();
        while(getPattern().getHasMatch()) {
            matches.add(getCurrentMatch());
            getPattern().findMatch();
        }
        return matches;
    }

    public LogicPO() {
        newInstance(null);
    }

    public LogicPO(Logic... hostGraphObject) {
        if(hostGraphObject == null || hostGraphObject.length < 1) {
            return;
        }
        newInstance(null, hostGraphObject);
    }

    public LogicPO(String modifier) {
        setModifier(modifier);
    }

    public GamePO createGamePO() {
        GamePO result = new GamePO(new Game[] {});
        result.setModifier(getPattern().getModifier());
        super.hasLink(Logic.PROPERTY_GAME, result);
        return result;
    }

    public GamePO createGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});
        result.setModifier(modifier);
        super.hasLink(Logic.PROPERTY_GAME, result);
        return result;
    }

    public LogicPO createGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, Logic.PROPERTY_GAME);
    }

    public LogicPO createGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, Logic.PROPERTY_GAME, modifier);
    }

    public Game getGame() {
        if(getPattern().getHasMatch()) {
            return getCurrentMatch().getGame();
        }
        return null;
    }

    public MovePO createCurrentMovePO() {
        MovePO result = new MovePO(new Move[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Logic.PROPERTY_CURRENTMOVE, result);

        return result;
    }

    public MovePO createCurrentMovePO(String modifier) {
        MovePO result = new MovePO(new Move[] {});

        result.setModifier(modifier);
        super.hasLink(Logic.PROPERTY_CURRENTMOVE, result);

        return result;
    }

    public LogicPO createCurrentMoveLink(MovePO tgt) {
        return hasLinkConstraint(tgt, Logic.PROPERTY_CURRENTMOVE);
    }

    public LogicPO createCurrentMoveLink(MovePO tgt, String modifier) {
        return hasLinkConstraint(tgt, Logic.PROPERTY_CURRENTMOVE, modifier);
    }

    public Move getCurrentMove() {
        if(this.getPattern().getHasMatch()) {
            return ((Logic) this.getCurrentMatch()).getCurrentMove();
        }
        return null;
    }

}
