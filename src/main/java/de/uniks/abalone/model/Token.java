/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import de.uniks.networkparser.interfaces.SendableEntity;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Player;

/**
 *
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/abalone/modelgenerator/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/abalone/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/abalone/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href='../../../../../../../src/test/java/de/uniks/modelgenerator/AbaloneModel.java'>AbaloneModel.java</a>
 */
public class Token implements SendableEntity {
    // ==========================================================================
    protected PropertyChangeSupport listeners = null;

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if(this.listeners != null) {
            this.listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    @Override
    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners.removePropertyChangeListener(listener);
        }
        this.listeners.removePropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(this.listeners != null) {
            this.listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    // ==========================================================================
    public void removeYou() {
        setGame(null);
        setField(null);
        setPlayer(null);
        firePropertyChange("REMOVE_YOU", this, null);
    }

    /********************************************************************
     * <pre>
     *              many                       one
     * Token ----------------------------------- Game
     *              tokens                   game
     * </pre>
     */
    public static final String PROPERTY_GAME = "game";

    private Game game = null;

    public Game getGame() {
        return this.game;
    }

    public boolean setGame(Game value) {
        boolean changed = false;
        if(this.game != value) {
            Game oldValue = this.game;
            if(this.game != null) {
                this.game = null;
                oldValue.withoutTokens(this);
            }
            this.game = value;
            if(value != null) {
                value.withTokens(this);
            }
            firePropertyChange(PROPERTY_GAME, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Token withGame(Game value) {
        setGame(value);
        return this;
    }

    public Game createGame() {
        Game value = new Game();
        withGame(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Token ----------------------------------- Field
     *              token                   field
     * </pre>
     */
    public static final String PROPERTY_FIELD = "field";

    private Field field = null;

    public Field getField() {
        return this.field;
    }

    public boolean setField(Field value) {
        return setField(value, false);
    }

    public boolean setField(Field value, boolean simulation) {
        boolean changed = false;
        if(this.field != value) {
            Field oldValue = this.field;
            if(this.field != null) {
                this.field = null;
                oldValue.setToken(null, simulation);
            }
            this.field = value;
            if(value != null) {
                value.setToken(this, simulation);
            }
            if(!simulation) {
                firePropertyChange(PROPERTY_FIELD, oldValue, value);
            }

            changed = true;
        }
        return changed;
    }

    public Token withField(Field value) {
        setField(value);
        return this;
    }

    public Field createField() {
        Field value = new Field();
        withField(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              many                       one
     * Token ----------------------------------- Player
     *              tokens                   player
     * </pre>
     */
    public static final String PROPERTY_PLAYER = "player";

    private Player player = null;

    public Player getPlayer() {
        return this.player;
    }

    public boolean setPlayer(Player value) {
        boolean changed = false;
        if(this.player != value) {
            Player oldValue = this.player;
            if(this.player != null) {
                this.player = null;
                oldValue.withoutTokens(this);
            }
            this.player = value;
            if(value != null) {
                value.withTokens(this);
            }
            firePropertyChange(PROPERTY_PLAYER, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Token withPlayer(Player value) {
        setPlayer(value);
        return this;
    }

    public Player createPlayer() {
        Player value = new Player();
        withPlayer(value);
        return value;
    }

    @Override
    public String toString() {
        if(getField() != null) {
            return new String(getField().getX() + "" + getField().getY());
        }
        return new String();
    }
}
