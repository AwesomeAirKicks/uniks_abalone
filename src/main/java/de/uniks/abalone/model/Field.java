/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import de.uniks.abalone.model.util.FieldSet;
import de.uniks.networkparser.interfaces.SendableEntity;

/**
 *
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/abalone/modelgenerator/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/abalone/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href='../../../../../../../src/test/java/de/uniks/modelgenerator/AbaloneModel.java'>AbaloneModel.java</a>
 */
public class Field implements SendableEntity {
    // ==========================================================================
    protected PropertyChangeSupport listeners = null;

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if(this.listeners != null) {
            this.listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    @Override
    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners.removePropertyChangeListener(listener);
        }
        this.listeners.removePropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(this.listeners != null) {
            this.listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    // ==========================================================================
    public void removeYou() {
        setGame(null);
        setBoard(null);
        setToken(null);
        setRight(null);
        setLeft(null);
        setDownLeft(null);
        setTopRight(null);
        setDownRight(null);
        setTopLeft(null);
        firePropertyChange("REMOVE_YOU", this, null);
    }

    // ==========================================================================
    public static final String PROPERTY_X = "x";

    private int x;

    public int getX() {
        return this.x;
    }

    public void setX(int value) {
        if(this.x != value) {
            int oldValue = this.x;
            this.x = value;
            firePropertyChange(PROPERTY_X, oldValue, value);
        }
    }

    public Field withX(int value) {
        setX(value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(" ").append(getX());
        result.append(" ").append(getY());
        return result.substring(1);
    }

    // ==========================================================================
    public static final String PROPERTY_Y = "y";

    private int y;

    public int getY() {
        return this.y;
    }

    public void setY(int value) {
        if(this.y != value) {
            int oldValue = this.y;
            this.y = value;
            firePropertyChange(PROPERTY_Y, oldValue, value);
        }
    }

    public Field withY(int value) {
        setY(value);
        return this;
    }

    /********************************************************************
     * <pre>
     *              many                       one
     * Field ----------------------------------- Game
     *              fields                   game
     * </pre>
     */
    public static final String PROPERTY_GAME = "game";

    private Game game = null;

    public Game getGame() {
        return this.game;
    }

    public boolean setGame(Game value) {
        boolean changed = false;
        if(this.game != value) {
            Game oldValue = this.game;
            if(this.game != null) {
                this.game = null;
                oldValue.withoutFields(this);
            }
            this.game = value;
            if(value != null) {
                value.withFields(this);
            }
            firePropertyChange(PROPERTY_GAME, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Field withGame(Game value) {
        setGame(value);
        return this;
    }

    public Game createGame() {
        Game value = new Game();
        withGame(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              many                       one
     * Field ----------------------------------- Board
     *              fields                   board
     * </pre>
     */
    public static final String PROPERTY_BOARD = "board";

    private Board board = null;

    public Board getBoard() {
        return this.board;
    }

    public boolean setBoard(Board value) {
        boolean changed = false;
        if(this.board != value) {
            Board oldValue = this.board;
            if(this.board != null) {
                this.board = null;
                oldValue.withoutFields(this);
            }
            this.board = value;
            if(value != null) {
                value.withFields(this);
            }
            firePropertyChange(PROPERTY_BOARD, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Field withBoard(Board value) {
        setBoard(value);
        return this;
    }

    public Board createBoard() {
        Board value = new Board();
        withBoard(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Field ----------------------------------- Token
     *              field                   token
     * </pre>
     */
    public static final String PROPERTY_TOKEN = "token";

    private Token token = null;

    public Token getToken() {
        return this.token;
    }

    public boolean setToken(Token value) {
        return setToken(value, false);
    }

    public boolean setToken(Token value, boolean simulation) {
        boolean changed = false;
        if(this.token != value) {
            Token oldValue = this.token;
            if(this.token != null) {
                this.token = null;
                oldValue.setField(null, simulation);
            }
            this.token = value;
            if(value != null) {
                value.setField(this, simulation);
            }
            if(!simulation) {
                firePropertyChange(PROPERTY_TOKEN, oldValue, value);
            }
            changed = true;
        }
        return changed;
    }

    public Field withToken(Token value) {
        setToken(value);
        return this;
    }

    public Token createToken() {
        Token value = new Token();
        withToken(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Field ----------------------------------- Field
     *              left                   right
     * </pre>
     */
    public static final String PROPERTY_RIGHT = "right";

    private Field right = null;

    public Field getRight() {
        return this.right;
    }

    public FieldSet getRightTransitive() {
        FieldSet result = new FieldSet().with(this);
        return result.getRightTransitive();
    }

    public boolean setRight(Field value) {
        boolean changed = false;
        if(this.right != value) {
            Field oldValue = this.right;
            if(this.right != null) {
                this.right = null;
                oldValue.setLeft(null);
            }
            this.right = value;
            if(value != null) {
                value.withLeft(this);
            }
            firePropertyChange(PROPERTY_RIGHT, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Field withRight(Field value) {
        setRight(value);
        return this;
    }

    public Field createRight() {
        Field value = new Field();
        withRight(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Field ----------------------------------- Field
     *              right                   left
     * </pre>
     */
    public static final String PROPERTY_LEFT = "left";

    private Field left = null;

    public Field getLeft() {
        return this.left;
    }

    public FieldSet getLeftTransitive() {
        FieldSet result = new FieldSet().with(this);
        return result.getLeftTransitive();
    }

    public boolean setLeft(Field value) {
        boolean changed = false;
        if(this.left != value) {
            Field oldValue = this.left;
            if(this.left != null) {
                this.left = null;
                oldValue.setRight(null);
            }
            this.left = value;
            if(value != null) {
                value.withRight(this);
            }
            firePropertyChange(PROPERTY_LEFT, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Field withLeft(Field value) {
        setLeft(value);
        return this;
    }

    public Field createLeft() {
        Field value = new Field();
        withLeft(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Field ----------------------------------- Field
     *              topRight                   downLeft
     * </pre>
     */
    public static final String PROPERTY_DOWNLEFT = "downLeft";

    private Field downLeft = null;

    public Field getDownLeft() {
        return this.downLeft;
    }

    public FieldSet getDownLeftTransitive() {
        FieldSet result = new FieldSet().with(this);
        return result.getDownLeftTransitive();
    }

    public boolean setDownLeft(Field value) {
        boolean changed = false;
        if(this.downLeft != value) {
            Field oldValue = this.downLeft;
            if(this.downLeft != null) {
                this.downLeft = null;
                oldValue.setTopRight(null);
            }
            this.downLeft = value;
            if(value != null) {
                value.withTopRight(this);
            }
            firePropertyChange(PROPERTY_DOWNLEFT, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Field withDownLeft(Field value) {
        setDownLeft(value);
        return this;
    }

    public Field createDownLeft() {
        Field value = new Field();
        withDownLeft(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Field ----------------------------------- Field
     *              downLeft                   topRight
     * </pre>
     */
    public static final String PROPERTY_TOPRIGHT = "topRight";

    private Field topRight = null;

    public Field getTopRight() {
        return this.topRight;
    }

    public FieldSet getTopRightTransitive() {
        FieldSet result = new FieldSet().with(this);
        return result.getTopRightTransitive();
    }

    public boolean setTopRight(Field value) {
        boolean changed = false;
        if(this.topRight != value) {
            Field oldValue = this.topRight;
            if(this.topRight != null) {
                this.topRight = null;
                oldValue.setDownLeft(null);
            }
            this.topRight = value;
            if(value != null) {
                value.withDownLeft(this);
            }
            firePropertyChange(PROPERTY_TOPRIGHT, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Field withTopRight(Field value) {
        setTopRight(value);
        return this;
    }

    public Field createTopRight() {
        Field value = new Field();
        withTopRight(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Field ----------------------------------- Field
     *              topLeft                   downRight
     * </pre>
     */
    public static final String PROPERTY_DOWNRIGHT = "downRight";

    private Field downRight = null;

    public Field getDownRight() {
        return this.downRight;
    }

    public FieldSet getDownRightTransitive() {
        FieldSet result = new FieldSet().with(this);
        return result.getDownRightTransitive();
    }

    public boolean setDownRight(Field value) {
        boolean changed = false;
        if(this.downRight != value) {
            Field oldValue = this.downRight;
            if(this.downRight != null) {
                this.downRight = null;
                oldValue.setTopLeft(null);
            }
            this.downRight = value;
            if(value != null) {
                value.withTopLeft(this);
            }
            firePropertyChange(PROPERTY_DOWNRIGHT, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Field withDownRight(Field value) {
        setDownRight(value);
        return this;
    }

    public Field createDownRight() {
        Field value = new Field();
        withDownRight(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Field ----------------------------------- Field
     *              downRight                   topLeft
     * </pre>
     */
    public static final String PROPERTY_TOPLEFT = "topLeft";

    private Field topLeft = null;

    public Field getTopLeft() {
        return this.topLeft;
    }

    public FieldSet getTopLeftTransitive() {
        FieldSet result = new FieldSet().with(this);
        return result.getTopLeftTransitive();
    }

    public boolean setTopLeft(Field value) {
        boolean changed = false;
        if(this.topLeft != value) {
            Field oldValue = this.topLeft;
            if(this.topLeft != null) {
                this.topLeft = null;
                oldValue.setDownRight(null);
            }
            this.topLeft = value;
            if(value != null) {
                value.withDownRight(this);
            }
            firePropertyChange(PROPERTY_TOPLEFT, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Field withTopLeft(Field value) {
        setTopLeft(value);
        return this;
    }

    public Field createTopLeft() {
        Field value = new Field();
        withTopLeft(value);
        return value;
    }

    // ==========================================================================

    public static final String PROPERTY_SELECTED = "selected";

    private boolean selected;

    public boolean isSelected() {
        return this.selected;
    }

    public void setSelected(boolean value) {
        if(this.selected != value) {

            boolean oldValue = this.selected;
            this.selected = value;
            this.firePropertyChange(PROPERTY_SELECTED, oldValue, value);
        }
    }

    public Field withSelected(boolean value) {
        setSelected(value);
        return this;
    }

    // ==============================================================================

    public Field getNeighbor(int i) {

        Field returnField = null;

        switch(i) {
            case 0: {
                returnField = getTopRight();
                break;
            }
            case 1: {
                returnField = getRight();
                break;
            }
            case 2: {
                returnField = getDownRight();
                break;
            }
            case 3: {
                returnField = getDownLeft();
                break;
            }
            case 4: {
                returnField = getLeft();
                break;
            }
            case 5: {
                returnField = getTopLeft();
                break;
            }

        }
        return returnField;
    }

    // ==========================================================================

    public static final String PROPERTY_LASTMOVED = "lastMoved";

    private boolean lastMoved;

    public boolean isLastMoved() {
        return this.lastMoved;
    }

    public void setLastMoved(boolean value) {
        if(this.lastMoved != value) {

            boolean oldValue = this.lastMoved;
            this.lastMoved = value;
            this.firePropertyChange(PROPERTY_LASTMOVED, oldValue, value);
        }
    }

    public Field withLastMoved(boolean value) {
        setLastMoved(value);
        return this;
    }
}
