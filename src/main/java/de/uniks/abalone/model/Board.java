/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.BitSet;

import de.uniks.abalone.model.util.FieldSet;
import de.uniks.abalone.tools.Tools;
import de.uniks.ai.BoardInterface;
import de.uniks.networkparser.interfaces.SendableEntity;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Field;

/**
 *
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/abalone/modelgenerator/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/abalone/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href='../../../../../../../src/test/java/de/uniks/modelgenerator/AbaloneModel.java'>AbaloneModel.java</a>
 */
public class Board implements SendableEntity, BoardInterface {
    // ==========================================================================
    protected PropertyChangeSupport listeners = null;

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if(this.listeners != null) {
            this.listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    @Override
    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners.removePropertyChangeListener(listener);
        }
        this.listeners.removePropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(this.listeners != null) {
            this.listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    // ==========================================================================
    public void removeYou() {
        setGame(null);
        withoutFields(getFields().toArray(new Field[getFields().size()]));
        firePropertyChange("REMOVE_YOU", this, null);
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Board ----------------------------------- Game
     *              board                   game
     * </pre>
     */
    public static final String PROPERTY_GAME = "game";

    private Game game = null;

    public Game getGame() {
        return this.game;
    }

    public boolean setGame(Game value) {
        boolean changed = false;
        if(this.game != value) {
            Game oldValue = this.game;
            if(this.game != null) {
                this.game = null;
                oldValue.setBoard(null);
            }
            this.game = value;
            if(value != null) {
                value.withBoard(this);
            }
            firePropertyChange(PROPERTY_GAME, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Board withGame(Game value) {
        setGame(value);
        return this;
    }

    public Game createGame() {
        Game value = new Game();
        withGame(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       many
     * Board ----------------------------------- Field
     *              board                   fields
     * </pre>
     */
    public static final String PROPERTY_FIELDS = "fields";

    private FieldSet fields = null;

    public FieldSet getFields() {
        if(this.fields == null) {
            return FieldSet.EMPTY_SET;
        }
        return this.fields;
    }

    public Board withFields(Field... value) {
        if(value == null) {
            return this;
        }
        for(Field item: value) {
            if(item != null) {
                if(this.fields == null) {
                    this.fields = new FieldSet();
                }
                boolean changed = this.fields.add(item);
                if(changed) {
                    item.withBoard(this);
                    firePropertyChange(PROPERTY_FIELDS, null, item);
                }
            }
        }
        return this;
    }

    public Board withoutFields(Field... value) {
        for(Field item: value) {
            if((this.fields != null) && (item != null)) {
                if(this.fields.remove(item)) {
                    item.setBoard(null);
                    firePropertyChange(PROPERTY_FIELDS, item, null);
                }
            }
        }
        return this;
    }

    public Field createFields() {
        Field value = new Field();
        withFields(value);
        return value;
    }

    /**
     * Initializes the board. At first the board structure will be created. Afterwards every player gets his tokens.
     * 
     * @param first Player 1
     * @param second Player 2
     * @param tokenLayout The chosen initial board layout.
     */
    public void init(Player first, Player second, int[][] tokenLayout) {
        buildFields();
        buildTokens(first, second, tokenLayout);
    }

    /**
     * Creates every field and connects them to the appropriate neighbors.
     */
    public void buildFields() {
        int size = 9;
        Field a[][] = new Field[size][size];
        int maxY = size - 1; // 8
        int xStart = size / 2; // 4
        int xEnd = size - 1; // 8
        for(int y = 0; y <= maxY; y++) {
            Field leftField = null;
            for(int x = xStart; x <= xEnd; x++) {
                Field f = new Field().withX(x).withY(y);
                a[y][x] = f;
                if(leftField != null) {
                    f.withLeft(leftField);
                }
                leftField = f;
            }
            if(y < size / 2) {
                xStart--;
            }
            if(y >= size / 2) {
                xEnd--;
            }
        }

        int y = 8;
        int x = 0;
        while(y - 1 >= 0 && a[y - 1][x] != null) {
            a[y][x].setTopLeft(a[y - 1][x]);
            y--;
        }

        y = 8;
        x = 1;
        while(y - 1 >= 0 && a[y - 1][x] != null) {
            a[y][x].setTopLeft(a[y - 1][x]);
            y--;
        }

        y = 8;
        x = 2;
        while(y - 1 >= 0 && a[y - 1][x] != null) {
            a[y][x].setTopLeft(a[y - 1][x]);
            y--;
        }

        y = 8;
        x = 3;
        while(y - 1 >= 0 && a[y - 1][x] != null) {
            a[y][x].setTopLeft(a[y - 1][x]);
            y--;
        }

        y = 8;
        x = 4;
        while(y - 1 >= 0 && a[y - 1][x] != null) {
            a[y][x].setTopLeft(a[y - 1][x]);
            y--;
        }

        y = 7;
        x = 5;
        while(y - 1 >= 0 && a[y - 1][x] != null) {
            a[y][x].setTopLeft(a[y - 1][x]);
            y--;
        }

        y = 6;
        x = 6;
        while(y - 1 >= 0 && a[y - 1][x] != null) {
            a[y][x].setTopLeft(a[y - 1][x]);
            y--;
        }

        y = 5;
        x = 7;
        while(y - 1 >= 0 && a[y - 1][x] != null) {
            a[y][x].setTopLeft(a[y - 1][x]);
            y--;
        }

        y = 4;
        x = 8;
        while(y - 1 >= 0 && a[y - 1][x] != null) {
            a[y][x].setTopLeft(a[y - 1][x]);
            y--;
        }

        // topRight and downLeft
        y = 4;
        x = 0;
        while(y - 1 >= 0 && x + 1 <= 8) {
            a[y][x].setTopRight(a[y - 1][x + 1]);
            y--;
            x++;
        }

        y = 5;
        x = 0;
        while(y - 1 >= 0 && x + 1 <= 8) {
            a[y][x].setTopRight(a[y - 1][x + 1]);
            y--;
            x++;
        }

        y = 6;
        x = 0;
        while(y - 1 >= 0 && x + 1 <= 8) {
            a[y][x].setTopRight(a[y - 1][x + 1]);
            y--;
            x++;
        }

        y = 7;
        x = 0;
        while(y - 1 >= 0 && x + 1 <= 8) {
            a[y][x].setTopRight(a[y - 1][x + 1]);
            y--;
            x++;
        }

        y = 8;
        x = 0;
        while(y - 1 >= 0 && x + 1 <= 8) {
            a[y][x].setTopRight(a[y - 1][x + 1]);
            y--;
            x++;
        }

        y = 8;
        x = 1;
        while(y - 1 >= 0 && x + 1 <= 8) {
            a[y][x].setTopRight(a[y - 1][x + 1]);
            y--;
            x++;
        }

        y = 8;
        x = 2;
        while(y - 1 >= 0 && x + 1 <= 8) {
            a[y][x].setTopRight(a[y - 1][x + 1]);
            y--;
            x++;
        }

        y = 8;
        x = 3;
        while(y - 1 >= 0 && x + 1 <= 8) {
            a[y][x].setTopRight(a[y - 1][x + 1]);
            y--;
            x++;
        }

        y = 8;
        x = 4;
        while(y - 1 >= 0 && x + 1 <= 8) {
            a[y][x].setTopRight(a[y - 1][x + 1]);
            y--;
            x++;
        }

        // add it to SDMLIB set
        for(Field[] element: a) {
            for(Field element2: element) {
                if(element2 != null) {
                    element2.setGame(getGame());
                    withFields(element2);
                }
            }
        }
    }

    /**
     * Depending on the passed token layout every player gets his tokens created and placed on the board.
     *
     * @param first Player 1
     * @param second Player 2
     * @param tokenLayout The chosen initial board layout.
     */
    public void buildTokens(Player first, Player second, int[][] tokenLayout) {
        Field[][] fields = getFieldsAs2DimArray();
        for(int i = 0; i < tokenLayout.length; i++) {
            for(int j = 0; j < tokenLayout[i].length; j++) {
                if(tokenLayout[i][j] == 1) {
                    new Token().withField(fields[i][j]).withPlayer(first).withGame(first.getGame());
                } else if(tokenLayout[i][j] == 2) {
                    new Token().withField(fields[i][j]).withPlayer(second).withGame(second.getGame());
                }
            }
        }
    }

    public Field[][] getFieldsAs2DimArray() {
        Field[][] a = new Field[9][9];
        for(Field f: getFields()) {
            a[f.getY()][f.getX()] = f;
        }
        return a;
    }

    /**
     * Creates a console output of the current board in a squared array shape.
     */
    public void print2DimArray() {
        Field[][] a = getFieldsAs2DimArray();

        System.out.println("--------------------------------------------");
        System.out.println("--    row = y       col = x      a[y][x]  --");
        System.out.println("--------------------------------------------");
        System.out.println("  0 1 2 3 4 5 6 7 8");

        for(int y = 0; y < a.length; y++) {
            System.out.print(y);
            for(int x = 0; x < a[y].length; x++) {
                System.out.print(' ');
                if(a[y][x] != null) {
                    if(a[y][x].getToken() != null) {
                        System.out.print(a[y][x].getToken().getPlayer().getColor().toUpperCase().charAt(0));
                    } else {
                        System.out.print('O');
                    }
                } else {
                    System.out.print(' ');
                }
            }
            System.out.print("\n");
        }
        System.out.println("  0 1 2 3 4 5 6 7 8");

        System.out.println("--------------------------------------------");
    }

    /**
     * Creates a console output of the current board in the original hexagonal shape.
     */
    public void printBoard() {
        Field[][] a = getFieldsAs2DimArray();

        System.out.println("--------------------------------------------");
        System.out.println("--    row = y       dia = x      a[y][x]  --");
        System.out.println("--------------------------------------------");

        for(int y = 0; y < a.length; y++) {
            boolean printSpace = true;
            System.out.print(y);
            for(int spaces = 0; spaces < y; spaces++) {
                System.out.print(' ');
            }
            for(int x = 0; x < a[y].length; x++) {
                if(a[y][x] != null) {
                    if(a[y][x].getToken() != null) {
                        System.out.print(" " + a[y][x].getToken().getPlayer().getColor().toUpperCase().charAt(0));
                        printSpace = false;
                    } else {
                        System.out.print(" O");
                        printSpace = false;
                    }
                } else if(printSpace) {
                    System.out.print("  ");
                }
            }
            if(y >= 5) {
                System.out.print(" " + (13 - y));
            }
            System.out.print("\n");
        }
        System.out.println("           0 1 2 3 4");

        System.out.println("--------------------------------------------");
    }

    /**
     * Due to the symmetry of the board and the moves, the AI is able to drastically reduce all possible moves by
     * rotating and/or mirroring the board before it creates the regarding hash value.
     * <p>
     * In our case the board will be rotated that the most black tokens are in the lower center area. Afterwards it may
     * be necessary to mirror the board that the most white tokens are in the right half of the board. Statistically we
     * should be able to reduce the total number of possible situations by roughly 90%.
     */
    @Override
    public BitSet encode() {
        // ========== Phase 1: Normalization of the board ==========
        // Compute in which sixth are the most of player2's tokens
        // count[i] = sector_i
        int[] count = new int[6];

        Player player2 = getGame().getPlayers().last();
        FieldSet fields = player2.getTokens().filter(token -> {
            return token.getField() != null;
        }).getField();

        for(Field f: fields) {
            int y = f.getY();
            int x = f.getX();

            switch(y) {
                case 0:
                    if(x <= 4) {
                        count[2]++;
                    }
                    if(x >= 4 && x <= 8) {
                        count[3]++;
                    }
                    if(x >= 8) {
                        count[4]++;
                    }
                    break;
                case 1:
                    if(x <= 4) {
                        count[2]++;
                    }
                    if(x >= 4 && x <= 7) {
                        count[3]++;
                    }
                    if(x >= 7) {
                        count[4]++;
                    }
                    break;
                case 2:
                    if(x <= 4) {
                        count[2]++;
                    }
                    if(x >= 4 && x <= 6) {
                        count[3]++;
                    }
                    if(x >= 6) {
                        count[4]++;
                    }
                    break;
                case 3:
                    if(x <= 4) {
                        count[2]++;
                    }
                    if(x >= 4 && x <= 5) {
                        count[3]++;
                    }
                    if(x >= 5) {
                        count[4]++;
                    }
                    break;
                case 4:
                    if(x < 4) {
                        count[2]++;
                        count[1]++;
                    }
                    if(x > 4) {
                        count[4]++;
                        count[5]++;
                    }
                    break;
                case 5:
                    if(x <= 3) {
                        count[1]++;
                    }
                    if(x >= 3 && x <= 4) {
                        count[0]++;
                    }
                    if(x >= 4) {
                        count[5]++;
                    }
                    break;
                case 6:
                    if(x <= 2) {
                        count[1]++;
                    }
                    if(x >= 2 && x <= 4) {
                        count[0]++;
                    }
                    if(x >= 4) {
                        count[5]++;
                    }
                    break;
                case 7:
                    if(x <= 1) {
                        count[1]++;
                    }
                    if(x >= 1 && x <= 4) {
                        count[0]++;
                    }
                    if(x >= 4) {
                        count[5]++;
                    }
                    break;
                case 8:
                    if(x <= 0) {
                        count[1]++;
                    }
                    if(x >= 0 && x <= 4) {
                        count[0]++;
                    }
                    if(x >= 4) {
                        count[5]++;
                    }
                    break;
            }
        }
        int max = count[0];
        String code = "0";
        for(int i = 1; i <= 5; i++) {
            if(count[i] > max) {
                max = count[i];
                code = Integer.toString(i);
            }
        }

        // Compute in which half (orthogonal to the previous calculated sector) are the most of player1's tokens
        // count[0] = left, count[1] = right
        count = new int[2];

        Player player1 = player2.getNext();
        fields = player1.getTokens().filter(token -> {
            return token.getField() != null;
        }).getField();
        for(Field f: fields) {
            int y = f.getY();
            int x = f.getX();

            switch(code) {
                case "0":
                    switch(y) {
                        case 0:
                            if(x <= 6) {
                                count[0]++;
                            }
                            if(x >= 6) {
                                count[1]++;
                            }
                            break;
                        case 1:
                            if(x <= 5) {
                                count[0]++;
                            } else {
                                count[1]++;
                            }
                            break;
                        case 2:
                            if(x <= 5) {
                                count[0]++;
                            }
                            if(x >= 5) {
                                count[1]++;
                            }
                            break;
                        case 3:
                            if(x <= 4) {
                                count[0]++;
                            } else {
                                count[1]++;
                            }
                            break;
                        case 4:
                            if(x <= 4) {
                                count[0]++;
                            }
                            if(x >= 4) {
                                count[1]++;
                            }
                            break;
                        case 5:
                            if(x <= 3) {
                                count[0]++;
                            } else {
                                count[1]++;
                            }
                            break;
                        case 6:
                            if(x <= 3) {
                                count[0]++;
                            }
                            if(x >= 3) {
                                count[1]++;
                            }
                            break;
                        case 7:
                            if(x <= 2) {
                                count[0]++;
                            } else {
                                count[1]++;
                            }
                            break;
                        case 8:
                            if(x <= 2) {
                                count[0]++;
                            }
                            if(x >= 2) {
                                count[1]++;
                            }
                            break;
                    }
                    break;
                case "1":
                    switch(x) {
                        case 0:
                            if(y <= 6) {
                                count[0]++;
                            }
                            if(y >= 6) {
                                count[1]++;
                            }
                            break;
                        case 1:
                            if(y <= 5) {
                                count[0]++;
                            } else {
                                count[1]++;
                            }
                            break;
                        case 2:
                            if(y <= 5) {
                                count[0]++;
                            }
                            if(y >= 5) {
                                count[1]++;
                            }
                            break;
                        case 3:
                            if(y <= 4) {
                                count[0]++;
                            } else {
                                count[1]++;
                            }
                            break;
                        case 4:
                            if(y <= 4) {
                                count[0]++;
                            }
                            if(y >= 4) {
                                count[1]++;
                            }
                            break;
                        case 5:
                            if(y <= 3) {
                                count[0]++;
                            } else {
                                count[1]++;
                            }
                            break;
                        case 6:
                            if(y <= 3) {
                                count[0]++;
                            }
                            if(y >= 3) {
                                count[1]++;
                            }
                            break;
                        case 7:
                            if(y <= 2) {
                                count[0]++;
                            } else {
                                count[1]++;
                            }
                            break;
                        case 8:
                            if(y <= 2) {
                                count[0]++;
                            }
                            if(y >= 2) {
                                count[1]++;
                            }
                            break;
                    }
                    break;
                case "2":
                    switch(y) {
                        case 0:
                            count[0]++;
                            break;
                        case 1:
                            count[0]++;
                            break;
                        case 2:
                            count[0]++;
                            if(x == 2) {
                                count[1]++;
                            }
                            break;
                        case 3:
                            if(x <= 3) {
                                count[1]++;
                            }
                            if(x >= 3) {
                                count[0]++;
                            }
                            break;
                        case 4:
                            if(x <= 4) {
                                count[1]++;
                            }
                            if(x >= 4) {
                                count[0]++;
                            }
                            break;
                        case 5:
                            if(x <= 5) {
                                count[1]++;
                            }
                            if(x >= 5) {
                                count[0]++;
                            }
                            break;
                        case 6:
                            count[1]++;
                            if(x == 6) {
                                count[0]++;
                            }
                            break;
                        case 7:
                            count[1]++;
                            break;
                        case 8:
                            count[1]++;
                            break;
                    }
                    break;
                case "3":
                    switch(y) {
                        case 0:
                            if(x <= 6) {
                                count[1]++;
                            }
                            if(x >= 6) {
                                count[0]++;
                            }
                            break;
                        case 1:
                            if(x <= 5) {
                                count[1]++;
                            } else {
                                count[0]++;
                            }
                            break;
                        case 2:
                            if(x <= 5) {
                                count[1]++;
                            }
                            if(x >= 5) {
                                count[0]++;
                            }
                            break;
                        case 3:
                            if(x <= 4) {
                                count[1]++;
                            } else {
                                count[0]++;
                            }
                            break;
                        case 4:
                            if(x <= 4) {
                                count[1]++;
                            }
                            if(x >= 4) {
                                count[0]++;
                            }
                            break;
                        case 5:
                            if(x <= 3) {
                                count[1]++;
                            } else {
                                count[0]++;
                            }
                            break;
                        case 6:
                            if(x <= 3) {
                                count[1]++;
                            }
                            if(x >= 3) {
                                count[0]++;
                            }
                            break;
                        case 7:
                            if(x <= 2) {
                                count[1]++;
                            } else {
                                count[0]++;
                            }
                            break;
                        case 8:
                            if(x <= 2) {
                                count[1]++;
                            }
                            if(x >= 2) {
                                count[0]++;
                            }
                            break;
                    }
                    break;
                case "4":
                    switch(x) {
                        case 0:
                            if(y <= 6) {
                                count[1]++;
                            }
                            if(y >= 6) {
                                count[0]++;
                            }
                            break;
                        case 1:
                            if(y <= 5) {
                                count[1]++;
                            } else {
                                count[0]++;
                            }
                            break;
                        case 2:
                            if(y <= 5) {
                                count[1]++;
                            }
                            if(y >= 5) {
                                count[0]++;
                            }
                            break;
                        case 3:
                            if(y <= 4) {
                                count[1]++;
                            } else {
                                count[0]++;
                            }
                            break;
                        case 4:
                            if(y <= 4) {
                                count[1]++;
                            }
                            if(y >= 4) {
                                count[0]++;
                            }
                            break;
                        case 5:
                            if(y <= 3) {
                                count[1]++;
                            } else {
                                count[0]++;
                            }
                            break;
                        case 6:
                            if(y <= 3) {
                                count[1]++;
                            }
                            if(y >= 3) {
                                count[0]++;
                            }
                            break;
                        case 7:
                            if(y <= 2) {
                                count[1]++;
                            } else {
                                count[0]++;
                            }
                            break;
                        case 8:
                            if(y <= 2) {
                                count[1]++;
                            }
                            if(y >= 2) {
                                count[0]++;
                            }
                            break;
                    }
                    break;
                case "5":
                    switch(y) {
                        case 0:
                            count[1]++;
                            break;
                        case 1:
                            count[1]++;
                            break;
                        case 2:
                            count[1]++;
                            if(x == 2) {
                                count[0]++;
                            }
                            break;
                        case 3:
                            if(x <= 3) {
                                count[0]++;
                            }
                            if(x >= 3) {
                                count[1]++;
                            }
                            break;
                        case 4:
                            if(x <= 4) {
                                count[0]++;
                            }
                            if(x >= 4) {
                                count[1]++;
                            }
                            break;
                        case 5:
                            if(x <= 5) {
                                count[0]++;
                            }
                            if(x >= 5) {
                                count[1]++;
                            }
                            break;
                        case 6:
                            count[0]++;
                            if(x == 6) {
                                count[1]++;
                            }
                            break;
                        case 7:
                            count[0]++;
                            break;
                        case 8:
                            count[0]++;
                            break;
                    }
                    break;
            }
        }
        if(count[0] > count[1]) {
            code += "1";
        } else {
            code += "0";
        }

        int[][] x_values = Tools.getXOp(code);
        int[][] y_values = Tools.getYOp(code);

        // ========== Create the hash value ==========
        BitSet hashValue = new BitSet(122);
        int index = 0;
        for(Field field: getFields()) {
            // Calculate which field would be there after the transposition and use this instead
            int transX = x_values[field.getY()][field.getX()];
            int transY = y_values[field.getY()][field.getX()];
            Field transField = getFields().filterX(transX).filterY(transY).first();
            if(transField.getToken() != null) {
                if(transField.getToken().getPlayer().equals(player1)) {
                    // 01 for player1
                    hashValue.set(index++, true);
                    hashValue.set(index++, false);
                } else {
                    // 10 for player2
                    hashValue.set(index++, false);
                    hashValue.set(index++, true);
                }
            } else {
                // 00 for empty fields
                hashValue.set(index++, false);
                hashValue.set(index++, false);
            }
        }
        return hashValue;
    }
}
