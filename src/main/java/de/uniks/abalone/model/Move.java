/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import de.uniks.abalone.model.util.TokenSet;
import de.uniks.ai.MoveInterface;
import de.uniks.networkparser.interfaces.SendableEntity;

/**
 *
 * @param <M>
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/abalone/modelgenerator/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/abalone/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href='../../../../../../../src/test/java/de/uniks/modelgenerator/AbaloneModel.java'>AbaloneModel.java</a>
 */
public class Move implements SendableEntity, MoveInterface<Move> {
    // ==========================================================================
    public static final int TOP_RIGHT = 0;

    public static final int RIGHT = 1;

    public static final int DOWN_RIGHT = 2;

    public static final int DOWN_LEFT = 3;

    public static final int LEFT = 4;

    public static final int TOP_LEFT = 5;

    // ==========================================================================
    protected PropertyChangeSupport listeners = null;

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if(this.listeners != null) {
            this.listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    @Override
    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners.removePropertyChangeListener(listener);
        }
        this.listeners.removePropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(this.listeners != null) {
            this.listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    // ==========================================================================
    public void removeYou() {
        withoutTokens(getTokens().toArray(new Token[getTokens().size()]));
        withoutOpponentTokens(this.getOpponentTokens().toArray(new Token[this.getOpponentTokens().size()]));
        setPreviousPosition(null);
        firePropertyChange("REMOVE_YOU", this, null);
    }

    // ==========================================================================
    public static final String PROPERTY_DIRECTION = "direction";

    private int direction;

    public int getDirection() {
        return this.direction;
    }

    public void setDirection(int value) {
        if(this.direction != value) {
            int oldValue = this.direction;
            this.direction = value;
            firePropertyChange(PROPERTY_DIRECTION, oldValue, value);
        }
    }

    public Move withDirection(int value) {
        setDirection(value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(" (");
        if(this.tokens != null) {
            for(Token token: this.tokens) {
                result.append(token + ",");
            }
        }
        if(this.opponentTokens != null) {
            for(Token token: this.opponentTokens) {
                result.append(token + ",");
            }
        }
        switch(getDirection()) {
            case 0: {
                result.append("Top-Right");
                break;
            }
            case 1: {
                result.append("Right");
                break;
            }
            case 2: {
                result.append("Down-Right");
                break;
            }
            case 3: {
                result.append("Down-Left");
                break;
            }
            case 4: {
                result.append("Left");
                break;
            }
            case 5: {
                result.append("Top-Left");
                break;
            }
        }
        result.append(this.previousPosition != null ? ", X)" : ")");
        return result.substring(1);
    }

    /********************************************************************
     * <pre>
     *              one                       many
     * Move ----------------------------------- Token
     *              move                   tokens
     * </pre>
     */
    public static final String PROPERTY_TOKENS = "tokens";

    private TokenSet tokens = null;

    public TokenSet getTokens() {
        if(this.tokens == null) {
            return TokenSet.EMPTY_SET;
        }
        return this.tokens;
    }

    public Move withTokens(Token... value) {
        if(value == null) {
            return this;
        }
        for(Token item: value) {
            if(item != null) {
                if(this.tokens == null) {
                    this.tokens = new TokenSet();
                }
                boolean changed = this.tokens.add(item);
                if(changed) {
                    firePropertyChange(PROPERTY_TOKENS, null, item);
                }
            }
        }
        return this;
    }

    public Move withoutTokens(Token... value) {
        for(Token item: value) {
            if((this.tokens != null) && (item != null)) {
                if(this.tokens.remove(item)) {
                    firePropertyChange(PROPERTY_TOKENS, item, null);
                }
            }
        }
        return this;
    }

    public Token createTokens() {
        Token value = new Token();
        withTokens(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       many
     * Move ----------------------------------- Token
     *              move                   opponentTokens
     * </pre>
     */
    public static final String PROPERTY_OPPONENTTOKENS = "opponentTokens";

    private TokenSet opponentTokens = null;

    public TokenSet getOpponentTokens() {
        if(this.opponentTokens == null) {
            return TokenSet.EMPTY_SET;
        }
        return this.opponentTokens;
    }

    public Move withOpponentTokens(Token... value) {
        if(value == null) {
            return this;
        }
        for(Token item: value) {
            if(item != null) {
                if(this.opponentTokens == null) {
                    this.opponentTokens = new TokenSet();
                }
                boolean changed = this.opponentTokens.add(item);
                if(changed) {
                    firePropertyChange(PROPERTY_OPPONENTTOKENS, null, item);
                }
            }
        }
        return this;
    }

    public Move withoutOpponentTokens(Token... value) {
        for(Token item: value) {
            if((this.opponentTokens != null) && (item != null)) {
                if(this.opponentTokens.remove(item)) {
                    firePropertyChange(PROPERTY_OPPONENTTOKENS, item, null);
                }
            }
        }
        return this;
    }

    public Token createOpponentTokens() {
        Token value = new Token();
        withOpponentTokens(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Move ----------------------------------- Field
     *              move                   previousPosition
     * </pre>
     */
    public static final String PROPERTY_PREVIOUSPOSITION = "previousPosition";

    private Field previousPosition = null;

    public Field getPreviousPosition() {
        return this.previousPosition;
    }

    public boolean setPreviousPosition(Field value) {
        boolean changed = false;
        if(this.previousPosition != value) {
            Field oldValue = this.previousPosition;
            this.previousPosition = value;
            firePropertyChange(PROPERTY_PREVIOUSPOSITION, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Move withPreviousPosition(Field value) {
        setPreviousPosition(value);
        return this;
    }

    public Field createPreviousPosition() {
        Field value = new Field();
        withPreviousPosition(value);
        return value;
    }

    @Override
    public boolean hitMove() {
        return this.previousPosition == null ? false : true;
    }

    /**
     * Compares two Abalone move objects by counting the tokens and opponents tokens.
     *
     * @param Move is an object of the Abalone Move class.
     * @return Returns 1, if move1 is less than move2. -1, if move1 is greater than move2. 0, if they are equal.
     */
    @Override
    public int compareTo(Move move) {

        // this moves number of opponents tokens greater than?
        if(this.getOpponentTokens().size() > move.getOpponentTokens().size()) {
            return -1;
        }

        // this moves number of opponents tokens less than?
        if(this.getOpponentTokens().size() < move.getOpponentTokens().size()) {
            return 1;
        }

        // if they have the same size compare own tokens.
        if(this.getOpponentTokens().size() == move.getOpponentTokens().size()) {

            if(this.getTokens().size() > move.getTokens().size()) {
                return -1;
            }
            if(this.getTokens().size() < move.getTokens().size()) {
                return 1;
            }
            return 0;
        }
        return 0;
    }

    /**
     * This methods compares two abalone moves by iterating through all tokens and opponents token and comparing the
     * fields coordinates of these tokens.
     *
     * @param move is an object of the abalone Move class.
     * @return true, if both moves are equal. False, if they are not.
     */
    @Override
    public boolean equals(Move move) {

        // Get the number of tokens of both moves.
        int thisTokenSize = this.getTokens().size();
        int moveTokenSize = move.getTokens().size();
        int tokenCount = 0;

        // Are the number of tokens the same?
        if(thisTokenSize == moveTokenSize) {
            // Compare the field of all tokens to each other
            for(int i = 0; i < thisTokenSize; i++) {
                for(int j = 0; j < moveTokenSize; j++) {
                    if(this.getTokens().get(i).getField().getX() == move.getTokens().get(j).getField().getX()
                            && this.getTokens().get(i).getField().getY() == move.getTokens().get(j).getField().getY()) {
                        // Count the tokens, which fields are equal.
                        tokenCount++;
                    }
                }
            }
        } else {
            return false;
        }

        // Do the same with the opponents tokens.
        int thisOpponentsTokenSize = this.getOpponentTokens().size();
        int moveOpponentsTokenSize = move.getOpponentTokens().size();
        int opponentTokenCount = 0;

        if(thisOpponentsTokenSize == moveOpponentsTokenSize) {
            for(int i = 0; i < thisOpponentsTokenSize; i++) {
                for(int j = 0; j < moveOpponentsTokenSize; j++) {
                    if(this.getOpponentTokens().get(i).getField().getX() == move.getOpponentTokens().get(j).getField()
                            .getX()
                            && this.getOpponentTokens().get(i).getField().getY() == move.getOpponentTokens().get(j)
                                    .getField().getY()) {
                        // Count the tokens, which fields are equal.
                        opponentTokenCount++;
                    }
                }
            }

        } else {
            return false;
        }

        // Return true if all fields of the tokens are equal.
        if(tokenCount == thisTokenSize && opponentTokenCount == thisOpponentsTokenSize) {
            return true;
        }

        // Return false, if they are not equal.
        return false;
    }
}
