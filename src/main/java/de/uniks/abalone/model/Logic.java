/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;

import de.uniks.abalone.tools.Tools;
import de.uniks.ai.LogicInterface;
import de.uniks.networkparser.interfaces.SendableEntity;

/**
 *
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/abalone/modelgenerator/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/abalone/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href='../../../../../../../src/test/java/de/uniks/modelgenerator/AbaloneModel.java'>AbaloneModel.java</a>
 */
public class Logic implements SendableEntity, LogicInterface<Board, Move, Player> {
    // ==========================================================================
    protected PropertyChangeSupport listeners = null;

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if(this.listeners != null) {
            this.listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    @Override
    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners = new PropertyChangeSupport(this);
        }
        this.listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if(this.listeners == null) {
            this.listeners.removePropertyChangeListener(listener);
        }
        this.listeners.removePropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(this.listeners != null) {
            this.listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    // ==========================================================================
    public void removeYou() {
        setGame(null);
        setCurrentMove(null);
        firePropertyChange("REMOVE_YOU", this, null);
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Logic ----------------------------------- Game
     *              logic                   game
     * </pre>
     */
    public static final String PROPERTY_GAME = "game";

    private Game game = null;

    public Game getGame() {
        return this.game;
    }

    public boolean setGame(Game value) {
        boolean changed = false;
        if(this.game != value) {
            Game oldValue = this.game;
            if(this.game != null) {
                this.game = null;
                oldValue.setLogic(null);
            }
            this.game = value;
            if(value != null) {
                value.withLogic(this);
            }
            firePropertyChange(PROPERTY_GAME, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Logic withGame(Game value) {
        setGame(value);
        return this;
    }

    public Game createGame() {
        Game value = new Game();
        withGame(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Logic ----------------------------------- Move
     *              logic                   currentMove
     * </pre>
     */
    public static final String PROPERTY_CURRENTMOVE = "currentMove";

    private Move currentMove = null;

    public Move getCurrentMove() {
        return this.currentMove;
    }

    public boolean setCurrentMove(Move value) {
        boolean changed = false;
        if(this.currentMove != value) {
            Move oldValue = this.currentMove;
            this.currentMove = value;
            firePropertyChange(PROPERTY_CURRENTMOVE, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Logic withCurrentMove(Move value) {
        setCurrentMove(value);
        return this;
    }

    public Move createCurrentMove() {
        Move value = new Move();
        withCurrentMove(value);
        return value;
    }

    // These value indicates if the current move could be combined with a direction to execute it
    private boolean validMove = false;

    private void setMoveValid() {
        this.validMove = true;
    }

    private void setMoveInvalid() {
        this.validMove = false;
    }

    // These two methods are for the JUnit tests only!
    public boolean isValdidMove() {
        return this.validMove;
    }

    public void resetValidMove() {
        setMoveInvalid();
    }

    // These values indicate the alignment of selected tokens for the move validation.
    private static final int LEFT_RIGHT = 1;            // -

    private static final int DOWNLEFT_TOPRIGHT = 0;     // /

    private static final int DOWNRIGHT_TOPLEFT = 2;     // \

    private int alignment = -1;

    // The 'public' marker is for the JUnit tests only!
    public void setAlignment(int n) {
        this.alignment = n;
    }

    public int getAlignment() {
        return this.alignment;
    }

    /**
     * When a player selects tokens to create a move, this function checks, if he is allowed to include this particular
     * field to the move (e.g. Is it not empty? Does it contain one of his own token?). It also checks, if the field
     * fits to eventually previous selected fields (e.g. Are they connected? Are they aligned?).
     * <p>
     * If the field passes all tests, it will be selected and its token will be added to the move.
     *
     * @param field The chosen field the player wants to use for his next move.
     */
    public void passField(Field field) {
        // Initialize the move for the very first time only
        if(getCurrentMove() == null) {
            createCurrentMove();
        }
        // ===== The next two conditions have to apply to every field
        // Empty field selected?
        if(field.getToken() == null) {
            return;
        }
        // Field with an enemy's token selected?
        if(!field.getToken().getPlayer().equals(getGame().getCurrentPlayer())) {
            return;
        }
        // ===== Case 1: The player selected a new field =====
        if(!field.isSelected()) {
            // Different validations depending on the number of previous selected fields
            switch(getCurrentMove().getTokens().size()) {
                case 0:
                    // A single token is always a valid move
                    getCurrentMove().withTokens(field.getToken());
                    field.setSelected(true);
                    setMoveValid();
                    break;
                case 1:
                    // Two tokens must be aligned on one of the three possible axes and have a distance of 2 at most
                    Field firstField = getCurrentMove().getTokens().first().getField();
                    int deltaX = firstField.getX() - field.getX();
                    int deltaY = firstField.getY() - field.getY();
                    if(Math.abs(deltaX) > 2 || Math.abs(deltaY) > 2) {
                        return;
                    }
                    // There must be an own token between them
                    if(Math.abs(deltaY) == 2) {
                        Field target = this.game.getFields().filterX((firstField.getX() + field.getX()) / 2)
                                .filterY((firstField.getY() + field.getY()) / 2).first();
                        if(target.getToken() == null
                                || !target.getToken().getPlayer().equals(field.getToken().getPlayer())) {
                            return;
                        }
                    }
                    if(deltaX == 0 && deltaY != 0) {
                        setAlignment(DOWNRIGHT_TOPLEFT);
                        getCurrentMove().withTokens(field.getToken());
                        field.setSelected(true);
                        if(Math.abs(deltaY) == 1) {
                            setMoveValid();
                        } else {
                            setMoveInvalid();
                        }
                    } else if(deltaY == 0 && deltaX != 0) {
                        setAlignment(LEFT_RIGHT);
                        getCurrentMove().withTokens(field.getToken());
                        field.setSelected(true);
                        if(Math.abs(deltaX) == 1) {
                            setMoveValid();
                        } else {
                            setMoveInvalid();
                        }
                    } else if(deltaX + deltaY == 0) {
                        setAlignment(DOWNLEFT_TOPRIGHT);
                        getCurrentMove().withTokens(field.getToken());
                        field.setSelected(true);
                        if(Math.abs(deltaX) == 1) {
                            setMoveValid();
                        } else {
                            setMoveInvalid();
                        }
                    }
                    break;
                case 2:
                    // The third token can only lie between the other ones or extend the line in the determined
                    // direction
                    Field field1 = getCurrentMove().getTokens().get(0).getField();
                    Field field2 = getCurrentMove().getTokens().get(1).getField();
                    deltaX = field1.getX() - field2.getX();
                    deltaY = field1.getY() - field2.getY();
                    if(Math.abs(deltaX) == 2 || Math.abs(deltaY) == 2) {
                        Field expected = getGame().getFields().filterX((field1.getX() + field2.getX()) / 2)
                                .filterY((field1.getY() + field2.getY()) / 2).first();
                        if(field.equals(expected)) {
                            getCurrentMove().withTokens(field.getToken());
                            field.setSelected(true);
                            setMoveValid();
                        }
                    } else {
                        switch(getAlignment()) {
                            case LEFT_RIGHT:
                                if((field1.getLeft() != null && field1.getLeft().equals(field))
                                        || (field1.getRight() != null && field1.getRight().equals(field))
                                        || (field2.getLeft() != null && field2.getLeft().equals(field))
                                        || (field2.getRight() != null && field2.getRight().equals(field))) {
                                    getCurrentMove().withTokens(field.getToken());
                                    field.setSelected(true);
                                    setMoveValid();
                                }
                                break;
                            case DOWNLEFT_TOPRIGHT:
                                if((field1.getDownLeft() != null && field1.getDownLeft().equals(field))
                                        || (field1.getTopRight() != null && field1.getTopRight().equals(field))
                                        || (field2.getDownLeft() != null && field2.getDownLeft().equals(field))
                                        || (field2.getTopRight() != null && field2.getTopRight().equals(field))) {
                                    getCurrentMove().withTokens(field.getToken());
                                    field.setSelected(true);
                                    setMoveValid();
                                }
                                break;
                            case DOWNRIGHT_TOPLEFT:
                                if((field1.getTopLeft() != null && field1.getTopLeft().equals(field))
                                        || (field1.getDownRight() != null && field1.getDownRight().equals(field))
                                        || (field2.getTopLeft() != null && field2.getTopLeft().equals(field))
                                        || (field2.getDownRight() != null && field2.getDownRight().equals(field))) {
                                    getCurrentMove().withTokens(field.getToken());
                                    field.setSelected(true);
                                    setMoveValid();
                                }
                                break;
                        }
                    }
                    break;
            }
        } else {
            // ===== Case 2: The player wanted to deselect a field =====
            switch(getCurrentMove().getTokens().size()) {
                // Different validations depending on the number of previous selected fields
                case 1:
                    getCurrentMove().withoutTokens(field.getToken());
                    field.setSelected(false);
                    setMoveInvalid();
                    break;
                case 2:
                    getCurrentMove().withoutTokens(field.getToken());
                    field.setSelected(false);
                    setAlignment(-1);
                    setMoveValid();
                    break;
                case 3:
                    getCurrentMove().withoutTokens(field.getToken());
                    field.setSelected(false);
                    Field field1 = getCurrentMove().getTokens().get(0).getField();
                    Field field2 = getCurrentMove().getTokens().get(1).getField();
                    int deltaX = Math.abs(field1.getX() - field2.getX());
                    int deltaY = Math.abs(field1.getY() - field2.getY());
                    if(deltaX == 1 || deltaY == 1) {
                        setMoveValid();
                    } else {
                        setMoveInvalid();
                    }
                    break;
            }
        }
    }

    /**
     * After selecting his fields, the player passes a direction in which he wants to move the tokens. This function
     * checks, if an execution in this direction is possible. In case enemy's tokens will be moved, they also will be
     * added to the move.
     * <p>
     * If the selected fields and the direction build a valid move, it will be executed by the logic and the logic will
     * be reset for the next move.
     *
     * @param direction The chosen direction in which the player wants to execute his move.
     */
    public void passDirection(int direction) {
        // No invalid move possible
        if(!isValdidMove()) {
            return;
        }
        // When a player wants to move a single token or move all token parallel to their alignment, the regarding
        // target fields just have to exist and be empty.
        if(getCurrentMove().getTokens().size() == 1
                || !(getAlignment() == direction || (getAlignment() + 3) == direction)) {
            for(Field field: getCurrentMove().getTokens().getField()) {
                if(field.getNeighbor(direction) == null || field.getNeighbor(direction).getToken() != null) {
                    return;
                }
            }
        } else {
            // In this case the player wants to move 2 or 3 tokens along their alignment.
            Field first = getCurrentMove().getTokens().getField().filter(field -> {
                Field neighbor = field.getNeighbor(direction);
                return neighbor == null || !getCurrentMove().getTokens().getField().contains(neighbor);
            }).first();
            // The player wants to push his own token over the edge
            if(first.getNeighbor(direction) == null) {
                return;
            }
            // The player wants to push another token
            if(first.getNeighbor(direction).getToken() != null) {
                // An own token is not allowed
                if(first.getNeighbor(direction).getToken().getPlayer().equals(first.getToken().getPlayer())) {
                    return;
                } else {
                    // Let 'n' be the number of own tokens the player wants to move, then we must check at most the next
                    // 'n' fields.
                    int enemyToken = 0;
                    boolean validMove = false;
                    Field temp = first;
                    for(int i = 0; i < getCurrentMove().getTokens().size(); i++) {
                        temp = temp.getNeighbor(direction);
                        // Here we have reached the edge of the board or an empty field
                        if(temp == null || temp.getToken() == null) {
                            validMove = true;
                            break;
                        } else if(temp.getToken().getPlayer().equals(first.getToken().getPlayer())) {
                            // Only enemy tokens can be pushed -> invalid move
                            return;
                        } else {
                            enemyToken++;
                        }
                    }
                    // If 'validMove' is still false, there are as many tokens behind the player's tokens, as the player
                    // wants to move, independent from the token's owner. Therefore the move is invalid.
                    if(!validMove) {
                        return;
                    }
                    // Finally add all enemy tokens to the move
                    temp = first;
                    for(int i = 0; i < enemyToken; i++) {
                        temp = temp.getNeighbor(direction);
                        getCurrentMove().withOpponentTokens(temp.getToken());
                    }
                }
            }
        }
        // By reaching this point, everything is all right and the move can be executed.
        getCurrentMove().withDirection(direction);
        executeMove(getCurrentMove());
        // Clean up for the next move.
        setAlignment(-1);
        setMoveInvalid();
        setCurrentMove(null);
        getGame().getFields().forEach(field -> field.setSelected(false));
        getGame().withCurrentPlayer(getGame().getCurrentPlayer().getNext());
    }

    // If activated, the logger records every move
    private BufferedWriter logger;

    public void setLogger(BufferedWriter logger) {
        this.logger = logger;
    }

    public BufferedWriter getLogger() {
        return this.logger;
    }

    private String buildTimestamp() {
        DecimalFormat df = new DecimalFormat("00");
        Calendar time = Calendar.getInstance();
        StringBuilder result = new StringBuilder();
        result.append("[");
        result.append(df.format(time.get(Calendar.HOUR)));
        result.append(":");
        result.append(df.format(time.get(Calendar.MINUTE)));
        result.append(":");
        result.append(df.format(time.get(Calendar.SECOND)));
        result.append("] ");
        return result.toString();
    }

    /**
     * The AI needs to rate a certain board situation to be able to compute 'the best' move. For our rating we consider
     * the difference between the number of tokens and the distance of all tokens to the center.
     *
     * @param board The current board situation
     * @param player The current player
     */
    @Override
    public int evaluate(Board board, Player player) {
        // First step: Check if a player has won the game.
        int score = 0;
        int coefficient = 100_000;
        Player winner = checkWinner(board);
        if(winner != null) {
            score += -10_000_000;
        }
        // Second step: Difference between number of token on the board
        // range: -500_000 <= i <= 500_000
        score += coefficient * differenceInNumberOfTokens(board, player);
        // Third step: Distance from Center
        score += distanceFromCenter(board, player);

        return score;
    }

    private int differenceInNumberOfTokens(Board board, Player player) {
        int numberOfEnenmyPlayerTokens = 0;
        int numberOfFriendlyPlayerTokens = 0;
        for(Token token: player.getTokens()) {
            if(token.getField() != null) {
                numberOfFriendlyPlayerTokens++;
            }
        }
        for(Token token: player.getNext().getTokens()) {
            if(token.getField() != null) {
                numberOfEnenmyPlayerTokens++;
            }
        }
        return numberOfEnenmyPlayerTokens - numberOfFriendlyPlayerTokens;
    }

    public int distanceFromCenter(Board board, Player player) {
        int x = 0;
        int y = 0;
        double tokenCounter = 0;
        double distances = 0;
        for(Token token: player.getTokens()) {
            if(token.getField() != null) {
                x = token.getField().getX();
                y = token.getField().getY();
                distances += Tools.DISTANCE_WEIGHTS[y][x];
                tokenCounter++;
            }
        }

        // Range: 1 - 5
        double avgDistanceOurPlayer = distances / tokenCounter;
        tokenCounter = 0;
        distances = 0;
        for(Token token: player.getNext().getTokens()) {
            if(token.getField() != null) {
                x = token.getField().getX();
                y = token.getField().getY();
                distances += Tools.DISTANCE_WEIGHTS[y][x];
                tokenCounter++;
            }
        }
        // Range: 1 - 5
        double avgDistanceOpponentPlayer = distances / tokenCounter;
        // Range: 4 to -4
        double distanceDifference = avgDistanceOpponentPlayer - avgDistanceOurPlayer;
        // Range: 48_000 to -48_000
        return (int) (12_000 * distanceDifference);
    }

    /**
     * Returns the winner for a given board.
     *
     * @param board
     * @return winner or null if there is no winner
     */
    @Override
    public Player checkWinner(Board board) {
        int counter = 0;
        for(Player player: board.getGame().getPlayers()) {
            for(Token token: player.getTokens()) {
                if(token.getField() == null) {
                    counter++;
                }
            }
            if(counter >= 6) {
                return player.getNext();
            }
            counter = 0;
        }
        return null;
    }

    /**
     * Undo a move in simulation mode without triggering the listener Field.token and Token.field.
     *
     * @param move
     */
    @Override
    public void undoMoveSimulation(Move move) {
        undoMove(move, true);
    }

    /**
     * Undo a move.
     *
     * @param move
     */
    public void undoMove(Move move) {
        undoMove(move, false);
        if(this.logger != null) {
            try {
                StringBuilder entry = new StringBuilder();
                entry.append(buildTimestamp());
                entry.append("UNDO: ");
                entry.append(move);
                entry.append(System.lineSeparator());
                this.logger.write(entry.toString());
                this.logger.flush();
            } catch(IOException e) {
                e.printStackTrace();
            }
        }
        this.game.withoutHistory(move);
    }

    private void undoMove(Move move, boolean simulation) {
        // manipulate move direction
        move.setDirection((move.getDirection() + 3) % 6);
        executeMove(move, simulation);
        // restore the move direction
        move.setDirection((move.getDirection() + 3) % 6);
    }

    /**
     * Executes a move and adds it to the game history. Does no validation at all.
     *
     * @param move
     */
    public void executeMove(Move move) {
        // Remove all 'lastMoved' flags from enemy tokens
        getGame().getCurrentPlayer().getNext().getTokens().getField().withLastMoved(false);
        // Execute current move
        executeMove(move, false);
        // Mark all previously moved token as 'last moved'
        move.getTokens().getField().withLastMoved(true);
        getGame().withHistory(move);
        // Create log entry if desired
        if(this.logger != null) {
            try {
                StringBuilder entry = new StringBuilder();
                entry.append(buildTimestamp());
                entry.append(this.game.getCurrentPlayer().getName() + ": ");
                entry.append(move);
                entry.append(System.lineSeparator());
                this.logger.write(entry.toString());
                this.logger.flush();
            } catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Executes a move in simulation mode without triggering the listener Field.token and Token.field. Does no
     * validation at all.
     *
     * @param move
     */
    @Override
    public void executeMoveSimulation(Move move) {
        executeMove(move, true);
    }

    private void executeMove(Move move, boolean simulation) {
        int modifyScore = 0;
        ArrayList<Field> newTokenFields = new ArrayList<>();
        ArrayList<Field> newOpponentTokenFields = new ArrayList<>();
        // determine new token positions
        for(Token token: move.getTokens()) {
            newTokenFields.add(token.getField().getNeighbor(move.getDirection()));
        }
        // determine new opponentToken positions
        for(Token token: move.getOpponentTokens()) {
            if(token.getField() != null) {
                Field newPosition = token.getField().getNeighbor(move.getDirection());
                // token is kicked of the board
                if(newPosition == null) {
                    move.setPreviousPosition(token.getField());
                }
                newOpponentTokenFields.add(newPosition);
            }
            // case undo, token has no field
            else {
                if(!simulation) {
                    modifyScore = -1;
                }
                newOpponentTokenFields.add(move.getPreviousPosition());
            }
        }
        // set tokens on new Position
        for(int i = 0; i < move.getTokens().size(); i++) {
            Token token = move.getTokens().get(i);
            Field newPosition = newTokenFields.get(i);
            token.setField(newPosition, simulation);
        }
        // set opponentTokens on new Position
        for(int i = 0; i < move.getOpponentTokens().size(); i++) {
            Token token = move.getOpponentTokens().get(i);
            Field newPosition = newOpponentTokenFields.get(i);
            token.setField(newPosition, simulation);
            // token is kicked of the board and its no simulation -> update score
            if(newPosition == null && !simulation) {
                modifyScore = 1;
            }
        }
        // modifyScore
        if(modifyScore != 0) {
            Player player = move.getTokens().get(0).getPlayer();
            player.setScores(player.getScores() + modifyScore);
            Player winner = checkWinner(getGame().getBoard());
            if(winner != null) {
                try {
                    if(this.logger != null) {
                        StringBuilder entry = new StringBuilder();
                        entry.append(buildTimestamp());
                        entry.append(winner.getName() + " won the game.");
                        this.logger.write(entry.toString());
                        this.logger.flush();
                    }
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
            getGame().setWinner(winner);
        }
    }

    /**
     * Creates a list of all possible moves which could be played by a certain player for a certain board situation.
     *
     * @param player The player
     * @param board The board situation
     */
    @Override
    public ArrayList<Move> getAllMoves(Player player, Board board) {
        ArrayList<Move> allMoves = new ArrayList<>();
        // for each token..
        for(Token token: player.getTokens()) {
            // .. thats on the board
            if(token.getField() == null) {
                continue;
            }
            // build moves with this single token
            allMoves.addAll(getMovesForGivenTokens(0, player, token));
            // now collect neighbors for combo moves
            // right
            Field tmpField = token.getField().getRight();
            // if there is a field to the right with one of our tokens
            if(tmpField != null && tmpField.getToken() != null && tmpField.getToken().getPlayer() == player) {
                // 2er combo
                allMoves.addAll(getMovesForGivenTokens(1, player, token, tmpField.getToken()));
                if(tmpField.getRight() != null && tmpField.getRight().getToken() != null
                        && tmpField.getRight().getToken().getPlayer() == player) {
                    // 3er combo
                    allMoves.addAll(getMovesForGivenTokens(1, player, token, tmpField.getToken(),
                            tmpField.getRight().getToken()));
                }
            }
            // down right
            tmpField = token.getField().getDownRight();
            // if there is a field down right with one of our tokens
            if(tmpField != null && tmpField.getToken() != null && tmpField.getToken().getPlayer() == player) {
                // 2er combo
                allMoves.addAll(getMovesForGivenTokens(2, player, token, tmpField.getToken()));
                if(tmpField.getDownRight() != null && tmpField.getDownRight().getToken() != null
                        && tmpField.getDownRight().getToken().getPlayer() == player) {
                    // 3er combo
                    allMoves.addAll(getMovesForGivenTokens(2, player, token, tmpField.getToken(),
                            tmpField.getDownRight().getToken()));
                }
            }
            // down left
            tmpField = token.getField().getDownLeft();
            // if there is a field down left with one of our tokens
            if(tmpField != null && tmpField.getToken() != null && tmpField.getToken().getPlayer() == player) {
                // 2er combo
                allMoves.addAll(getMovesForGivenTokens(3, player, token, tmpField.getToken()));
                if(tmpField.getDownLeft() != null && tmpField.getDownLeft().getToken() != null
                        && tmpField.getDownLeft().getToken().getPlayer() == player) {
                    // 3er combo
                    allMoves.addAll(getMovesForGivenTokens(3, player, token, tmpField.getToken(),
                            tmpField.getDownLeft().getToken()));
                }
            }
        }
        return allMoves;
    }

    /**
     * Creates a list of possible moves for a certain player and a specific set of tokens.
     *
     * @param orientation The orientation of the tokens on the board
     * @param player The player
     * @param tokens The combination of tokens
     * @return
     */
    public ArrayList<Move> getMovesForGivenTokens(int orientation, Player player, Token... tokens) {
        ArrayList<Move> allMoves = new ArrayList<>();
        // single Token
        if(tokens.length == 1) {
            // for every direction
            for(int direction = 0; direction < 6; direction++) {
                Field targetField = tokens[0].getField().getNeighbor(direction);
                // if the target Field exists and is free..
                if(targetField != null && targetField.getToken() == null) {
                    // ..build the move
                    allMoves.add(new Move().withTokens(tokens[0]).withDirection(direction));
                }
            }
        }
        // combo moves
        if(tokens.length > 1) {
            // for every direction
            for(int direction = 0; direction < 6; direction++) {
                if(direction == (orientation + 3) % 6) {
                    // if there is a field
                    Field targetField = tokens[0].getField().getNeighbor(direction);
                    if(targetField != null) {
                        // its free -> build the move
                        if(targetField.getToken() == null) {
                            allMoves.add(new Move().withTokens(tokens).withDirection(direction));
                        }
                        // there is a opponent token
                        else if(targetField.getToken().getPlayer() != player) {
                            if(targetField.getNeighbor(direction) != null) {
                                // the next field is free -> build the move
                                if(targetField.getNeighbor(direction).getToken() == null) {
                                    allMoves.add(new Move().withTokens(tokens)
                                            .withOpponentTokens(targetField.getToken()).withDirection(direction));
                                }
                                // there is a opponent token
                                else if(targetField.getNeighbor(direction).getToken().getPlayer() != player) {
                                    // if we have enough tokens to push two enemy tokens..
                                    if(tokens.length == 3) {
                                        // and the field behind the enemies doesnt exist
                                        if(targetField.getNeighbor(direction).getNeighbor(direction) == null) {
                                            allMoves.add(new Move().withTokens(tokens)
                                                    .withOpponentTokens(targetField.getToken(),
                                                            targetField.getNeighbor(direction).getToken())
                                                    .withDirection(direction));
                                        }
                                        // or is free
                                        else if(targetField.getNeighbor(direction).getNeighbor(direction)
                                                .getToken() == null) {
                                            allMoves.add(new Move().withTokens(tokens)
                                                    .withOpponentTokens(targetField.getToken(),
                                                            targetField.getNeighbor(direction).getToken())
                                                    .withDirection(direction));
                                        }
                                    }
                                }
                                // there is no field -> build the move
                            } else {
                                allMoves.add(new Move().withTokens(tokens).withOpponentTokens(targetField.getToken())
                                        .withDirection(direction));
                            }
                        }
                    }
                } else if(direction == orientation) {
                    // if there is a field
                    Field targetField = tokens[tokens.length - 1].getField().getNeighbor(direction);
                    if(targetField != null) {
                        // its free -> build the move
                        if(targetField.getToken() == null) {
                            allMoves.add(new Move().withTokens(tokens).withDirection(direction));
                        }
                        // there is a opponent token
                        else if(targetField.getToken().getPlayer() != player) {
                            if(targetField.getNeighbor(direction) != null) {
                                // the next field is free -> build the move
                                if(targetField.getNeighbor(direction).getToken() == null) {
                                    allMoves.add(new Move().withTokens(tokens)
                                            .withOpponentTokens(targetField.getToken()).withDirection(direction));
                                }
                                // there is a opponent token
                                else if(targetField.getNeighbor(direction).getToken().getPlayer() != player) {
                                    // if we have enough tokens to push two enemy tokens..
                                    if(tokens.length == 3) {
                                        // and the field behind the enemies doesnt exist
                                        if(targetField.getNeighbor(direction).getNeighbor(direction) == null) {
                                            allMoves.add(new Move().withTokens(tokens)
                                                    .withOpponentTokens(targetField.getToken(),
                                                            targetField.getNeighbor(direction).getToken())
                                                    .withDirection(direction));
                                        }
                                        // or is free
                                        else if(targetField.getNeighbor(direction).getNeighbor(direction)
                                                .getToken() == null) {
                                            allMoves.add(new Move().withTokens(tokens)
                                                    .withOpponentTokens(targetField.getToken(),
                                                            targetField.getNeighbor(direction).getToken())
                                                    .withDirection(direction));
                                        }
                                    }
                                }
                                // there is no field -> build the move
                            } else {
                                allMoves.add(new Move().withTokens(tokens).withOpponentTokens(targetField.getToken())
                                        .withDirection(direction));
                            }
                        }
                    }
                }
                // its one of the 4 parallel moves
                else {
                    // check that every target field is free
                    boolean allFieldsAreFree = true;
                    for(Token token: tokens) {
                        if(token.getField().getNeighbor(direction) != null
                                && token.getField().getNeighbor(direction).getToken() == null) {
                            allFieldsAreFree = allFieldsAreFree && true;
                        } else {
                            allFieldsAreFree = false;
                        }
                    }
                    if(allFieldsAreFree) {
                        // build the move
                        allMoves.add(new Move().withTokens(tokens).withDirection(direction));
                    }
                }
            }
        }
        return allMoves;
    }
}
