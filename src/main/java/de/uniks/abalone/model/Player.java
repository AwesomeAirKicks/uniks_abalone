/*
 * Copyright (c) 2017 Olaf Versteeg Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package de.uniks.abalone.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import de.uniks.abalone.model.util.PlayerSet;
import de.uniks.abalone.model.util.TokenSet;
import de.uniks.ai.PlayerInterface;
import de.uniks.networkparser.EntityUtil;
import de.uniks.networkparser.interfaces.SendableEntity;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Token;

/**
 *
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/abalone/modelgenerator/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/abalone/ModelGenerator.java'>ModelGenerator.java</a>
 * @see <a href='../../../../../../../src/test/java/de/uniks/modelgenerator/AbaloneModel.java'>AbaloneModel.java</a>
 */
public class Player implements SendableEntity, PlayerInterface {
    // ==========================================================================
    protected PropertyChangeSupport listeners = null;

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if(listeners != null) {
            listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    @Override
    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if(listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if(listeners == null) {
            listeners.removePropertyChangeListener(listener);
        }
        listeners.removePropertyChangeListener(listener);
        return true;
    }

    @Override
    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(listeners != null) {
            listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    // ==========================================================================
    public void removeYou() {
        setGame(null);
        setCurrentGame(null);
        setWonGame(null);
        withoutTokens(getTokens().toArray(new Token[getTokens().size()]));
        setPrevious(null);
        setNext(null);
        firePropertyChange("REMOVE_YOU", this, null);
    }

    // ==========================================================================
    public static final String PROPERTY_NAME = "name";

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String value) {
        if(!EntityUtil.stringEquals(name, value)) {
            String oldValue = name;
            name = value;
            firePropertyChange(PROPERTY_NAME, oldValue, value);
        }
    }

    public Player withName(String value) {
        setName(value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(" ").append(getName());
        result.append(" ").append(getColor());
        result.append(" ").append(getScores());
        return result.substring(1);
    }

    // ==========================================================================
    public static final String PROPERTY_COLOR = "color";

    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String value) {
        if(!EntityUtil.stringEquals(color, value)) {
            String oldValue = color;
            color = value;
            firePropertyChange(PROPERTY_COLOR, oldValue, value);
        }
    }

    public Player withColor(String value) {
        setColor(value);
        return this;
    }

    // ==========================================================================
    public static final String PROPERTY_SCORES = "scores";

    private int scores;

    public int getScores() {
        return scores;
    }

    public void setScores(int value) {
        if(scores != value) {
            int oldValue = scores;
            scores = value;
            firePropertyChange(PROPERTY_SCORES, oldValue, value);
        }
    }

    public Player withScores(int value) {
        setScores(value);
        return this;
    }

    /********************************************************************
     * <pre>
     *              many                       one
     * Player ----------------------------------- Game
     *              players                   game
     * </pre>
     */
    public static final String PROPERTY_GAME = "game";

    private Game game = null;

    public Game getGame() {
        return game;
    }

    public boolean setGame(Game value) {
        boolean changed = false;
        if(game != value) {
            Game oldValue = game;
            if(game != null) {
                game = null;
                oldValue.withoutPlayers(this);
            }
            game = value;
            if(value != null) {
                value.withPlayers(this);
            }
            firePropertyChange(PROPERTY_GAME, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Player withGame(Game value) {
        setGame(value);
        return this;
    }

    public Game createGame() {
        Game value = new Game();
        withGame(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Player ----------------------------------- Game
     *              currentPlayer                   currentGame
     * </pre>
     */
    public static final String PROPERTY_CURRENTGAME = "currentGame";

    private Game currentGame = null;

    public Game getCurrentGame() {
        return currentGame;
    }

    public boolean setCurrentGame(Game value) {
        boolean changed = false;
        if(currentGame != value) {
            Game oldValue = currentGame;
            if(currentGame != null) {
                currentGame = null;
                oldValue.setCurrentPlayer(null);
            }
            currentGame = value;
            if(value != null) {
                value.withCurrentPlayer(this);
            }
            firePropertyChange(PROPERTY_CURRENTGAME, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Player withCurrentGame(Game value) {
        setCurrentGame(value);
        return this;
    }

    public Game createCurrentGame() {
        Game value = new Game();
        withCurrentGame(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Player ----------------------------------- Game
     *              winner                   wonGame
     * </pre>
     */
    public static final String PROPERTY_WONGAME = "wonGame";

    private Game wonGame = null;

    public Game getWonGame() {
        return wonGame;
    }

    public boolean setWonGame(Game value) {
        boolean changed = false;
        if(wonGame != value) {
            Game oldValue = wonGame;
            if(wonGame != null) {
                wonGame = null;
                oldValue.setWinner(null);
            }
            wonGame = value;
            if(value != null) {
                value.withWinner(this);
            }
            firePropertyChange(PROPERTY_WONGAME, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Player withWonGame(Game value) {
        setWonGame(value);
        return this;
    }

    public Game createWonGame() {
        Game value = new Game();
        withWonGame(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       many
     * Player ----------------------------------- Token
     *              player                   tokens
     * </pre>
     */
    public static final String PROPERTY_TOKENS = "tokens";

    private TokenSet tokens = null;

    public TokenSet getTokens() {
        if(tokens == null) {
            return TokenSet.EMPTY_SET;
        }
        return tokens;
    }

    public Player withTokens(Token... value) {
        if(value == null) {
            return this;
        }
        for(Token item: value) {
            if(item != null) {
                if(tokens == null) {
                    tokens = new TokenSet();
                }
                boolean changed = tokens.add(item);
                if(changed) {
                    item.withPlayer(this);
                    firePropertyChange(PROPERTY_TOKENS, null, item);
                }
            }
        }
        return this;
    }

    public Player withoutTokens(Token... value) {
        for(Token item: value) {
            if((tokens != null) && (item != null)) {
                if(tokens.remove(item)) {
                    item.setPlayer(null);
                    firePropertyChange(PROPERTY_TOKENS, item, null);
                }
            }
        }
        return this;
    }

    public Token createTokens() {
        Token value = new Token();
        withTokens(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Player ----------------------------------- Player
     *              next                   previous
     * </pre>
     */
    public static final String PROPERTY_PREVIOUS = "previous";

    private Player previous = null;

    public Player getPrevious() {
        return previous;
    }

    public PlayerSet getPreviousTransitive() {
        PlayerSet result = new PlayerSet().with(this);
        return result.getPreviousTransitive();
    }

    public boolean setPrevious(Player value) {
        boolean changed = false;
        if(previous != value) {
            Player oldValue = previous;
            if(previous != null) {
                previous = null;
                oldValue.setNext(null);
            }
            previous = value;
            if(value != null) {
                value.withNext(this);
            }
            firePropertyChange(PROPERTY_PREVIOUS, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Player withPrevious(Player value) {
        setPrevious(value);
        return this;
    }

    public Player createPrevious() {
        Player value = new Player();
        withPrevious(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Player ----------------------------------- Player
     *              previous                   next
     * </pre>
     */
    public static final String PROPERTY_NEXT = "next";

    private Player next = null;

    public Player getNext() {
        return next;
    }

    public PlayerSet getNextTransitive() {
        PlayerSet result = new PlayerSet().with(this);
        return result.getNextTransitive();
    }

    public boolean setNext(Player value) {
        boolean changed = false;
        if(next != value) {
            Player oldValue = next;
            if(next != null) {
                next = null;
                oldValue.setPrevious(null);
            }
            next = value;
            if(value != null) {
                value.withPrevious(this);
            }
            firePropertyChange(PROPERTY_NEXT, oldValue, value);
            changed = true;
        }
        return changed;
    }

    public Player withNext(Player value) {
        setNext(value);
        return this;
    }

    public Player createNext() {
        Player value = new Player();
        withNext(value);
        return value;
    }
}
