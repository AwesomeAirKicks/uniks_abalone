package de.uniks.abalone.tools;

/**
 * This class provides different initial situations for Abalone. The default layout is the classic game mode. In the
 * Belgian Daisy layout every player starts with half of his token on is own side and the other ones in the opponent's
 * half.
 */
public class Layouts {
    public static final int[][] DEFAULT_TOKEN_LAYOUT = {
            {0, 0, 0, 0, 1, 1, 1, 1, 1},
            {0, 0, 0, 1, 1, 1, 1, 1, 1},
            {0, 0, 0, 0, 1, 1, 1, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 2, 2, 2, 0, 0, 0, 0},
            {2, 2, 2, 2, 2, 2, 0, 0, 0},
            {2, 2, 2, 2, 2, 0, 0, 0, 0}};

    public static final int[][] BELGIAN_DAISY_TOKEN_LAYOUT = {
            {0, 0, 0, 0, 2, 2, 0, 1, 1},
            {0, 0, 0, 2, 2, 2, 1, 1, 1},
            {0, 0, 0, 2, 2, 0, 1, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 1, 0, 2, 2, 0, 0, 0},
            {1, 1, 1, 2, 2, 2, 0, 0, 0},
            {1, 1, 0, 2, 2, 0, 0, 0, 0}};
}