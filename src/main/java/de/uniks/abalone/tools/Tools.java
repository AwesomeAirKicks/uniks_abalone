package de.uniks.abalone.tools;

/**
 * This class provides different tools for the game logic.
 */
public class Tools {
    /**
     * For the evaluation each field has a certain weight. The closer a field to the center, the higher is its weight.
     */
    public static final int[][] DISTANCE_WEIGHTS = {
            {0, 0, 0, 0, 1, 1, 1, 1, 1},
            {0, 0, 0, 1, 2, 2, 2, 2, 1},
            {0, 0, 1, 2, 3, 3, 3, 2, 1},
            {0, 1, 2, 3, 4, 4, 3, 2, 1},
            {1, 2, 3, 4, 5, 4, 3, 2, 1},
            {1, 2, 3, 4, 4, 3, 2, 1, 0},
            {1, 2, 3, 3, 3, 2, 1, 0, 0},
            {1, 2, 2, 2, 2, 1, 0, 0, 0},
            {1, 1, 1, 1, 1, 0, 0, 0, 0}};

    /**
     * The board can be rotated in six different positions. Afterwards it may be necessary to mirror the board at the
     * vertical. For each of these operations there is an array with the regarding x-values.
     * <p>
     * The code consists of two digits. The first one represents the sector which will be rotated in the lower center
     * area. The second one determines if the board has to be mirrored.
     *
     * @param code A certain code for each operation.
     * @return The regarding array with the new x-values.
     */
    public static int[][] getXOp(String code) {
        switch(code) {
            case "00":
                return Tools.IDENTITY_X;
            case "01":
                return Tools.SIMPLE_MIRROR_X;

            case "10":
                return Tools.ROTATE_1_LEFT_X;
            case "11":
                return Tools.ROTATE_1_LEFT_MIRROR_X;

            case "20":
                return Tools.ROTATE_2_LEFT_X;
            case "21":
                return Tools.ROTATE_2_LEFT_MIRROR_X;

            case "30":
                return Tools.ROTATE_3_LEFT_X;
            case "31":
                return Tools.ROTATE_3_LEFT_MIRROR_X;

            case "40":
                return Tools.ROTATE_2_RIGHT_X;
            case "41":
                return Tools.ROTATE_2_RIGHT_MIRROR_X;

            case "50":
                return Tools.ROTATE_1_RIGHT_X;
            case "51":
                return Tools.ROTATE_1_RIGHT_MIRROR_X;
        }
        return null;
    }

    /**
     * The board can be rotated in six different positions. Afterwards it may be necessary to mirror the board at the
     * vertical. For each of these operations there is an array with the regarding y-values.
     * <p>
     * The code consists of two digits. The first one represents the sector which will be rotated in the lower center
     * area. The second one determines if the board has to be mirrored.
     *
     * @param code A certain code for each operation.
     * @return The regarding array with the new y-values.
     */
    public static int[][] getYOp(String code) {
        switch(code) {
            case "00":
                return Tools.IDENTITY_Y;
            case "01":
                return Tools.SIMPLE_MIRROR_Y;

            case "10":
                return Tools.ROTATE_1_LEFT_Y;
            case "11":
                return Tools.ROTATE_1_LEFT_MIRROR_Y;

            case "20":
                return Tools.ROTATE_2_LEFT_Y;
            case "21":
                return Tools.ROTATE_2_LEFT_MIRROR_Y;

            case "30":
                return Tools.ROTATE_3_LEFT_Y;
            case "31":
                return Tools.ROTATE_3_LEFT_MIRROR_Y;

            case "40":
                return Tools.ROTATE_2_RIGHT_Y;
            case "41":
                return Tools.ROTATE_2_RIGHT_MIRROR_Y;

            case "50":
                return Tools.ROTATE_1_RIGHT_Y;
            case "51":
                return Tools.ROTATE_1_RIGHT_MIRROR_Y;
        }
        return null;
    }

    // ===== Identity =====
    private static int[][] IDENTITY_X = {
            {0, 0, 0, 0, 4, 5, 6, 7, 8},
            {0, 0, 0, 3, 4, 5, 6, 7, 8},
            {0, 0, 2, 3, 4, 5, 6, 7, 8},
            {0, 1, 2, 3, 4, 5, 6, 7, 8},
            {0, 1, 2, 3, 4, 5, 6, 7, 8},
            {0, 1, 2, 3, 4, 5, 6, 7, 0},
            {0, 1, 2, 3, 4, 5, 6, 0, 0},
            {0, 1, 2, 3, 4, 5, 0, 0, 0},
            {0, 1, 2, 3, 4, 0, 0, 0, 0}};

    private static int[][] IDENTITY_Y = {
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 1, 1, 1, 1, 1},
            {0, 0, 2, 2, 2, 2, 2, 2, 2},
            {0, 3, 3, 3, 3, 3, 3, 3, 3},
            {4, 4, 4, 4, 4, 4, 4, 4, 4},
            {5, 5, 5, 5, 5, 5, 5, 5, 0},
            {6, 6, 6, 6, 6, 6, 6, 0, 0},
            {7, 7, 7, 7, 7, 7, 0, 0, 0},
            {8, 8, 8, 8, 8, 0, 0, 0, 0}};

    // ===== Mirroring only =====
    private static int[][] SIMPLE_MIRROR_X = {
            {0, 0, 0, 0, 8, 7, 6, 5, 4},
            {0, 0, 0, 8, 7, 6, 5, 4, 3},
            {0, 0, 8, 7, 6, 5, 4, 3, 2},
            {0, 8, 7, 6, 5, 4, 3, 2, 1},
            {8, 7, 6, 5, 4, 3, 2, 1, 0},
            {7, 6, 5, 4, 3, 2, 1, 0, 0},
            {6, 5, 4, 3, 2, 1, 0, 0, 0},
            {5, 4, 3, 2, 1, 0, 0, 0, 0},
            {4, 3, 2, 1, 0, 0, 0, 0, 0}};

    private static int[][] SIMPLE_MIRROR_Y = {
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 1, 1, 1, 1, 1},
            {0, 0, 2, 2, 2, 2, 2, 2, 2},
            {0, 3, 3, 3, 3, 3, 3, 3, 3},
            {4, 4, 4, 4, 4, 4, 4, 4, 4},
            {5, 5, 5, 5, 5, 5, 5, 5, 0},
            {6, 6, 6, 6, 6, 6, 6, 0, 0},
            {7, 7, 7, 7, 7, 7, 0, 0, 0},
            {8, 8, 8, 8, 8, 0, 0, 0, 0}};

    // ===== Turn 60� left =====
    private static int[][] ROTATE_1_LEFT_X = {
            {0, 0, 0, 0, 8, 8, 8, 8, 8},
            {0, 0, 0, 7, 7, 7, 7, 7, 7},
            {0, 0, 6, 6, 6, 6, 6, 6, 6},
            {0, 5, 5, 5, 5, 5, 5, 5, 5},
            {4, 4, 4, 4, 4, 4, 4, 4, 4},
            {3, 3, 3, 3, 3, 3, 3, 3, 0},
            {2, 2, 2, 2, 2, 2, 2, 0, 0},
            {1, 1, 1, 1, 1, 1, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0}};

    private static int[][] ROTATE_1_LEFT_Y = {
            {0, 0, 0, 0, 0, 1, 2, 3, 4},
            {0, 0, 0, 0, 1, 2, 3, 4, 5},
            {0, 0, 0, 1, 2, 3, 4, 5, 6},
            {0, 0, 1, 2, 3, 4, 5, 6, 7},
            {0, 1, 2, 3, 4, 5, 6, 7, 8},
            {1, 2, 3, 4, 5, 6, 7, 8, 0},
            {2, 3, 4, 5, 6, 7, 8, 0, 0},
            {3, 4, 5, 6, 7, 8, 0, 0, 0},
            {4, 5, 6, 7, 8, 0, 0, 0, 0}};

    // ===== Turn 60� left + mirroring
    private static int[][] ROTATE_1_LEFT_MIRROR_X = {
            {0, 0, 0, 0, 8, 8, 8, 8, 8},
            {0, 0, 0, 7, 7, 7, 7, 7, 7},
            {0, 0, 6, 6, 6, 6, 6, 6, 6},
            {0, 5, 5, 5, 5, 5, 5, 5, 5},
            {4, 4, 4, 4, 4, 4, 4, 4, 4},
            {3, 3, 3, 3, 3, 3, 3, 3, 0},
            {2, 2, 2, 2, 2, 2, 2, 0, 0},
            {1, 1, 1, 1, 1, 1, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0}};

    private static int[][] ROTATE_1_LEFT_MIRROR_Y = {
            {0, 0, 0, 0, 4, 3, 2, 1, 0},
            {0, 0, 0, 5, 4, 3, 2, 1, 0},
            {0, 0, 6, 5, 4, 3, 2, 1, 0},
            {0, 7, 6, 5, 4, 3, 2, 1, 0},
            {8, 7, 6, 5, 4, 3, 2, 1, 0},
            {8, 7, 6, 5, 4, 3, 2, 1, 0},
            {8, 7, 6, 5, 4, 3, 2, 0, 0},
            {8, 7, 6, 5, 4, 3, 0, 0, 0},
            {8, 7, 6, 5, 4, 0, 0, 0, 0}};

    // ===== Turn 120� left =====
    private static int[][] ROTATE_2_LEFT_X = {
            {0, 0, 0, 0, 8, 7, 6, 5, 4},
            {0, 0, 0, 8, 7, 6, 5, 4, 3},
            {0, 0, 8, 7, 6, 5, 4, 3, 2},
            {0, 8, 7, 6, 5, 4, 3, 2, 1},
            {8, 7, 6, 5, 4, 3, 2, 1, 0},
            {7, 6, 5, 4, 3, 2, 1, 0, 0},
            {6, 5, 4, 3, 2, 1, 0, 0, 0},
            {5, 4, 3, 2, 1, 0, 0, 0, 0},
            {4, 3, 2, 1, 0, 0, 0, 0, 0}};

    private static int[][] ROTATE_2_LEFT_Y = {
            {0, 0, 0, 0, 4, 5, 6, 7, 8},
            {0, 0, 0, 3, 4, 5, 6, 7, 8},
            {0, 0, 2, 3, 4, 5, 6, 7, 8},
            {0, 1, 2, 3, 4, 5, 6, 7, 8},
            {0, 1, 2, 3, 4, 5, 6, 7, 8},
            {0, 1, 2, 3, 4, 5, 6, 7, 0},
            {0, 1, 2, 3, 4, 5, 6, 0, 0},
            {0, 1, 2, 3, 4, 5, 0, 0, 0},
            {0, 1, 2, 3, 4, 0, 0, 0, 0}};

    // =====Turn 120� left + mirroring =====
    private static int[][] ROTATE_2_LEFT_MIRROR_X = {
            {0, 0, 0, 0, 4, 5, 6, 7, 8},
            {0, 0, 0, 3, 4, 5, 6, 7, 8},
            {0, 0, 2, 3, 4, 5, 6, 7, 8},
            {0, 1, 2, 3, 4, 5, 6, 7, 8},
            {0, 1, 2, 3, 4, 5, 6, 7, 8},
            {0, 1, 2, 3, 4, 5, 6, 7, 0},
            {0, 1, 2, 3, 4, 5, 6, 0, 0},
            {0, 1, 2, 3, 4, 5, 0, 0, 0},
            {0, 1, 2, 3, 4, 0, 0, 0, 0}};

    private static int[][] ROTATE_2_LEFT_MIRROR_Y = {
            {0, 0, 0, 0, 8, 7, 6, 5, 4},
            {0, 0, 0, 8, 7, 6, 5, 4, 3},
            {0, 0, 8, 7, 6, 5, 4, 3, 2},
            {0, 8, 7, 6, 5, 4, 3, 2, 1},
            {8, 7, 6, 5, 4, 3, 2, 1, 0},
            {7, 6, 5, 4, 3, 2, 1, 0, 0},
            {6, 5, 4, 3, 2, 1, 0, 0, 0},
            {5, 4, 3, 2, 1, 0, 0, 0, 0},
            {4, 3, 2, 1, 0, 0, 0, 0, 0}};

    // ===== Turn 180� left =====
    private static int[][] ROTATE_3_LEFT_X = {
            {0, 0, 0, 0, 4, 3, 2, 1, 0},
            {0, 0, 0, 5, 4, 3, 2, 1, 0},
            {0, 0, 6, 5, 4, 3, 2, 1, 0},
            {0, 7, 6, 5, 4, 3, 2, 1, 0},
            {8, 7, 6, 5, 4, 3, 2, 1, 0},
            {8, 7, 6, 5, 4, 3, 2, 1, 0},
            {8, 7, 6, 5, 4, 3, 2, 0, 0},
            {8, 7, 6, 5, 4, 3, 0, 0, 0},
            {8, 7, 6, 5, 4, 0, 0, 0, 0}};

    private static int[][] ROTATE_3_LEFT_Y = {
            {0, 0, 0, 0, 8, 8, 8, 8, 8},
            {0, 0, 0, 7, 7, 7, 7, 7, 7},
            {0, 0, 6, 6, 6, 6, 6, 6, 6},
            {0, 5, 5, 5, 5, 5, 5, 5, 5},
            {4, 4, 4, 4, 4, 4, 4, 4, 4},
            {3, 3, 3, 3, 3, 3, 3, 3, 0},
            {2, 2, 2, 2, 2, 2, 2, 0, 0},
            {1, 1, 1, 1, 1, 1, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0}};

    // ===== Turn 180� left + mirroring
    private static int[][] ROTATE_3_LEFT_MIRROR_X = {
            {0, 0, 0, 0, 0, 1, 2, 3, 4},
            {0, 0, 0, 0, 1, 2, 3, 4, 5},
            {0, 0, 0, 1, 2, 3, 4, 5, 6},
            {0, 0, 1, 2, 3, 4, 5, 6, 7},
            {0, 1, 2, 3, 4, 5, 6, 7, 8},
            {1, 2, 3, 4, 5, 6, 7, 8, 0},
            {2, 3, 4, 5, 6, 7, 8, 0, 0},
            {3, 4, 5, 6, 7, 8, 0, 0, 0},
            {4, 5, 6, 7, 8, 0, 0, 0, 0}};

    private static int[][] ROTATE_3_LEFT_MIRROR_Y = {
            {0, 0, 0, 0, 8, 8, 8, 8, 8},
            {0, 0, 0, 7, 7, 7, 7, 7, 7},
            {0, 0, 6, 6, 6, 6, 6, 6, 6},
            {0, 5, 5, 5, 5, 5, 5, 5, 5},
            {4, 4, 4, 4, 4, 4, 4, 4, 4},
            {3, 3, 3, 3, 3, 3, 3, 3, 0},
            {2, 2, 2, 2, 2, 2, 2, 0, 0},
            {1, 1, 1, 1, 1, 1, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0}};

    // ===== Turn 60� right =====
    private static int[][] ROTATE_1_RIGHT_X = {
            {0, 0, 0, 0, 0, 1, 2, 3, 4},
            {0, 0, 0, 0, 1, 2, 3, 4, 5},
            {0, 0, 0, 1, 2, 3, 4, 5, 6},
            {0, 0, 1, 2, 3, 4, 5, 6, 7},
            {0, 1, 2, 3, 4, 5, 6, 7, 8},
            {1, 2, 3, 4, 5, 6, 7, 8, 0},
            {2, 3, 4, 5, 6, 7, 8, 0, 0},
            {3, 4, 5, 6, 7, 8, 0, 0, 0},
            {4, 5, 6, 7, 8, 0, 0, 0, 0}};

    private static int[][] ROTATE_1_RIGHT_Y = {
            {0, 0, 0, 0, 4, 3, 2, 1, 0},
            {0, 0, 0, 5, 4, 3, 2, 1, 0},
            {0, 0, 6, 5, 4, 3, 2, 1, 0},
            {0, 7, 6, 5, 4, 3, 2, 1, 0},
            {8, 7, 6, 5, 4, 3, 2, 1, 0},
            {8, 7, 6, 5, 4, 3, 2, 1, 0},
            {8, 7, 6, 5, 4, 3, 2, 0, 0},
            {8, 7, 6, 5, 4, 3, 0, 0, 0},
            {8, 7, 6, 5, 4, 0, 0, 0, 0}};

    // ===== Turn 60� right + mirroring =====
    private static int[][] ROTATE_1_RIGHT_MIRROR_X = {
            {0, 0, 0, 0, 4, 3, 2, 1, 0},
            {0, 0, 0, 5, 4, 3, 2, 1, 0},
            {0, 0, 6, 5, 4, 3, 2, 1, 0},
            {0, 7, 6, 5, 4, 3, 2, 1, 0},
            {8, 7, 6, 5, 4, 3, 2, 1, 0},
            {8, 7, 6, 5, 4, 3, 2, 1, 0},
            {8, 7, 6, 5, 4, 3, 2, 0, 0},
            {8, 7, 6, 5, 4, 3, 0, 0, 0},
            {8, 7, 6, 5, 4, 0, 0, 0, 0}};

    private static int[][] ROTATE_1_RIGHT_MIRROR_Y = {
            {0, 0, 0, 0, 0, 1, 2, 3, 4},
            {0, 0, 0, 0, 1, 2, 3, 4, 5},
            {0, 0, 0, 1, 2, 3, 4, 5, 6},
            {0, 0, 1, 2, 3, 4, 5, 6, 7},
            {0, 1, 2, 3, 4, 5, 6, 7, 8},
            {1, 2, 3, 4, 5, 6, 7, 8, 0},
            {2, 3, 4, 5, 6, 7, 8, 0, 0},
            {3, 4, 5, 6, 7, 8, 0, 0, 0},
            {4, 5, 6, 7, 8, 0, 0, 0, 0}};

    // ===== Turn 120� right =====
    private static int[][] ROTATE_2_RIGHT_X = {
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 1, 1, 1, 1, 1},
            {0, 0, 2, 2, 2, 2, 2, 2, 2},
            {0, 3, 3, 3, 3, 3, 3, 3, 3},
            {4, 4, 4, 4, 4, 4, 4, 4, 4},
            {5, 5, 5, 5, 5, 5, 5, 5, 0},
            {6, 6, 6, 6, 6, 6, 6, 0, 0},
            {7, 7, 7, 7, 7, 7, 0, 0, 0},
            {8, 8, 8, 8, 8, 0, 0, 0, 0}};

    private static int[][] ROTATE_2_RIGHT_Y = {
            {0, 0, 0, 0, 8, 7, 6, 5, 4},
            {0, 0, 0, 8, 7, 6, 5, 4, 3},
            {0, 0, 8, 7, 6, 5, 4, 3, 2},
            {0, 8, 7, 6, 5, 4, 3, 2, 1},
            {8, 7, 6, 5, 4, 3, 2, 1, 0},
            {7, 6, 5, 4, 3, 2, 1, 0, 0},
            {6, 5, 4, 3, 2, 1, 0, 0, 0},
            {5, 4, 3, 2, 1, 0, 0, 0, 0},
            {4, 3, 2, 1, 0, 0, 0, 0, 0}};

    // ===== Turn 120� right + mirroring =====
    private static int[][] ROTATE_2_RIGHT_MIRROR_X = {
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 1, 1, 1, 1, 1},
            {0, 0, 2, 2, 2, 2, 2, 2, 2},
            {0, 3, 3, 3, 3, 3, 3, 3, 3},
            {4, 4, 4, 4, 4, 4, 4, 4, 4},
            {5, 5, 5, 5, 5, 5, 5, 5, 0},
            {6, 6, 6, 6, 6, 6, 6, 0, 0},
            {7, 7, 7, 7, 7, 7, 0, 0, 0},
            {8, 8, 8, 8, 8, 0, 0, 0, 0}};

    private static int[][] ROTATE_2_RIGHT_MIRROR_Y = {
            {0, 0, 0, 0, 4, 5, 6, 7, 8},
            {0, 0, 0, 3, 4, 5, 6, 7, 8},
            {0, 0, 2, 3, 4, 5, 6, 7, 8},
            {0, 1, 2, 3, 4, 5, 6, 7, 8},
            {0, 1, 2, 3, 4, 5, 6, 7, 8},
            {0, 1, 2, 3, 4, 5, 6, 7, 0},
            {0, 1, 2, 3, 4, 5, 6, 0, 0},
            {0, 1, 2, 3, 4, 5, 0, 0, 0},
            {0, 1, 2, 3, 4, 0, 0, 0, 0}};
}