package de.uniks.abalone.app;

import de.uniks.networkparser.ext.javafx.window.FXStageController;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * This class starts the application 'Abalone'. The first visible screen is the {@code StartScreen.fxml}.
 */
public class Abalone extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXStageController.show(stage, "/fxml/StartScreen.fxml", this.getClass());
    }
}