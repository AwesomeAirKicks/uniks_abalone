package de.uniks.abalone.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Random;

import de.uniks.abalone.model.AIPlayer;
import de.uniks.abalone.model.Board;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Logic;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.util.GameCreator;
import de.uniks.abalone.tools.Layouts;
import de.uniks.ai.AI;
import de.uniks.ai.AlphaBeta;
import de.uniks.ai.IterativeDeepening;
import de.uniks.ai.MiniMax;
import de.uniks.networkparser.IdMap;
import de.uniks.networkparser.ext.javafx.window.FXStageController;
import de.uniks.networkparser.ext.javafx.window.StageEvent;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * The {@code StartScreenController} provides the whole functionality for the start screen. It defines when every single
 * node is visible, creates the {@code Game} object and starts the game. Normally the next stage is the game screen.
 */
public class StartScreenController implements StageEvent {
    // Stage attributes
    private Stage stage;

    private FXStageController controller;

    ArrayList<String> names = new ArrayList<>();

    // Controls for player 1
    @FXML
    private TextField tf_name1;

    @FXML
    private ColorPicker cp_color1;

    @FXML
    private RadioButton rb_human1;

    @FXML
    private RadioButton rb_minimax1;

    @FXML
    private RadioButton rb_alphabeta1;

    @FXML
    private RadioButton rb_deepening1;

    @FXML
    private Label lbl_level1;

    @FXML
    private Slider sld_level1;

    @FXML
    private Label lbl_value1;

    // Controls for player 2
    @FXML
    private TextField tf_name2;

    @FXML
    private ColorPicker cp_color2;

    @FXML
    private RadioButton rb_human2;

    @FXML
    private RadioButton rb_minimax2;

    @FXML
    private RadioButton rb_alphabeta2;

    @FXML
    private RadioButton rb_deepening2;

    @FXML
    private Label lbl_level2;

    @FXML
    private Slider sld_level2;

    @FXML
    private Label lbl_value2;

    // General controls
    @FXML
    private RadioButton rb_classic;

    @FXML
    private RadioButton rb_belgianDaisy;

    @FXML
    private CheckBox cb_logfile;

    // Initializes the stage and all the nodes with properties and listeners
    private void initStage() {
        // General settings
        Image icon = new Image(this.getClass().getResource("/gfx/icon.png").toString());
        this.stage.getIcons().add(icon);
        this.stage.setTitle("Abalone");
        this.stage.centerOnScreen();

        // By default player one is black and player two is white
        this.cp_color1.setValue(Color.BLACK);
        this.cp_color2.setValue(Color.WHITE);

        // If an AI is selected, the player gets a random name. Also further settings can be adjusted
        Random r = new Random();
        try {
            this.names = getNames();
        } catch(Exception e) {
            e.printStackTrace();
        }
        this.rb_human1.setOnAction(evt -> {
            this.tf_name1.setText("");
            this.lbl_level1.setVisible(false);
            this.sld_level1.setVisible(false);
            this.lbl_value1.setVisible(false);
        });
        this.rb_minimax1.setOnAction(evt -> {
            this.tf_name1.setText(this.names.get(Math.abs(r.nextInt() % this.names.size())));
            this.lbl_level1.setText("Level");
            this.sld_level1.setMax(10);
            this.sld_level1.setMin(1);
            this.lbl_value1.setText(String.valueOf((int) this.sld_level1.getValue()));
            this.lbl_level1.setVisible(true);
            this.sld_level1.setVisible(true);
            this.lbl_value1.setVisible(true);
        });
        this.rb_alphabeta1.setOnAction(evt -> {
            this.tf_name1.setText(this.names.get(Math.abs(r.nextInt() % this.names.size())));
            this.lbl_level1.setText("Level");
            this.sld_level1.setMax(10);
            this.sld_level1.setMin(1);
            this.lbl_value1.setText(String.valueOf((int) this.sld_level1.getValue()));
            this.lbl_level1.setVisible(true);
            this.sld_level1.setVisible(true);
            this.lbl_value1.setVisible(true);
        });
        this.rb_deepening1.setOnAction(evt -> {
            this.tf_name1.setText(this.names.get(Math.abs(r.nextInt() % this.names.size())));
            this.lbl_level1.setText("Time");
            this.sld_level1.setMax(30);
            this.sld_level1.setMin(0);
            this.lbl_value1.setText(String.valueOf((int) this.sld_level1.getValue()) + 's');
            this.lbl_level1.setVisible(true);
            this.sld_level1.setVisible(true);
            this.lbl_value1.setVisible(true);
        });
        this.rb_human2.setOnAction(evt -> {
            this.tf_name2.setText("");
            this.lbl_level2.setVisible(false);
            this.sld_level2.setVisible(false);
            this.lbl_value2.setVisible(false);
        });
        this.rb_minimax2.setOnAction(evt -> {
            this.tf_name2.setText(this.names.get(Math.abs(r.nextInt() % this.names.size())));
            this.lbl_level2.setText("Level");
            this.sld_level2.setMax(10);
            this.sld_level2.setMin(1);
            this.lbl_value2.setText(String.valueOf((int) this.sld_level2.getValue()));
            this.lbl_level2.setVisible(true);
            this.sld_level2.setVisible(true);
            this.lbl_value2.setVisible(true);
        });
        this.rb_alphabeta2.setOnAction(evt -> {
            this.tf_name2.setText(this.names.get(Math.abs(r.nextInt() % this.names.size())));
            this.lbl_level2.setText("Level");
            this.sld_level2.setMax(10);
            this.sld_level2.setMin(1);
            this.lbl_value2.setText(String.valueOf((int) this.sld_level2.getValue()));
            this.lbl_level2.setVisible(true);
            this.sld_level2.setVisible(true);
            this.lbl_value2.setVisible(true);
        });
        this.rb_deepening2.setOnAction(evt -> {
            this.tf_name2.setText(this.names.get(Math.abs(r.nextInt() % this.names.size())));
            this.lbl_level2.setText("Time");
            this.sld_level2.setMax(30);
            this.sld_level2.setMin(0);
            this.lbl_value2.setText(String.valueOf((int) this.sld_level2.getValue()) + 's');
            this.lbl_level2.setVisible(true);
            this.sld_level2.setVisible(true);
            this.lbl_value2.setVisible(true);
        });

        // Add tooltips to the radio buttons
        Tooltip human = new Tooltip(
                "A human player is only able to calculate as many moves in advance as its brain is able to compute.");
        human.setWrapText(true);
        human.setMaxWidth(150);
        this.rb_human1.setTooltip(human);
        this.rb_human2.setTooltip(human);

        Tooltip minimax = new Tooltip(
                "The MiniMax-AI calculates every possible Move.\n\n"
                        + "The level defines the number of moves that the AI calculates in advance.");
        minimax.setWrapText(true);
        minimax.setMaxWidth(150);
        this.rb_minimax1.setTooltip(minimax);
        this.rb_minimax2.setTooltip(minimax);

        Tooltip alphabeta = new Tooltip(
                "The AlphaBeta-AI uses some clever extensions to drastically reduce the number of moves to be calculated.\n\n"
                        + "The level defines the number of moves that the AI calculates in advance.");
        alphabeta.setWrapText(true);
        alphabeta.setMaxWidth(150);
        this.rb_alphabeta1.setTooltip(alphabeta);
        this.rb_alphabeta2.setTooltip(alphabeta);

        Tooltip deepening = new Tooltip(
                "Instead of the depth-first search like MiniMax or AlphaBeta, the Iterative Deepening uses the breadth-first search technique.\n\n"
                        + "The time is the limit of the search for every turn.\n\n"
                        + "It is also possible to interrupt this AI by pressing the Space Bar.");
        deepening.setWrapText(true);
        deepening.setMaxWidth(150);
        this.rb_deepening1.setTooltip(deepening);
        this.rb_deepening2.setTooltip(deepening);

        // The current value will be displayed next to the slider
        this.sld_level1.valueProperty().addListener((ChangeListener<Number>) (observable, oldValue,
                newValue) -> {
            int value = newValue.intValue();
            StartScreenController.this.lbl_value1
                    .setText(String.valueOf(value) + (this.rb_deepening1.isSelected() ? "s" : ""));
        });
        this.sld_level2.valueProperty().addListener((ChangeListener<Number>) (observable, oldValue,
                newValue) -> {
            int value = newValue.intValue();
            StartScreenController.this.lbl_value2
                    .setText(String.valueOf(value) + (this.rb_deepening2.isSelected() ? "s" : ""));
        });
    }

    /**
     * This method gets called by the 'Start Game' button.
     * <p>
     * At first it validates all parameters for the players. Afterwards it creates the {@code Game} object and starts
     * the game.
     */
    @FXML
    private void startGame() {
        // Both players need a name
        if(this.tf_name1.getText().isEmpty() || this.tf_name2.getText().isEmpty()) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Warning!");
            alert.setHeaderText("Please enter a name for both players!");
            alert.showAndWait();
            return;
        }

        // The players must play with different colors
        if(this.cp_color1.getValue().equals(this.cp_color2.getValue())) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Warning!");
            alert.setHeaderText("Please choose different colors for the players!");
            alert.showAndWait();
            return;
        }

        // At first the playeres will be created
        Player player1 = null, player2 = null;

        // Player 1
        if(this.rb_human1.isSelected()) {
            player1 = new Player();
        } else if(this.rb_minimax1.isSelected()) {
            player1 = new AIPlayer().withStrength((int) this.sld_level1.getValue());
            AI<Logic, Board, Move, Player> ai = new MiniMax<>();
            ((AIPlayer) player1).setAi(ai);
        } else if(this.rb_alphabeta1.isSelected()) {
            player1 = new AIPlayer().withStrength((int) this.sld_level1.getValue());
            AI<Logic, Board, Move, Player> ai = new AlphaBeta<>();
            ((AIPlayer) player1).setAi(ai);
        } else {
            player1 = new AIPlayer().withStrength(Integer.MAX_VALUE);
            AI<Logic, Board, Move, Player> ai = new IterativeDeepening<>();
            ((IterativeDeepening<Logic, Board, Move, Player>) ai).setTimeLimit((int) this.sld_level1.getValue() * 1000);
            ((AIPlayer) player1).setAi(ai);
        }
        player1.withName(this.tf_name1.getText()).withColor(this.cp_color1.getValue().toString());

        // Player 2
        if(this.rb_human2.isSelected()) {
            player2 = new Player();
        } else if(this.rb_minimax2.isSelected()) {
            player2 = new AIPlayer().withStrength((int) this.sld_level2.getValue());
            AI<Logic, Board, Move, Player> ai = new MiniMax<>();
            ((AIPlayer) player2).setAi(ai);
        } else if(this.rb_alphabeta2.isSelected()) {
            player2 = new AIPlayer().withStrength((int) this.sld_level2.getValue());
            AI<Logic, Board, Move, Player> ai = new AlphaBeta<>();
            ((AIPlayer) player2).setAi(ai);
        } else {
            player2 = new AIPlayer().withStrength(Integer.MAX_VALUE);
            AI<Logic, Board, Move, Player> ai = new IterativeDeepening<>();
            ((IterativeDeepening<Logic, Board, Move, Player>) ai).setTimeLimit((int) this.sld_level2.getValue() * 1000);
            ((AIPlayer) player2).setAi(ai);
        }
        player2.withName(this.tf_name2.getText()).withColor(this.cp_color2.getValue().toString());

        // And now the game
        Game game = new Game();
        int mode = 0;
        if(this.rb_classic.isSelected()) {
            game.init(player1, player2, Layouts.DEFAULT_TOKEN_LAYOUT);
        } else {
            game.init(player1, player2, Layouts.BELGIAN_DAISY_TOKEN_LAYOUT);
            mode = 1;
        }

        // Load the next stage and pass the game to the regarding controller
        this.controller.showNewStage("/fxml/GameScreen.fxml", this.getClass());
        ((GameScreenController) this.controller.getController()).initStage(game, mode, this.cb_logfile.isSelected());
    }

    /**
     * This function gets called by the 'Load Game' button.
     * <p>
     * Previous saved games can be loaded for continuation. Unfortunately it is not possible to continue the log file
     * which might have been created for the saved game.
     */
    @FXML
    private void loadGame() {
        // Initialize the file chooser
        FileChooser fc = new FileChooser();
        fc.setTitle("Load Game");
        String initDir = "";
        try {
            initDir = URLDecoder.decode(this.getClass().getResource("/savegames").getPath(), "UTF-8");
        } catch(UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        fc.setInitialDirectory(new File(initDir));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Abalone Game", "*.abl"));

        // Display the file chooser
        File file = fc.showOpenDialog(this.stage);
        if(file != null) {
            String content = "";
            Game game = null;
            int mode = 0;

            // We used the ID map from the 'NetworkParser' library to store the data model.
            try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
                content = reader.readLine();
                IdMap map = GameCreator.createIdMap("42");
                game = (Game) map.decode(content);

                // If there was an AIPlayer, the regarding AI has to be set manually
                for(Player player: game.getPlayers()) {
                    if(player instanceof AIPlayer) {
                        content = reader.readLine();
                        if(content.endsWith("MiniMax")) {
                            AI<Logic, Board, Move, Player> ai = new MiniMax<>();
                            ((AIPlayer) player).setAi(ai);
                        } else if(content.endsWith("AlphaBeta")) {
                            AI<Logic, Board, Move, Player> ai = new AlphaBeta<>();
                            ((AIPlayer) player).setAi(ai);
                        } else {
                            content = reader.readLine();
                            AI<Logic, Board, Move, Player> ai = new IterativeDeepening<>();
                            ((IterativeDeepening<Logic, Board, Move, Player>) ai).setTimeLimit(Long.parseLong(content));
                            ((AIPlayer) player).setAi(ai);
                        }
                    }
                }

                // For an eventual restart we need the initial token layout for the game
                content = reader.readLine();
                mode = Integer.parseInt(content);
                reader.close();
            } catch(IOException e) {
                e.printStackTrace();
            }

            // Load the next stage and pass the game to the regarding controller
            this.controller.showNewStage("/fxml/GameScreen.fxml", this.getClass());
            ((GameScreenController) this.controller.getController()).initStage(game, mode, false);
        }
    }

    // Initializes an array with names for the AI players
    private ArrayList<String> getNames() throws Exception {
        ArrayList<String> names = new ArrayList<>();
        String file = URLDecoder.decode(this.getClass().getResource("/txt/names.txt").getPath(), "UTF-8");
        BufferedReader in = new BufferedReader(new FileReader(file));
        String line = "";
        while((line = in.readLine()) != null) {
            names.add(line);
        }
        if(in != null) {
            in.close();
        }
        return names;
    }

    @Override
    public void stageClosing(WindowEvent event, Stage stage, FXStageController controller) {
        System.exit(0);
    }

    @Override
    public void stageShowing(WindowEvent event, Stage stage, FXStageController controller) {
        this.stage = stage;
        this.controller = controller;
        initStage();
    }
}