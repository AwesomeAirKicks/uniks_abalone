package de.uniks.abalone.controller;

import de.uniks.abalone.model.AIPlayer;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Move;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

/**
 * 
 * @date 25.05.2017
 */
public class HistoryController {
    private int counter = 1;

    private ObservableList<HistoryEntry> entryList = FXCollections.observableArrayList();

    public HistoryController(ListView<HistoryEntry> history_list, Game game) {
        history_list.setItems(entryList);
        for(Move move: game.getHistory()) {
            entryList.add(new HistoryEntry(move, counter++));
        }
        game.addPropertyChangeListener(Game.PROPERTY_HISTORY, e -> {
            if(e.getNewValue() != null) {
                entryList.add(new HistoryEntry((Move) e.getNewValue(), counter++));
            }
            if(e.getOldValue() != null) {
                removeEntry((Move) e.getOldValue());
                counter--;
            }
        });

        // Override the CellFactory to implement "DoubleClicked" actions on cells
        history_list.setCellFactory(
                new Callback<ListView<HistoryEntry>, ListCell<HistoryEntry>>() {
                    @Override
                    public ListCell<HistoryEntry> call(ListView<HistoryEntry> arg0) {
                        ListCell<HistoryEntry> cell = new ListCell<HistoryEntry>() {
                            @Override
                            public void updateItem(HistoryEntry item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(empty ? null : getString());
                                setGraphic(null);
                            }

                            private String getString() {
                                return getItem() == null ? "" : getItem().toString();
                            }
                        };
                        cell.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                            @SuppressWarnings("unchecked")
                            @Override
                            public void handle(MouseEvent event) {
                                if(event.getClickCount() > 1) {
                                    System.out.println("double clicked!");
                                    ListCell<HistoryEntry> c = (ListCell<HistoryEntry>) event.getSource();
                                    if(c.getItem() != null) {
                                        if(!(game.getCurrentPlayer() instanceof AIPlayer)) {
                                            game.restoreSelectedBoard(c.getItem().getMove());
                                        }
                                    }
                                }
                            }
                        });
                        return cell;
                    }
                });
    }

    private void removeEntry(Move move) {
        for(HistoryEntry entry: entryList) {
            if(entry.getMove() == move) {
                entryList.remove(entry);
                return;
            }
        }
    }
}
