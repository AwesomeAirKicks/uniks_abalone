package de.uniks.abalone.controller;

import java.beans.PropertyChangeEvent;
import java.util.Optional;

import de.uniks.abalone.model.AIPlayer;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Player;
import de.uniks.networkparser.ext.javafx.controller.AbstractModelController;
import de.uniks.networkparser.ext.javafx.window.FXStageController;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.shape.Circle;

/**
 * The {@code GameController} manages different changes in the data model and publishes them in the gui.
 * <p>
 * <ul>
 * <li>The current player's name label will be underlined.
 * <li>If a player scores, it will be published via the regarding number of circles after his name label.
 * <li>When a player won the game, a dialog shows up with further options.
 * </ul>
 */
public class GameController extends AbstractModelController {
    private Game game;

    private FXStageController controller;

    private GameScreenController gsc;

    public GameController(Game game, FXStageController controller, GameScreenController gsc) {
        this.game = game;
        this.controller = controller;
        this.gsc = gsc;

        // Add the necessary listener
        addListener(game, Game.PROPERTY_CURRENTPLAYER);
        addListener(game, Game.PROPERTY_WINNER);
        for(Player player: game.getPlayers()) {
            addListener(player, Player.PROPERTY_SCORES);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        // Behind the name label of every player are 6 invisible circles. Whenever a player receives or loses a point,
        // the regarding circle will be shown or hidden.
        if(Player.PROPERTY_SCORES.equals(evt.getPropertyName())) {
            Player player = (Player) evt.getSource();
            int playerNumber = this.game.getPlayers().indexOf(player) + 1;
            int oldScore = evt.getOldValue() == null ? 0 : (int) evt.getOldValue();
            int newScore = (int) evt.getNewValue();
            if(oldScore < newScore) {
                Circle circle = (Circle) this.controller.getElementById("#player" + playerNumber + "_" + newScore);
                circle.setVisible(true);
            } else if(oldScore > newScore) {
                Circle circle = (Circle) this.controller.getElementById("#player" + playerNumber + "_" + oldScore);
                circle.setVisible(false);
            }
        }

        // The current player's name label is underlined. When it's the opponent's turn, the underlining switches to his
        // name label.
        if(Game.PROPERTY_CURRENTPLAYER.equals(evt.getPropertyName())) {
            Player oldPlayer = (Player) evt.getOldValue();
            Player newPlayer = (Player) evt.getNewValue();
            int oldID = this.game.getPlayers().indexOf(oldPlayer) + 1;
            int newID = this.game.getPlayers().indexOf(newPlayer) + 1;
            if(newPlayer != null) {
                Label label = (Label) this.controller.getElementById("#lbl_name" + newID);
                label.setUnderline(true);
                // It is only possible to save a game, when no AI is currently playing
                if(this.game.getCurrentPlayer() instanceof AIPlayer) {
                    this.gsc.getSaveMenuItem().setDisable(true);
                } else {
                    this.gsc.getSaveMenuItem().setDisable(false);
                }
            }
            if(oldPlayer != null) {
                Label label = (Label) this.controller.getElementById("#lbl_name" + oldID);
                label.setUnderline(false);
            }
        }

        // If a player won the game, it will be displayed in a dialog box.
        if(Game.PROPERTY_WINNER.equals(evt.getPropertyName())) {
            if(evt.getNewValue() != null) {
                Player winner = (Player) evt.getNewValue();
                Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle("Game Over!");
                alert.setHeaderText("Congratulations! " + winner.getName() + " is the Winner!");
                alert.setContentText("Do you want to play again?");
                ButtonType startNew = new ButtonType("Start new Game");
                ButtonType restart = new ButtonType("Restart this Game");
                ButtonType exit = new ButtonType("Exit Game", ButtonData.CANCEL_CLOSE);
                alert.getButtonTypes().setAll(startNew, restart, exit);
                Optional<ButtonType> result = alert.showAndWait();
                if(result.get() == startNew) {
                    this.gsc.startNewGame();
                } else if(result.get() == restart) {
                    this.gsc.restartGame();
                } else {
                    this.gsc.exitGame();
                }
            }
        }
    }

    @Override
    public void initPropertyChange(Object model, Node gui) {
    }
}