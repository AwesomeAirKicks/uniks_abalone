package de.uniks.abalone.controller;

import java.beans.PropertyChangeEvent;

import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Move;
import de.uniks.networkparser.ext.javafx.controller.AbstractModelController;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * The {@code FieldController} manages all changes regarding to the {@code Field} objects. This includes the relocating
 * on or removing from the board and the highlighting.
 */
public class FieldController extends AbstractModelController implements EventHandler<MouseEvent> {
    private Circle crl_field;

    private Field field;

    private double x;

    private double y;

    public FieldController(Field field, GameScreenController gsc) {
        this.crl_field = new Circle();
        this.field = field;

        // Calculate size and position of the circle and fill it with the player's color or transparency.
        Double boardCenterX = gsc.getBoardWidth() / 2;
        Double boardCenterY = gsc.getBoardWidth() * (Math.sqrt(3) / 4);
        Double deltaX = gsc.getBoardWidth() / 9;
        Double deltaY = gsc.getBoardWidth() * (Math.sqrt(3) / 18);
        this.crl_field.setRadius(0.9 * gsc.getBoardWidth() * (Math.sqrt(3) / 40));
        this.crl_field.setStroke(Color.BLACK);
        this.crl_field.setStrokeWidth(3);
        if(this.field.getToken() != null) {
            String color = this.field.getToken().getPlayer().getColor();
            this.crl_field.setFill(Color.web(color));
        } else {
            this.crl_field.setFill(Color.TRANSPARENT);
        }
        this.crl_field
                .setCenterX(boardCenterX + (this.field.getX() - 4) * deltaX + (this.field.getY() - 4) * (deltaX / 2));
        this.crl_field.setCenterY(boardCenterY + (this.field.getY() - 4) * deltaY);

        // Add all necessary listener and handler
        addListener(this.field, Field.PROPERTY_TOKEN);
        addListener(this.field, Field.PROPERTY_SELECTED);
        addListener(this.field, Field.PROPERTY_LASTMOVED);
        this.crl_field.setOnMouseClicked(this);
        this.crl_field.setOnMousePressed(this);
        this.crl_field.setOnMouseReleased(this);

        // Add the field to the board
        gsc.getBoardGroup().getChildren().add(this.crl_field);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        // A change at the 'token' property has been fired
        if(Field.PROPERTY_TOKEN.equals(evt.getPropertyName())) {
            if(evt.getNewValue() != null) {
                // A token has been placed on the field
                String color = this.field.getToken().getPlayer().getColor();
                this.crl_field.setFill(Color.web(color));
            } else {
                // A token has been removed from the field
                this.crl_field.setFill(Color.TRANSPARENT);
            }
        }
        // A change at the 'selected' property has been fired
        if(Field.PROPERTY_SELECTED.equals(evt.getPropertyName())) {
            if(evt.getNewValue().equals(true)) {
                // The field has been selected for a potentially move
                this.crl_field.setEffect(glow(Color.GREENYELLOW));
            } else if(evt.getNewValue().equals(false) && evt.getOldValue() != null) {
                // The field has been deselected again
                if(!((Field) evt.getSource()).isLastMoved()) {
                    this.crl_field.setEffect(null);
                }
            }
        }
        // A change at the 'lastMoved' property has been fired
        if(Field.PROPERTY_LASTMOVED.equals(evt.getPropertyName())) {
            if(evt.getNewValue().equals(true)) {
                this.crl_field.setEffect(glow(Color.DODGERBLUE));
            } else if(evt.getNewValue().equals(false) && evt.getOldValue() != null) {
                // The 'lastMoved' flag is no longer necessary
                this.crl_field.setEffect(null);
            }
        }
    }

    @Override
    public void handle(MouseEvent event) {
        // If it's an AI's turn, no events will be handled
        if(this.field.getGame().getCurrentPlayer() instanceof de.uniks.abalone.model.AIPlayer) {
            return;
        }
        // If there's already a winner, no further moves are possible
        if(this.field.getGame().getWinner() != null) {
            return;
        }
        // Clicking on a field passes it to the logic for validation
        if(event.getEventType().equals(MouseEvent.MOUSE_CLICKED)) {
            this.field.getGame().getLogic().passField(this.field);
        }
        // Dragging selected fields in a certain direction will pass this direction to the logic
        if(this.field.isSelected() && event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
            // Store the position of the cursor
            this.x = event.getX();
            this.y = event.getY();
        }
        if(this.field.isSelected() && event.getEventType().equals(MouseEvent.MOUSE_RELEASED)) {
            // Point of release = point of press? -> No drag
            if(this.x == event.getX() && this.y == event.getY()) {
                return;
            } else {
                // Calculate the angle
                int direction = -1;
                Double deltaX = event.getX() - this.x;
                Double deltaY = (-1) * (event.getY() - this.y);
                Double angle = Math.toDegrees(Math.atan2(deltaY, deltaX));
                if(angle < 0) {
                    angle = 360 + angle;
                }
                if(angle >= 330 || angle < 30) {
                    direction = Move.RIGHT;
                } else if(angle < 90) {
                    direction = Move.TOP_RIGHT;
                } else if(angle < 150) {
                    direction = Move.TOP_LEFT;
                } else if(angle < 210) {
                    direction = Move.LEFT;
                } else if(angle < 270) {
                    direction = Move.DOWN_LEFT;
                } else {
                    direction = Move.DOWN_RIGHT;
                }
                this.field.getGame().getLogic().passDirection(direction);
            }
        }
    }

    private DropShadow glow(Color color) {
        DropShadow glow = new DropShadow();
        glow.setColor(color);
        glow.setBlurType(BlurType.GAUSSIAN);
        glow.setSpread(0.6);
        glow.setHeight(25);
        glow.setWidth(25);
        return glow;
    }

    @Override
    public void initPropertyChange(Object model, Node gui) {
    }
}