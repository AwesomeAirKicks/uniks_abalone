package de.uniks.ai;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Iterator;

/**
 * A TranspostionTable implementation based on an underlying HahMap to store the entries. In case of a hash collision,
 * the 'greater' entry wins ({@code ComparatorTTEntry}). The hashing function works as follows and may be changed in
 * future: Board -> BitSet(122bit) -> MD5(160bit) -> int(32bit) -> index(%MaxSize)
 * 
 * The MD5 instance is used to create some diffusion in the index space, else there are too many indices that will never
 * be hit during a standard Abalone game.
 */
public class TranspositionTableHM<M extends MoveInterface<M>> {

    public HashMap<Integer, TTEntry<M>> tableData;

    // some attributes for internal statistics
    public int size = 0;

    public int collision = 0;

    public int total = 0;

    private ArrayList<TTEntry<M>> orderData;

    private ComparatorTTEntry comperator = new ComparatorTTEntry();

    private MessageDigest md;

    private static final int MAX_SIZE = 1_000_000;

    private static final float CLEANUP_PERCENTAGE = 0.2f;

    public TranspositionTableHM() {
        // allocates 'MAX_SIZE' buckets and set the loadFactor >1
        // this prevents rehashing inside the HashMap implementation
        this.tableData = new HashMap<>(MAX_SIZE, 1.01f);
        this.orderData = new ArrayList<>();
        try {
            this.md = MessageDigest.getInstance("MD5");
        } catch(NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private boolean isFull() {
        return this.size >= MAX_SIZE;
    }

    private void cleanUp() {
        ArrayList<TTEntry<M>> removed = new ArrayList<>();
        this.orderData.sort(this.comperator);
        int remove = this.size - (int) (this.size * CLEANUP_PERCENTAGE);
        Iterator<TTEntry<M>> iter = this.orderData.iterator();
        int counter = 0;
        while(counter < remove) {
            TTEntry<M> toRemove = iter.next();
            BigInteger bi = new BigInteger(this.md.digest(toRemove.getId().toByteArray()));
            int index = bi.intValue() % MAX_SIZE;
            index = (index < 0 ? index * -1 : index);
            this.tableData.remove(index, toRemove);
            removed.add(toRemove);
            this.size--;
            counter++;
        }
        this.orderData.removeAll(removed);
        removed.clear();
    }

    /**
     * Returns the entry to which the specified id is mapped.
     * 
     * @param id of the board
     * @param requiredDepth
     * @param imPlayerOne
     * @return The entry or null
     */
    public TTEntry<M> get(BitSet id, int requiredDepth, boolean imPlayerOne) {
        BigInteger bi = new BigInteger(this.md.digest(id.toByteArray()));
        int index = bi.intValue() % MAX_SIZE;
        index = (index < 0 ? index * -1 : index);
        TTEntry<M> entry = this.tableData.get(index);
        if(entry != null
                && entry.getId().equals(id)
                && entry.getDepth() >= requiredDepth
                && entry.isImPlayerOne() == imPlayerOne) {
            entry.increaseAccessCounter();
            return entry;
        }
        return null;
    }

    /**
     * Stores the specified entry this TranspositionTable.
     * 
     * @param entry
     */
    public void put(TTEntry<M> entry) {
        this.total++;
        if(isFull()) {
            cleanUp();
        }
        if(!isFull()) {
            BigInteger bi = new BigInteger(this.md.digest(entry.getId().toByteArray()));
            int index = bi.intValue() % MAX_SIZE;
            index = (index < 0 ? index * -1 : index);
            TTEntry<M> storedEntry = this.tableData.get(index);
            if(storedEntry != null) {
                this.collision++;
                if(this.comperator.compare(entry, storedEntry) >= 1) {
                    this.tableData.put(index, entry);
                    this.orderData.remove(storedEntry);
                    this.orderData.add(entry);
                }
            } else {
                this.tableData.put(index, entry);
                this.orderData.add(entry);
                this.size++;
            }
        }
    }
}
