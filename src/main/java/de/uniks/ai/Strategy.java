package de.uniks.ai;

import java.util.ArrayList;

public class Strategy<M extends MoveInterface<M>> {

    private ArrayList<M> moveList = new ArrayList<M>();

    private int score;

    public void setMoveList(ArrayList<M> moveList) {
        this.moveList = moveList;
    }

    public ArrayList<M> getMoveList() {
        return moveList;
    }

    @SuppressWarnings("unused")
    private void setList(ArrayList<M> moveList) {
        this.moveList = moveList;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String toString() {
        StringBuilder string = new StringBuilder();
        for(M m: getMoveList()) {
            string.append(m.toString());
        }
        return string.toString();
    }
}
