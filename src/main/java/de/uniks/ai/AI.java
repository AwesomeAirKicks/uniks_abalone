package de.uniks.ai;

import javafx.beans.property.SimpleStringProperty;

/**
 * The superclass for A.I. implementations
 */
public abstract class AI<L extends LogicInterface<B, M, P>, B extends BoardInterface, M extends MoveInterface<M>, P extends PlayerInterface> {

    public abstract M calculateBestMove(L logic, P playerMax, P playerMin, B board, int depth);

    // ==========================================================================

    private SimpleStringProperty statusString = new SimpleStringProperty();

    /**
     * This string contains informations about the current calculation progress. Content: iteration, current best
     * strategy
     *
     * @return The status string
     */
    public SimpleStringProperty getStatusString() {
        return this.statusString;
    }

    protected void setStatusStringValue(String string) {
        this.statusString.setValue(string);
    }

    // ==========================================================================

    private SimpleStringProperty speedString = new SimpleStringProperty();

    /**
     * This string contains additional informations about the current calculation and will be updated every second.
     * Content: speed in Nodes/s, elapsed time
     *
     * @return The speed string
     */
    public SimpleStringProperty getSpeedString() {
        return this.speedString;
    }

    protected void setSpeedStringValue(String string) {
        this.speedString.setValue(string);
    }

    // ==========================================================================
    /**
     * This flag will be set, when the AI thread should stop
     */
    private volatile boolean stop = false;

    /**
     * Stops the AI calculation. In case of a MiniMax or AlphaBeta instance, the calculation will return a null Move.
     * Iterative Deepening ensures a non-null value at every time.
     */
    public void stopAiThread() {
        this.stop = true;
    }

    protected void setStopFlag(boolean value) {
        this.stop = value;
    }

    protected boolean getStopFlag() {
        return this.stop;
    }

    // ==========================================================================
    /**
     * used by ai.speedString for Nodes/s calculation
     */
    private int visitedNodes = 0;

    protected int getVisitedNodes() {
        return visitedNodes;
    }

    protected void setVisitedNodes(int visitedNodes) {
        this.visitedNodes = visitedNodes;
    }
}
