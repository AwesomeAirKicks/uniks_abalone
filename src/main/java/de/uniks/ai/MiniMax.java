package de.uniks.ai;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MiniMax<L extends LogicInterface<B, M, P>, B extends BoardInterface, M extends MoveInterface<M>, P extends PlayerInterface>
        extends AI<L, B, M, P> {
    private boolean traTableActive = true;

    public void setTraTableActive(boolean isActive) {
        traTableActive = isActive;
    }

    public int entriesUsed = 0;

    public TranspositionTableHM<M> tt = new TranspositionTableHM<>();

    private Strategy<M> bestStrategy;

    /**
     * getBestStrategy
     * 
     * @return
     */
    public Strategy<M> getBestStrategy() {
        return bestStrategy;
    }

    public void setBestStrategy(Strategy<M> bestStrategy) {
        this.bestStrategy = bestStrategy;
    }

    private int desiredDepth;

    public int getDesiredDepth() {
        return this.desiredDepth;
    }

    public void setDesiredDepth(int value) {
        if(this.desiredDepth != value) {
            this.desiredDepth = value;
        }
    }

    public MiniMax<L, B, M, P> withDesiredDepth(int value) {
        setDesiredDepth(value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(" ").append(this.getDesiredDepth());
        return result.substring(1);
    }

    // ==========================================================================

    private P maxPlayer;

    public P getMaxPlayer() {
        return this.maxPlayer;
    }

    public void setMaxPlayer(P value) {
        if(this.playerOne == null) {
            this.playerOne = value;
        }
        if(this.maxPlayer != value) {
            this.maxPlayer = value;
        }
    }

    public MiniMax<L, B, M, P> withMaxPlayer(P value) {
        setMaxPlayer(value);
        return this;
    }

    // ==========================================================================
    private P minPlayer;

    public P getMinPlayer() {
        return this.minPlayer;
    }

    public void setMinPlayer(P value) {
        if(this.minPlayer != value) {
            this.minPlayer = value;
        }
    }

    public MiniMax<L, B, M, P> withMinPlayer(P value) {
        setMinPlayer(value);
        return this;
    }

    // ==========================================================================
    // used by TranspositionTable
    private P playerOne;

    public boolean imPlayerOne(int i) {
        if(i == 1) {
            return(getMaxPlayer() == this.playerOne);
        }
        if(i == -1) {
            return(getMinPlayer() == this.playerOne);
        }
        return false;
    }

    // ==========================================================================
    public MiniMax() {
        // console output
        getStatusString().addListener((arg) -> {
            System.out.println(arg);
        });
    }

    // ==========================================================================
    /**
     * Based on the classic recursive minimax algorithm. Calculates the best possible move for a given board. The best
     * possible strategy can be accessed with the getBestStrategy method.
     * 
     * @param logic is an object of the Logic class.
     * @param playerMax Is an object of the Player class. Used to set the maximizing player.
     * @param playerMin Is an object of the Player class. Used to set the minimizing player.
     * @param board Is an object of the Board class.
     * @param depth is of type integer. Determines the maximum search depth.
     * 
     * @return move returns the best possible move for the given depth.
     */
    @Override
    public M calculateBestMove(L logic, P playerMax, P playerMin, B board, int depth) {

        // Sets up the maximizing, minimizing player and the maximum search
        // depth.
        this.setDesiredDepth(depth);
        this.setMaxPlayer(playerMax);
        this.setMinPlayer(playerMin);

        Timer timer = createTimerForSpeedString();
        // Starts the algorithm
        Strategy<M> strategy = negamax(logic, board, depth, 1, false);
        setBestStrategy(strategy);
        timer.cancel();

        if(strategy == null) {
            return null;
        }
        for(M m: strategy.getMoveList()) {
            System.out.println(m.toString());
        }
        return strategy.getMoveList().get(0);
    }

    private int evaluateNegaMax(L logic, B board, P player, int depth) {
        return logic.evaluate(board, player) - depth;
    }

    private Strategy<M> negamax(L logic, B board, int depth, int player, boolean wasHitMove) {
        setVisitedNodes(getVisitedNodes() + 1);

        // Get all possible moves from the minimizing or maximizing player
        ArrayList<M> allMoves = null;
        if(player == 1) {
            allMoves = logic.getAllMoves(this.maxPlayer, board);
        } else if(player == -1) {
            allMoves = logic.getAllMoves(this.minPlayer, board);
        }

        // Evaluate the board, if there are no more possibles moves or there is
        // a winner,
        // or if the maximum depth is reached and the last move wasn't a hit
        // move.
        if(allMoves.size() == 0 || logic.checkWinner(board) != null || (depth <= 0 && !wasHitMove)) {
            Strategy<M> strategy = new Strategy<>();
            int score = 0;
            if(player == 1) {
                score = evaluateNegaMax(logic, board, this.minPlayer, depth);
            } else if(player == -1) {
                score = evaluateNegaMax(logic, board, this.maxPlayer, depth);
            }
            // Set the score in the strategy object.
            strategy.setScore(score);
            return strategy;
        }

        int maxValue = Integer.MIN_VALUE;
        Strategy<M> bestStrategy = null;
        // Go through all possible moves.
        // Execute move, go one depth deeper and undo the move. Prevents a
        // memory overflow.
        for(int i = 0; i < allMoves.size(); i++) {
            // When the thread should stop its calculation, it returns 'null'...
            if(getStopFlag()) {
                return null;
            }
            // ...otherwise it continues its work.
            M move = allMoves.get(i);
            if(depth == this.desiredDepth) {
                setStatusStringValue(
                        "Simulate move " + (i + 1) + "/" + allMoves.size() + " | " + ((i + 1) * 100 / allMoves.size())
                                + "% done. \t\t\t Strategy: " + (bestStrategy != null ? bestStrategy.toString() : "")
                                + (bestStrategy != null ? (" with Score " + bestStrategy.getScore()) : ""));
            }
            logic.executeMoveSimulation(move);
            Strategy<M> returnedStrategy;
            if(traTableActive) {
                TTEntry<M> entry = this.tt.get(board.encode(), depth, imPlayerOne(player));
                if(entry != null) {
                    returnedStrategy = entry.getStrategyCopy();
                    int factor = player * entry.getPlayer();
                    returnedStrategy.setScore(returnedStrategy.getScore() * factor);
                    entriesUsed++;
                } else {
                    returnedStrategy = negamax(logic, board, depth - 1, -player, move.hitMove());
                    if(returnedStrategy != null) {
                        returnedStrategy.setScore(returnedStrategy.getScore() * -1);
                        this.tt.put(
                                new TTEntry<>(board.encode(), returnedStrategy, depth, imPlayerOne(player), player));
                    }
                }
            } else {
                // Call the recursive function with the new board situation.
                returnedStrategy = negamax(logic, board, depth - 1, -player, move.hitMove());
                // Added the calculated score to the calculated strategy object.
                if(returnedStrategy != null) {
                    returnedStrategy.setScore(returnedStrategy.getScore() * -1);
                }
            }
            // Undo the simulated move.
            logic.undoMoveSimulation(move);
            // Update the new calculated strategy, if it has a better score.
            if(returnedStrategy != null && returnedStrategy.getScore() > maxValue) {
                maxValue = returnedStrategy.getScore();
                // Add move to the strategy
                returnedStrategy.getMoveList().add(0, move);
                bestStrategy = returnedStrategy;
                if(depth == this.desiredDepth) {
                    setStatusStringValue("Simulate move " + (i + 1) + "/" + allMoves.size() + " | "
                            + ((i + 1) * 100 / allMoves.size()) + "% done. \t\t\t Strategy: "
                            + (bestStrategy != null ? bestStrategy.toString() : "")
                            + (bestStrategy != null ? (" with Score " + bestStrategy.getScore()) : ""));
                }
            }
        }

        // Set the status string, if the desired depth is reached.
        if(depth == this.desiredDepth) {
            setStatusStringValue(
                    "AI choosed strategy " + bestStrategy.toString() + "  with score " + bestStrategy.getScore());
        }
        return bestStrategy;
    }

    private Timer createTimerForSpeedString() {
        long startTime = System.currentTimeMillis();
        class StringTimer extends TimerTask {
            public void run() {
                String elapsedTimeString = "elapsed time: " + ((System.currentTimeMillis() - startTime) / 1000)
                        + " seconds";
                String speedString = "speed: " + getVisitedNodes() + " Nodes/s";

                getSpeedString().setValue(speedString + "\t\t\t " + elapsedTimeString);
                setVisitedNodes(0);
            }
        }
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new StringTimer(), 0, 1000);
        return timer;
    }
}
