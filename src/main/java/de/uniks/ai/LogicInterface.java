package de.uniks.ai;

import java.util.ArrayList;

public interface LogicInterface<B extends BoardInterface, M extends MoveInterface<M>, P extends PlayerInterface> {

    public P checkWinner(B board);

    public void executeMoveSimulation(M move);

    public void undoMoveSimulation(M move);

    public ArrayList<M> getAllMoves(P player, B board);

    public int evaluate(B board, P player);
}
