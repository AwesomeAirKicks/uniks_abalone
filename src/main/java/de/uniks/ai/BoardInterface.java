package de.uniks.ai;

import java.util.BitSet;

public interface BoardInterface {

    public BitSet encode();
}
