package de.uniks.ai;

import java.util.Comparator;

public class ComparatorTTEntry implements Comparator<TTEntry<?>> {

    @Override
    public int compare(TTEntry<?> arg0, TTEntry<?> arg1) {

        if(arg0.getDepth() < arg1.getDepth()) {
            return -1;
        }
        if(arg0.getDepth() > arg1.getDepth()) {
            return 1;
        }
        if(arg0.getDepth() == arg1.getDepth()) {
            if(arg0.getAccessCounter() < arg1.getAccessCounter()) {
                return -1;
            }
            if(arg0.getAccessCounter() > arg1.getAccessCounter()) {
                return 1;
            }
            if(arg0.getAccessCounter() == arg1.getAccessCounter()) {
                return 0;
            }

        }

        return 0;
    }
}
