/*
 * Copyright (c) 2017 Ich Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.uniks.tictactoe.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.BitSet;

import de.uniks.ai.BoardInterface;
import de.uniks.networkparser.interfaces.SendableEntity;
import de.uniks.tictactoe.model.util.FieldSet;

/**
 * 
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/tictactoe/ModelGenerator.java'>ModelGenerator.java</a>
 */
public class Board implements SendableEntity, BoardInterface {

    // ==========================================================================

    protected PropertyChangeSupport listeners = null;

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if(listeners != null) {
            listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if(listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(listener);
        return true;
    }

    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if(listeners == null) {
            listeners.removePropertyChangeListener(listener);
        }
        listeners.removePropertyChangeListener(listener);
        return true;
    }

    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(listeners != null) {
            listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    // ==========================================================================

    public void removeYou() {
        setGame(null);
        withoutFields(this.getFields().toArray(new Field[this.getFields().size()]));
        firePropertyChange("REMOVE_YOU", this, null);
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Board ----------------------------------- Game
     *              board                   game
     * </pre>
     */

    public static final String PROPERTY_GAME = "game";

    private Game game = null;

    public Game getGame() {
        return this.game;
    }

    public boolean setGame(Game value) {
        boolean changed = false;

        if(this.game != value) {
            Game oldValue = this.game;

            if(this.game != null) {
                this.game = null;
                oldValue.setBoard(null);
            }

            this.game = value;

            if(value != null) {
                value.withBoard(this);
            }

            firePropertyChange(PROPERTY_GAME, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Board withGame(Game value) {
        setGame(value);
        return this;
    }

    public Game createGame() {
        Game value = new Game();
        withGame(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       many
     * Board ----------------------------------- Field
     *              board                   fields
     * </pre>
     */

    public static final String PROPERTY_FIELDS = "fields";

    private FieldSet fields = null;

    public FieldSet getFields() {
        if(this.fields == null) {
            return FieldSet.EMPTY_SET;
        }

        return this.fields;
    }

    public Board withFields(Field... value) {
        if(value == null) {
            return this;
        }
        for(Field item: value) {
            if(item != null) {
                if(this.fields == null) {
                    this.fields = new FieldSet();
                }

                boolean changed = this.fields.add(item);

                if(changed) {
                    item.withBoard(this);
                    firePropertyChange(PROPERTY_FIELDS, null, item);
                }
            }
        }
        return this;
    }

    public Board withoutFields(Field... value) {
        for(Field item: value) {
            if((this.fields != null) && (item != null)) {
                if(this.fields.remove(item)) {
                    item.setBoard(null);
                    firePropertyChange(PROPERTY_FIELDS, item, null);
                }
            }
        }
        return this;
    }

    public Field createFields() {
        Field value = new Field();
        withFields(value);
        return value;
    }

    public void init() {
        buildFields();
    }

    private void buildFields() {

        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                Field field = new Field().withX(i).withY(j).withGame(getGame());
                this.withFields(field);
                this.withGame(getGame());
            }
        }

    }

    @Override
    public BitSet encode() {
        BitSet hashValue = new BitSet(9);
        int index = 0;

        for(Field field: fields) {
            if(field.getToken() != null) {
                if(field.getToken().getPlayer() == game.getPlayers().get(0)) {
                    // 01 for player1
                    hashValue.set(index++, true);
                    hashValue.set(index++, false);
                } else {
                    // 10 for player2
                    hashValue.set(index++, false);
                    hashValue.set(index++, true);
                }
            } else {
                // 00 for empty fields
                hashValue.set(index++, false);
                hashValue.set(index++, false);
            }
        }
        return hashValue;
    }
}
