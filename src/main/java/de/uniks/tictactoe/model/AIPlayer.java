/*
 * Copyright (c) 2017 Ich Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.uniks.tictactoe.model;

/**
 * 
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/tictactoe/ModelGenerator.java'>ModelGenerator.java</a>
 */
public class AIPlayer extends Player {

    // ==========================================================================

    @Override
    public void removeYou() {
        setGame(null);
        setCurrentGame(null);
        setWonGame(null);
        withoutTokens(this.getTokens().toArray(new Token[this.getTokens().size()]));
        setPrevious(null);
        setNext(null);
        // setMove(null);
        firePropertyChange("REMOVE_YOU", this, null);
    }

    // ==========================================================================

    public static final String PROPERTY_STRENGTH = "strength";

    private int strength;

    public int getStrength() {
        return this.strength;
    }

    public void setStrength(int value) {
        if(this.strength != value) {

            int oldValue = this.strength;
            this.strength = value;
            this.firePropertyChange(PROPERTY_STRENGTH, oldValue, value);
        }
    }

    public AIPlayer withStrength(int value) {
        setStrength(value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append(" ").append(this.getStrength());
        result.append(" ").append(this.getName());
        result.append(" ").append(this.getSymbole());
        result.append(" ").append(this.getScore());
        return result.substring(1);
    }
}
