/*
 * Copyright (c) 2017 Ich Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.uniks.tictactoe.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import de.uniks.networkparser.interfaces.SendableEntity;

/**
 * 
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/tictactoe/ModelGenerator.java'>ModelGenerator.java</a>
 */
public class Field implements SendableEntity {

    // ==========================================================================

    protected PropertyChangeSupport listeners = null;

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if(listeners != null) {
            listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if(listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(listener);
        return true;
    }

    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if(listeners == null) {
            listeners.removePropertyChangeListener(listener);
        }
        listeners.removePropertyChangeListener(listener);
        return true;
    }

    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(listeners != null) {
            listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    // ==========================================================================

    public void removeYou() {
        setGame(null);
        setBoard(null);
        setToken(null);
        firePropertyChange("REMOVE_YOU", this, null);
    }

    // ==========================================================================

    public static final String PROPERTY_X = "x";

    private int x;

    public int getX() {
        return this.x;
    }

    public void setX(int value) {
        if(this.x != value) {

            int oldValue = this.x;
            this.x = value;
            this.firePropertyChange(PROPERTY_X, oldValue, value);
        }
    }

    public Field withX(int value) {
        setX(value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append(" ").append(this.getX());
        result.append(" ").append(this.getY());
        return result.substring(1);
    }

    // ==========================================================================

    public static final String PROPERTY_Y = "y";

    private int y;

    public int getY() {
        return this.y;
    }

    public void setY(int value) {
        if(this.y != value) {

            int oldValue = this.y;
            this.y = value;
            this.firePropertyChange(PROPERTY_Y, oldValue, value);
        }
    }

    public Field withY(int value) {
        setY(value);
        return this;
    }

    /********************************************************************
     * <pre>
     *              many                       one
     * Field ----------------------------------- Game
     *              fields                   game
     * </pre>
     */

    public static final String PROPERTY_GAME = "game";

    private Game game = null;

    public Game getGame() {
        return this.game;
    }

    public boolean setGame(Game value) {
        boolean changed = false;

        if(this.game != value) {
            Game oldValue = this.game;

            if(this.game != null) {
                this.game = null;
                oldValue.withoutFields(this);
            }

            this.game = value;

            if(value != null) {
                value.withFields(this);
            }

            firePropertyChange(PROPERTY_GAME, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Field withGame(Game value) {
        setGame(value);
        return this;
    }

    public Game createGame() {
        Game value = new Game();
        withGame(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              many                       one
     * Field ----------------------------------- Board
     *              fields                   board
     * </pre>
     */

    public static final String PROPERTY_BOARD = "board";

    private Board board = null;

    public Board getBoard() {
        return this.board;
    }

    public boolean setBoard(Board value) {
        boolean changed = false;

        if(this.board != value) {
            Board oldValue = this.board;

            if(this.board != null) {
                this.board = null;
                oldValue.withoutFields(this);
            }

            this.board = value;

            if(value != null) {
                value.withFields(this);
            }

            firePropertyChange(PROPERTY_BOARD, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Field withBoard(Board value) {
        setBoard(value);
        return this;
    }

    public Board createBoard() {
        Board value = new Board();
        withBoard(value);
        return value;
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Field ----------------------------------- Token
     *              field                   token
     * </pre>
     */

    public static final String PROPERTY_TOKEN = "token";

    private Token token = null;

    public Token getToken() {
        return this.token;
    }

    public boolean setToken(Token value) {
        boolean changed = false;

        if(this.token != value) {
            Token oldValue = this.token;

            if(this.token != null) {
                this.token = null;
                oldValue.setField(null);
            }

            this.token = value;

            if(value != null) {
                value.withField(this);
            }

            firePropertyChange(PROPERTY_TOKEN, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Field withToken(Token value) {
        setToken(value);
        return this;
    }

    public Token createToken() {
        Token value = new Token();
        withToken(value);
        return value;
    }

}
