/*
 * Copyright (c) 2017 Ich Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.uniks.tictactoe.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;

import de.uniks.ai.LogicInterface;
import de.uniks.networkparser.interfaces.SendableEntity;
import de.uniks.tictactoe.model.util.FieldSet;

/**
 * 
 * @see <a href=
 * '../../../../../../../src/test/java/de/uniks/modelgenerator/tictactoe/ModelGenerator.java'>ModelGenerator.java</a>
 */
public class Logic implements SendableEntity, LogicInterface<Board, Move, Player> {

    // ==========================================================================

    protected PropertyChangeSupport listeners = null;

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if(listeners != null) {
            listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if(listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(listener);
        return true;
    }

    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if(listeners == null) {
            listeners.removePropertyChangeListener(listener);
        }
        listeners.removePropertyChangeListener(listener);
        return true;
    }

    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if(listeners != null) {
            listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    // ==========================================================================

    public void removeYou() {
        setGame(null);
        firePropertyChange("REMOVE_YOU", this, null);
    }

    /********************************************************************
     * <pre>
     *              one                       one
     * Logic ----------------------------------- Game
     *              logic                   game
     * </pre>
     */

    public static final String PROPERTY_GAME = "game";

    private Game game = null;

    public Game getGame() {
        return this.game;
    }

    public boolean setGame(Game value) {
        boolean changed = false;

        if(this.game != value) {
            Game oldValue = this.game;

            if(this.game != null) {
                this.game = null;
                oldValue.setLogic(null);
            }

            this.game = value;

            if(value != null) {
                value.withLogic(this);
            }

            firePropertyChange(PROPERTY_GAME, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Logic withGame(Game value) {
        setGame(value);
        return this;
    }

    public Game createGame() {
        Game value = new Game();
        withGame(value);
        return value;
    }

    /**
     * 
     */
    public Player checkWinner(Board board) {

        FieldSet fieldSet = board.getFields();

        // Check, if one player won the game by completing a horizontal line
        for(int i = 0; i < 3; i++) {
            if(findField(fieldSet, i, 0).getToken() != null && findField(fieldSet, i, 1).getToken() != null
                    && findField(fieldSet, i, 2).getToken() != null) {
                if(findField(fieldSet, i, 0).getToken().getPlayer() == findField(fieldSet, i, 1).getToken().getPlayer()
                        && findField(fieldSet, i, 0).getToken().getPlayer() == findField(fieldSet, i, 2).getToken()
                                .getPlayer()) {
                    return findField(fieldSet, i, 0).getToken().getPlayer();
                }
            }
        }

        // Check, if one player won the game by completing a vertical line
        for(int i = 0; i < 3; i++) {
            if(findField(fieldSet, 0, i).getToken() != null && findField(fieldSet, 1, i).getToken() != null
                    && findField(fieldSet, 2, i).getToken() != null) {
                if(findField(fieldSet, 0, i).getToken().getPlayer() == findField(fieldSet, 1, i).getToken().getPlayer()
                        && findField(fieldSet, 0, i).getToken().getPlayer() == findField(fieldSet, 2, i).getToken()
                                .getPlayer()) {
                    return findField(fieldSet, 0, i).getToken().getPlayer();
                }
            }
        }

        // Check, if one player won the game by completing the diagonally line ((0,0), (1,1), (2,2))
        if(findField(fieldSet, 0, 0).getToken() != null && findField(fieldSet, 1, 1).getToken() != null
                && findField(fieldSet, 2, 2).getToken() != null) {
            if(findField(fieldSet, 0, 0).getToken().getPlayer() == findField(fieldSet, 1, 1).getToken().getPlayer()
                    && findField(fieldSet, 0, 0).getToken().getPlayer() == findField(fieldSet, 2, 2).getToken()
                            .getPlayer()) {
                return findField(fieldSet, 0, 0).getToken().getPlayer();
            }
        }

        // Check, if one player won the game by completing the diagonally line ((2,0), (1,1), (0,2))
        if(findField(fieldSet, 2, 0).getToken() != null && findField(fieldSet, 1, 1).getToken() != null
                && findField(fieldSet, 0, 2).getToken() != null) {
            if(findField(fieldSet, 2, 0).getToken().getPlayer() == findField(fieldSet, 1, 1).getToken().getPlayer()
                    && findField(fieldSet, 2, 0).getToken().getPlayer() == findField(fieldSet, 0, 2).getToken()
                            .getPlayer()) {
                return findField(fieldSet, 2, 0).getToken().getPlayer();
            }
        }

        return null;
    }

    // The findField method searches the fieldSet for a field with the coordinates x,y.
    private Field findField(FieldSet fieldSet, int x, int y) {
        for(Field field: fieldSet) {
            if(field.getX() == x && field.getY() == y) {
                return field;
            }
        }
        return null;
    }

    /**
     * The getAllMoves method creates an ArrayList, that contains all possible moves a player can do as his/her next
     * move.
     * 
     * @param player
     * @param board
     */
    public ArrayList<Move> getAllMoves(Player player, Board board) {

        Player winner = checkWinner(board);

        ArrayList<Move> moveList = new ArrayList<Move>();

        if(winner != null) {
            return moveList;
        }

        for(Field field: board.getFields()) {
            if(field.getToken() == null) {
                Move move = new Move().withField(field).withPlayer(player);
                moveList.add(move);
            }
        }

        return moveList;
    }

    /**
     * Creates a token for a player and puts it on a specific field.
     * 
     * @param Move is an object of the Tic Tac Toe Move class.
     */
    public void executeMove(Move move) {

        for(Field field: move.getField().getBoard().getFields()) {
            if(field.getX() == move.getField().getX() && field.getY() == move.getField().getY()) {
                Token token = new Token().withPlayer(move.getPlayer()).withGame(game);
                field.withToken(token);
            }
        }

        return;
    }

    /**
     * 
     * @param move
     */
    public void undoMove(Move move) {

        for(Field field: move.getField().getBoard().getFields()) {
            if(field.getX() == move.getField().getX() && field.getY() == move.getField().getY()) {
                if(field.getToken() == null) {
                    return;
                }
            }
        }

        // True, if the move was made by the player.
        if(move.getField().getToken().getPlayer() == move.getPlayer()) {
            for(Field field: move.getField().getBoard().getFields()) {
                if(field.getX() == move.getField().getX() && field.getY() == move.getField().getY()) {
                    Token token = field.getToken();
                    token.removeYou();
                }
            }
            return;
            // Returns null, if it wasn't one of the players move
        } else {
            return;
        }
    }

    /**
     * Evaluates the board from the players point of view.
     * 
     * @param board
     * @param player
     * @return -10000, if a player has won the game. 0, if there is no winner.
     */
    @Override
    public int evaluate(Board board, Player player) {

        Player winner = checkWinner(board);

        if(winner != null) {
            return -10000;
        }
        return 0;
    }

    /**
     * 
     */
    @Override
    public void executeMoveSimulation(Move move) {
        executeMove(move);
    }

    /**
     * 
     */
    @Override
    public void undoMoveSimulation(Move move) {
        undoMove(move);
    }
}
