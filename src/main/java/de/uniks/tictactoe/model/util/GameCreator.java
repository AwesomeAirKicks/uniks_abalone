/*
 * Copyright (c) 2017 Ich Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.uniks.tictactoe.model.util;

import de.uniks.networkparser.IdMap;
import de.uniks.networkparser.interfaces.SendableEntityCreator;
import de.uniks.tictactoe.model.Board;
import de.uniks.tictactoe.model.Field;
import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Logic;
import de.uniks.tictactoe.model.Player;
import de.uniks.tictactoe.model.Token;

public class GameCreator implements SendableEntityCreator {
    private final String[] properties = new String[] {
            Game.PROPERTY_BOARD,
            Game.PROPERTY_FIELDS,
            Game.PROPERTY_TOKENS,
            Game.PROPERTY_PLAYERS,
            Game.PROPERTY_CURRENTPLAYER,
            Game.PROPERTY_WINNER,
            Game.PROPERTY_LOGIC,
    };

    @Override
    public String[] getProperties() {
        return properties;
    }

    @Override
    public Object getSendableInstance(boolean reference) {
        return new Game();
    }

    @Override
    public Object getValue(Object target, String attrName) {
        int pos = attrName.indexOf('.');
        String attribute = attrName;

        if(pos > 0) {
            attribute = attrName.substring(0, pos);
        }

        if(Game.PROPERTY_BOARD.equalsIgnoreCase(attribute)) {
            return ((Game) target).getBoard();
        }

        if(Game.PROPERTY_FIELDS.equalsIgnoreCase(attribute)) {
            return ((Game) target).getFields();
        }

        if(Game.PROPERTY_TOKENS.equalsIgnoreCase(attribute)) {
            return ((Game) target).getTokens();
        }

        if(Game.PROPERTY_PLAYERS.equalsIgnoreCase(attribute)) {
            return ((Game) target).getPlayers();
        }

        if(Game.PROPERTY_CURRENTPLAYER.equalsIgnoreCase(attribute)) {
            return ((Game) target).getCurrentPlayer();
        }

        if(Game.PROPERTY_WINNER.equalsIgnoreCase(attribute)) {
            return ((Game) target).getWinner();
        }

        if(Game.PROPERTY_LOGIC.equalsIgnoreCase(attribute)) {
            return ((Game) target).getLogic();
        }

        return null;
    }

    @Override
    public boolean setValue(Object target, String attrName, Object value, String type) {
        if(SendableEntityCreator.REMOVE.equals(type) && value != null) {
            attrName = attrName + type;
        }

        if(Game.PROPERTY_BOARD.equalsIgnoreCase(attrName)) {
            ((Game) target).setBoard((Board) value);
            return true;
        }

        if(Game.PROPERTY_FIELDS.equalsIgnoreCase(attrName)) {
            ((Game) target).withFields((Field) value);
            return true;
        }

        if((Game.PROPERTY_FIELDS + SendableEntityCreator.REMOVE).equalsIgnoreCase(attrName)) {
            ((Game) target).withoutFields((Field) value);
            return true;
        }

        if(Game.PROPERTY_TOKENS.equalsIgnoreCase(attrName)) {
            ((Game) target).withTokens((Token) value);
            return true;
        }

        if((Game.PROPERTY_TOKENS + SendableEntityCreator.REMOVE).equalsIgnoreCase(attrName)) {
            ((Game) target).withoutTokens((Token) value);
            return true;
        }

        if(Game.PROPERTY_PLAYERS.equalsIgnoreCase(attrName)) {
            ((Game) target).withPlayers((Player) value);
            return true;
        }

        if((Game.PROPERTY_PLAYERS + SendableEntityCreator.REMOVE).equalsIgnoreCase(attrName)) {
            ((Game) target).withoutPlayers((Player) value);
            return true;
        }

        if(Game.PROPERTY_CURRENTPLAYER.equalsIgnoreCase(attrName)) {
            ((Game) target).setCurrentPlayer((Player) value);
            return true;
        }

        if(Game.PROPERTY_WINNER.equalsIgnoreCase(attrName)) {
            ((Game) target).setWinner((Player) value);
            return true;
        }

        if(Game.PROPERTY_LOGIC.equalsIgnoreCase(attrName)) {
            ((Game) target).setLogic((Logic) value);
            return true;
        }

        return false;
    }

    public static IdMap createIdMap(String sessionID) {
        return de.uniks.tictactoe.model.util.CreatorCreator.createIdMap(sessionID);
    }

    // ==========================================================================
    public void removeObject(Object entity) {
        ((Game) entity).removeYou();
    }
}
