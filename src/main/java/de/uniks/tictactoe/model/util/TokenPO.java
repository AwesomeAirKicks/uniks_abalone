package de.uniks.tictactoe.model.util;

import org.sdmlib.models.pattern.PatternObject;

import de.uniks.tictactoe.model.Field;
import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Player;
import de.uniks.tictactoe.model.Token;

public class TokenPO extends PatternObject<TokenPO, Token> {

    public TokenSet allMatches() {
        this.setDoAllMatches(true);

        TokenSet matches = new TokenSet();

        while(this.getPattern().getHasMatch()) {
            matches.add((Token) this.getCurrentMatch());

            this.getPattern().findMatch();
        }

        return matches;
    }

    public TokenPO() {
        newInstance(null);
    }

    public TokenPO(Token... hostGraphObject) {
        if(hostGraphObject == null || hostGraphObject.length < 1) {
            return;
        }
        newInstance(null, hostGraphObject);
    }

    public TokenPO(String modifier) {
        this.setModifier(modifier);
    }

    public GamePO createGamePO() {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Token.PROPERTY_GAME, result);

        return result;
    }

    public GamePO createGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(modifier);
        super.hasLink(Token.PROPERTY_GAME, result);

        return result;
    }

    public TokenPO createGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, Token.PROPERTY_GAME);
    }

    public TokenPO createGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, Token.PROPERTY_GAME, modifier);
    }

    public Game getGame() {
        if(this.getPattern().getHasMatch()) {
            return ((Token) this.getCurrentMatch()).getGame();
        }
        return null;
    }

    public FieldPO createFieldPO() {
        FieldPO result = new FieldPO(new Field[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Token.PROPERTY_FIELD, result);

        return result;
    }

    public FieldPO createFieldPO(String modifier) {
        FieldPO result = new FieldPO(new Field[] {});

        result.setModifier(modifier);
        super.hasLink(Token.PROPERTY_FIELD, result);

        return result;
    }

    public TokenPO createFieldLink(FieldPO tgt) {
        return hasLinkConstraint(tgt, Token.PROPERTY_FIELD);
    }

    public TokenPO createFieldLink(FieldPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Token.PROPERTY_FIELD, modifier);
    }

    public Field getField() {
        if(this.getPattern().getHasMatch()) {
            return ((Token) this.getCurrentMatch()).getField();
        }
        return null;
    }

    public PlayerPO createPlayerPO() {
        PlayerPO result = new PlayerPO(new Player[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Token.PROPERTY_PLAYER, result);

        return result;
    }

    public PlayerPO createPlayerPO(String modifier) {
        PlayerPO result = new PlayerPO(new Player[] {});

        result.setModifier(modifier);
        super.hasLink(Token.PROPERTY_PLAYER, result);

        return result;
    }

    public TokenPO createPlayerLink(PlayerPO tgt) {
        return hasLinkConstraint(tgt, Token.PROPERTY_PLAYER);
    }

    public TokenPO createPlayerLink(PlayerPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Token.PROPERTY_PLAYER, modifier);
    }

    public Player getPlayer() {
        if(this.getPattern().getHasMatch()) {
            return ((Token) this.getCurrentMatch()).getPlayer();
        }
        return null;
    }

}
