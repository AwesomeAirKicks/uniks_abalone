/*
 * Copyright (c) 2017 Ich Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.uniks.tictactoe.model.util;

import java.util.Collection;
import java.util.Collections;

import de.uniks.networkparser.interfaces.Condition;
import de.uniks.networkparser.list.ObjectSet;
import de.uniks.networkparser.list.SimpleSet;
import de.uniks.tictactoe.model.Board;
import de.uniks.tictactoe.model.Field;
import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Logic;
import de.uniks.tictactoe.model.Player;
import de.uniks.tictactoe.model.Token;

public class GameSet extends SimpleSet<Game> {
    @Override
    protected Class<?> getTypClass() {
        return Game.class;
    }

    public GameSet() {
        // empty
    }

    public GameSet(Game... objects) {
        for(Game obj: objects) {
            this.add(obj);
        }
    }

    public GameSet(Collection<Game> objects) {
        this.addAll(objects);
    }

    public static final GameSet EMPTY_SET = new GameSet().withFlag(GameSet.READONLY);

    public GamePO createGamePO() {
        return new GamePO(this.toArray(new Game[this.size()]));
    }

    public String getEntryType() {
        return "de.uniks.tictactoe.model.Game";
    }

    @Override
    public GameSet getNewList(boolean keyValue) {
        return new GameSet();
    }

    @Override
    public GameSet filter(Condition<Game> condition) {
        GameSet filterList = new GameSet();
        filterItems(filterList, condition);
        return filterList;
    }

    @SuppressWarnings("unchecked")
    public GameSet with(Object value) {
        if(value == null) {
            return this;
        } else if(value instanceof java.util.Collection) {
            this.addAll((Collection<Game>) value);
        } else if(value != null) {
            this.add((Game) value);
        }

        return this;
    }

    public GameSet without(Game value) {
        this.remove(value);
        return this;
    }

    /**
     * Loop through the current set of Game objects and collect a set of the Board objects reached via board.
     * 
     * @return Set of Board objects reachable via board
     */
    public BoardSet getBoard() {
        BoardSet result = new BoardSet();

        for(Game obj: this) {
            result.with(obj.getBoard());
        }

        return result;
    }

    /**
     * Loop through the current set of Game objects and collect all contained objects with reference board pointing to
     * the object passed as parameter.
     * 
     * @param value The object required as board neighbor of the collected results.
     * 
     * @return Set of Board objects referring to value via board
     */
    public GameSet filterBoard(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        GameSet answer = new GameSet();

        for(Game obj: this) {
            if(neighbors.contains(obj.getBoard()) || (neighbors.isEmpty() && obj.getBoard() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Game object passed as parameter to the Board
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their Board attributes.
     */
    public GameSet withBoard(Board value) {
        for(Game obj: this) {
            obj.withBoard(value);
        }

        return this;
    }

    /**
     * Loop through the current set of Game objects and collect a set of the Field objects reached via fields.
     * 
     * @return Set of Field objects reachable via fields
     */
    public FieldSet getFields() {
        FieldSet result = new FieldSet();

        for(Game obj: this) {
            result.with(obj.getFields());
        }

        return result;
    }

    /**
     * Loop through the current set of Game objects and collect all contained objects with reference fields pointing to
     * the object passed as parameter.
     * 
     * @param value The object required as fields neighbor of the collected results.
     * 
     * @return Set of Field objects referring to value via fields
     */
    public GameSet filterFields(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        GameSet answer = new GameSet();

        for(Game obj: this) {
            if(!Collections.disjoint(neighbors, obj.getFields())) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Game object passed as parameter to the Fields
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their Fields attributes.
     */
    public GameSet withFields(Field value) {
        for(Game obj: this) {
            obj.withFields(value);
        }

        return this;
    }

    /**
     * Loop through current set of ModelType objects and remove the Game object passed as parameter from the Fields
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now without the old neighbor.
     */
    public GameSet withoutFields(Field value) {
        for(Game obj: this) {
            obj.withoutFields(value);
        }

        return this;
    }

    /**
     * Loop through the current set of Game objects and collect a set of the Token objects reached via tokens.
     * 
     * @return Set of Token objects reachable via tokens
     */
    public TokenSet getTokens() {
        TokenSet result = new TokenSet();

        for(Game obj: this) {
            result.with(obj.getTokens());
        }

        return result;
    }

    /**
     * Loop through the current set of Game objects and collect all contained objects with reference tokens pointing to
     * the object passed as parameter.
     * 
     * @param value The object required as tokens neighbor of the collected results.
     * 
     * @return Set of Token objects referring to value via tokens
     */
    public GameSet filterTokens(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        GameSet answer = new GameSet();

        for(Game obj: this) {
            if(!Collections.disjoint(neighbors, obj.getTokens())) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Game object passed as parameter to the Tokens
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their Tokens attributes.
     */
    public GameSet withTokens(Token value) {
        for(Game obj: this) {
            obj.withTokens(value);
        }

        return this;
    }

    /**
     * Loop through current set of ModelType objects and remove the Game object passed as parameter from the Tokens
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now without the old neighbor.
     */
    public GameSet withoutTokens(Token value) {
        for(Game obj: this) {
            obj.withoutTokens(value);
        }

        return this;
    }

    /**
     * Loop through the current set of Game objects and collect a set of the Player objects reached via players.
     * 
     * @return Set of Player objects reachable via players
     */
    public PlayerSet getPlayers() {
        PlayerSet result = new PlayerSet();

        for(Game obj: this) {
            result.with(obj.getPlayers());
        }

        return result;
    }

    /**
     * Loop through the current set of Game objects and collect all contained objects with reference players pointing to
     * the object passed as parameter.
     * 
     * @param value The object required as players neighbor of the collected results.
     * 
     * @return Set of Player objects referring to value via players
     */
    public GameSet filterPlayers(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        GameSet answer = new GameSet();

        for(Game obj: this) {
            if(!Collections.disjoint(neighbors, obj.getPlayers())) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Game object passed as parameter to the Players
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their Players attributes.
     */
    public GameSet withPlayers(Player value) {
        for(Game obj: this) {
            obj.withPlayers(value);
        }

        return this;
    }

    /**
     * Loop through current set of ModelType objects and remove the Game object passed as parameter from the Players
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now without the old neighbor.
     */
    public GameSet withoutPlayers(Player value) {
        for(Game obj: this) {
            obj.withoutPlayers(value);
        }

        return this;
    }

    /**
     * Loop through the current set of Game objects and collect a set of the Player objects reached via currentPlayer.
     * 
     * @return Set of Player objects reachable via currentPlayer
     */
    public PlayerSet getCurrentPlayer() {
        PlayerSet result = new PlayerSet();

        for(Game obj: this) {
            result.with(obj.getCurrentPlayer());
        }

        return result;
    }

    /**
     * Loop through the current set of Game objects and collect all contained objects with reference currentPlayer
     * pointing to the object passed as parameter.
     * 
     * @param value The object required as currentPlayer neighbor of the collected results.
     * 
     * @return Set of Player objects referring to value via currentPlayer
     */
    public GameSet filterCurrentPlayer(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        GameSet answer = new GameSet();

        for(Game obj: this) {
            if(neighbors.contains(obj.getCurrentPlayer()) || (neighbors.isEmpty() && obj.getCurrentPlayer() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Game object passed as parameter to the CurrentPlayer
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their CurrentPlayer
     * attributes.
     */
    public GameSet withCurrentPlayer(Player value) {
        for(Game obj: this) {
            obj.withCurrentPlayer(value);
        }

        return this;
    }

    /**
     * Loop through the current set of Game objects and collect a set of the Player objects reached via winner.
     * 
     * @return Set of Player objects reachable via winner
     */
    public PlayerSet getWinner() {
        PlayerSet result = new PlayerSet();

        for(Game obj: this) {
            result.with(obj.getWinner());
        }

        return result;
    }

    /**
     * Loop through the current set of Game objects and collect all contained objects with reference winner pointing to
     * the object passed as parameter.
     * 
     * @param value The object required as winner neighbor of the collected results.
     * 
     * @return Set of Player objects referring to value via winner
     */
    public GameSet filterWinner(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        GameSet answer = new GameSet();

        for(Game obj: this) {
            if(neighbors.contains(obj.getWinner()) || (neighbors.isEmpty() && obj.getWinner() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Game object passed as parameter to the Winner
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their Winner attributes.
     */
    public GameSet withWinner(Player value) {
        for(Game obj: this) {
            obj.withWinner(value);
        }

        return this;
    }

    /**
     * Loop through the current set of Game objects and collect a set of the Logic objects reached via logic.
     * 
     * @return Set of Logic objects reachable via logic
     */
    public LogicSet getLogic() {
        LogicSet result = new LogicSet();

        for(Game obj: this) {
            result.with(obj.getLogic());
        }

        return result;
    }

    /**
     * Loop through the current set of Game objects and collect all contained objects with reference logic pointing to
     * the object passed as parameter.
     * 
     * @param value The object required as logic neighbor of the collected results.
     * 
     * @return Set of Logic objects referring to value via logic
     */
    public GameSet filterLogic(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        GameSet answer = new GameSet();

        for(Game obj: this) {
            if(neighbors.contains(obj.getLogic()) || (neighbors.isEmpty() && obj.getLogic() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Game object passed as parameter to the Logic
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their Logic attributes.
     */
    public GameSet withLogic(Logic value) {
        for(Game obj: this) {
            obj.withLogic(value);
        }

        return this;
    }

}
