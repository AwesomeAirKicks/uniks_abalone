package de.uniks.tictactoe.model.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;

import de.uniks.networkparser.IdMap;
import de.uniks.tictactoe.model.AIPlayer;

public class AIPlayerPOCreator extends PatternObjectCreator {
    @Override
    public Object getSendableInstance(boolean reference) {
        if(reference) {
            return new AIPlayerPO(new AIPlayer[] {});
        } else {
            return new AIPlayerPO();
        }
    }

    public static IdMap createIdMap(String sessionID) {
        return de.uniks.tictactoe.model.util.CreatorCreator.createIdMap(sessionID);
    }
}
