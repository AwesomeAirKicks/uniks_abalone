package de.uniks.tictactoe.model.util;

import org.sdmlib.models.pattern.PatternObject;

import de.uniks.tictactoe.model.Field;
import de.uniks.tictactoe.model.Move;
import de.uniks.tictactoe.model.Player;

public class MovePO extends PatternObject<MovePO, Move> {

    public MoveSet allMatches() {
        this.setDoAllMatches(true);

        MoveSet matches = new MoveSet();

        while(this.getPattern().getHasMatch()) {
            matches.add((Move) this.getCurrentMatch());

            this.getPattern().findMatch();
        }

        return matches;
    }

    public MovePO() {
        newInstance(null);
    }

    public MovePO(Move... hostGraphObject) {
        if(hostGraphObject == null || hostGraphObject.length < 1) {
            return;
        }
        newInstance(null, hostGraphObject);
    }

    public MovePO(String modifier) {
        this.setModifier(modifier);
    }

    public FieldPO createFieldPO() {
        FieldPO result = new FieldPO(new Field[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Move.PROPERTY_FIELD, result);

        return result;
    }

    public FieldPO createFieldPO(String modifier) {
        FieldPO result = new FieldPO(new Field[] {});

        result.setModifier(modifier);
        super.hasLink(Move.PROPERTY_FIELD, result);

        return result;
    }

    public MovePO createFieldLink(FieldPO tgt) {
        return hasLinkConstraint(tgt, Move.PROPERTY_FIELD);
    }

    public MovePO createFieldLink(FieldPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Move.PROPERTY_FIELD, modifier);
    }

    public Field getField() {
        if(this.getPattern().getHasMatch()) {
            return ((Move) this.getCurrentMatch()).getField();
        }
        return null;
    }

    public PlayerPO createPlayerPO() {
        PlayerPO result = new PlayerPO(new Player[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Move.PROPERTY_PLAYER, result);

        return result;
    }

    public PlayerPO createPlayerPO(String modifier) {
        PlayerPO result = new PlayerPO(new Player[] {});

        result.setModifier(modifier);
        super.hasLink(Move.PROPERTY_PLAYER, result);

        return result;
    }

    public MovePO createPlayerLink(PlayerPO tgt) {
        return hasLinkConstraint(tgt, Move.PROPERTY_PLAYER);
    }

    public MovePO createPlayerLink(PlayerPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Move.PROPERTY_PLAYER, modifier);
    }

    public Player getPlayer() {
        if(this.getPattern().getHasMatch()) {
            return ((Move) this.getCurrentMatch()).getPlayer();
        }
        return null;
    }

}
