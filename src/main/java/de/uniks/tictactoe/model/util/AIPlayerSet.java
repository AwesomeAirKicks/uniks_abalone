/*
 * Copyright (c) 2017 Ich Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.uniks.tictactoe.model.util;

import java.util.Collection;
import java.util.Collections;

import de.uniks.networkparser.interfaces.Condition;
import de.uniks.networkparser.list.NumberList;
import de.uniks.networkparser.list.ObjectSet;
import de.uniks.networkparser.list.SimpleSet;
import de.uniks.tictactoe.model.AIPlayer;
import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Player;
import de.uniks.tictactoe.model.Token;

public class AIPlayerSet extends SimpleSet<AIPlayer> {
    @Override
    protected Class<?> getTypClass() {
        return AIPlayer.class;
    }

    public AIPlayerSet() {
        // empty
    }

    public AIPlayerSet(AIPlayer... objects) {
        for(AIPlayer obj: objects) {
            this.add(obj);
        }
    }

    public AIPlayerSet(Collection<AIPlayer> objects) {
        this.addAll(objects);
    }

    public static final AIPlayerSet EMPTY_SET = new AIPlayerSet().withFlag(AIPlayerSet.READONLY);

    public AIPlayerPO createAIPlayerPO() {
        return new AIPlayerPO(this.toArray(new AIPlayer[this.size()]));
    }

    public String getEntryType() {
        return "de.uniks.tictactoe.model.AIPlayer";
    }

    @Override
    public AIPlayerSet getNewList(boolean keyValue) {
        return new AIPlayerSet();
    }

    @Override
    public AIPlayerSet filter(Condition<AIPlayer> condition) {
        AIPlayerSet filterList = new AIPlayerSet();
        filterItems(filterList, condition);
        return filterList;
    }

    @SuppressWarnings("unchecked")
    public AIPlayerSet with(Object value) {
        if(value == null) {
            return this;
        } else if(value instanceof java.util.Collection) {
            this.addAll((Collection<AIPlayer>) value);
        } else if(value != null) {
            this.add((AIPlayer) value);
        }

        return this;
    }

    public AIPlayerSet without(AIPlayer value) {
        this.remove(value);
        return this;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect a list of the strength attribute values.
     * 
     * @return List of int objects reachable via strength attribute
     */
    public NumberList getStrength() {
        NumberList result = new NumberList();

        for(AIPlayer obj: this) {
            result.add(obj.getStrength());
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect those AIPlayer objects where the strength attribute
     * matches the parameter value.
     * 
     * @param value Search value
     * 
     * @return Subset of AIPlayer objects that match the parameter
     */
    public AIPlayerSet filterStrength(int value) {
        AIPlayerSet result = new AIPlayerSet();

        for(AIPlayer obj: this) {
            if(value == obj.getStrength()) {
                result.add(obj);
            }
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect those AIPlayer objects where the strength attribute
     * is between lower and upper.
     * 
     * @param lower Lower bound
     * @param upper Upper bound
     * 
     * @return Subset of AIPlayer objects that match the parameter
     */
    public AIPlayerSet filterStrength(int lower, int upper) {
        AIPlayerSet result = new AIPlayerSet();

        for(AIPlayer obj: this) {
            if(lower <= obj.getStrength() && obj.getStrength() <= upper) {
                result.add(obj);
            }
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and assign value to the strength attribute of each of it.
     * 
     * @param value New attribute value
     * 
     * @return Current set of AIPlayer objects now with new attribute values.
     */
    public AIPlayerSet withStrength(int value) {
        for(AIPlayer obj: this) {
            obj.setStrength(value);
        }

        return this;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect a list of the name attribute values.
     * 
     * @return List of String objects reachable via name attribute
     */
    public ObjectSet getName() {
        ObjectSet result = new ObjectSet();

        for(AIPlayer obj: this) {
            result.add(obj.getName());
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect those AIPlayer objects where the name attribute
     * matches the parameter value.
     * 
     * @param value Search value
     * 
     * @return Subset of AIPlayer objects that match the parameter
     */
    public AIPlayerSet filterName(String value) {
        AIPlayerSet result = new AIPlayerSet();

        for(AIPlayer obj: this) {
            if(value.equals(obj.getName())) {
                result.add(obj);
            }
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect those AIPlayer objects where the name attribute is
     * between lower and upper.
     * 
     * @param lower Lower bound
     * @param upper Upper bound
     * 
     * @return Subset of AIPlayer objects that match the parameter
     */
    public AIPlayerSet filterName(String lower, String upper) {
        AIPlayerSet result = new AIPlayerSet();

        for(AIPlayer obj: this) {
            if(lower.compareTo(obj.getName()) <= 0 && obj.getName().compareTo(upper) <= 0) {
                result.add(obj);
            }
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and assign value to the name attribute of each of it.
     * 
     * @param value New attribute value
     * 
     * @return Current set of AIPlayer objects now with new attribute values.
     */
    public AIPlayerSet withName(String value) {
        for(AIPlayer obj: this) {
            obj.setName(value);
        }

        return this;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect a list of the symbole attribute values.
     * 
     * @return List of String objects reachable via symbole attribute
     */
    public ObjectSet getSymbole() {
        ObjectSet result = new ObjectSet();

        for(AIPlayer obj: this) {
            result.add(obj.getSymbole());
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect those AIPlayer objects where the symbole attribute
     * matches the parameter value.
     * 
     * @param value Search value
     * 
     * @return Subset of AIPlayer objects that match the parameter
     */
    public AIPlayerSet filterSymbole(String value) {
        AIPlayerSet result = new AIPlayerSet();

        for(AIPlayer obj: this) {
            if(value.equals(obj.getSymbole())) {
                result.add(obj);
            }
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect those AIPlayer objects where the symbole attribute
     * is between lower and upper.
     * 
     * @param lower Lower bound
     * @param upper Upper bound
     * 
     * @return Subset of AIPlayer objects that match the parameter
     */
    public AIPlayerSet filterSymbole(String lower, String upper) {
        AIPlayerSet result = new AIPlayerSet();

        for(AIPlayer obj: this) {
            if(lower.compareTo(obj.getSymbole()) <= 0 && obj.getSymbole().compareTo(upper) <= 0) {
                result.add(obj);
            }
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and assign value to the symbole attribute of each of it.
     * 
     * @param value New attribute value
     * 
     * @return Current set of AIPlayer objects now with new attribute values.
     */
    public AIPlayerSet withSymbole(String value) {
        for(AIPlayer obj: this) {
            obj.setSymbole(value);
        }

        return this;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect a list of the score attribute values.
     * 
     * @return List of int objects reachable via score attribute
     */
    public NumberList getScore() {
        NumberList result = new NumberList();

        for(AIPlayer obj: this) {
            result.add(obj.getScore());
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect those AIPlayer objects where the score attribute
     * matches the parameter value.
     * 
     * @param value Search value
     * 
     * @return Subset of AIPlayer objects that match the parameter
     */
    public AIPlayerSet filterScore(int value) {
        AIPlayerSet result = new AIPlayerSet();

        for(AIPlayer obj: this) {
            if(value == obj.getScore()) {
                result.add(obj);
            }
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect those AIPlayer objects where the score attribute is
     * between lower and upper.
     * 
     * @param lower Lower bound
     * @param upper Upper bound
     * 
     * @return Subset of AIPlayer objects that match the parameter
     */
    public AIPlayerSet filterScore(int lower, int upper) {
        AIPlayerSet result = new AIPlayerSet();

        for(AIPlayer obj: this) {
            if(lower <= obj.getScore() && obj.getScore() <= upper) {
                result.add(obj);
            }
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and assign value to the score attribute of each of it.
     * 
     * @param value New attribute value
     * 
     * @return Current set of AIPlayer objects now with new attribute values.
     */
    public AIPlayerSet withScore(int value) {
        for(AIPlayer obj: this) {
            obj.setScore(value);
        }

        return this;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect a set of the Game objects reached via game.
     * 
     * @return Set of Game objects reachable via game
     */
    public GameSet getGame() {
        GameSet result = new GameSet();

        for(AIPlayer obj: this) {
            result.with(obj.getGame());
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect all contained objects with reference game pointing
     * to the object passed as parameter.
     * 
     * @param value The object required as game neighbor of the collected results.
     * 
     * @return Set of Game objects referring to value via game
     */
    public AIPlayerSet filterGame(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        AIPlayerSet answer = new AIPlayerSet();

        for(AIPlayer obj: this) {
            if(neighbors.contains(obj.getGame()) || (neighbors.isEmpty() && obj.getGame() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the AIPlayer object passed as parameter to the Game
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their Game attributes.
     */
    public AIPlayerSet withGame(Game value) {
        for(AIPlayer obj: this) {
            obj.withGame(value);
        }

        return this;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect a set of the Game objects reached via currentGame.
     * 
     * @return Set of Game objects reachable via currentGame
     */
    public GameSet getCurrentGame() {
        GameSet result = new GameSet();

        for(AIPlayer obj: this) {
            result.with(obj.getCurrentGame());
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect all contained objects with reference currentGame
     * pointing to the object passed as parameter.
     * 
     * @param value The object required as currentGame neighbor of the collected results.
     * 
     * @return Set of Game objects referring to value via currentGame
     */
    public AIPlayerSet filterCurrentGame(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        AIPlayerSet answer = new AIPlayerSet();

        for(AIPlayer obj: this) {
            if(neighbors.contains(obj.getCurrentGame()) || (neighbors.isEmpty() && obj.getCurrentGame() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the AIPlayer object passed as parameter to the
     * CurrentGame attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their CurrentGame attributes.
     */
    public AIPlayerSet withCurrentGame(Game value) {
        for(AIPlayer obj: this) {
            obj.withCurrentGame(value);
        }

        return this;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect a set of the Game objects reached via wonGame.
     * 
     * @return Set of Game objects reachable via wonGame
     */
    public GameSet getWonGame() {
        GameSet result = new GameSet();

        for(AIPlayer obj: this) {
            result.with(obj.getWonGame());
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect all contained objects with reference wonGame
     * pointing to the object passed as parameter.
     * 
     * @param value The object required as wonGame neighbor of the collected results.
     * 
     * @return Set of Game objects referring to value via wonGame
     */
    public AIPlayerSet filterWonGame(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        AIPlayerSet answer = new AIPlayerSet();

        for(AIPlayer obj: this) {
            if(neighbors.contains(obj.getWonGame()) || (neighbors.isEmpty() && obj.getWonGame() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the AIPlayer object passed as parameter to the WonGame
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their WonGame attributes.
     */
    public AIPlayerSet withWonGame(Game value) {
        for(AIPlayer obj: this) {
            obj.withWonGame(value);
        }

        return this;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect a set of the Token objects reached via tokens.
     * 
     * @return Set of Token objects reachable via tokens
     */
    public TokenSet getTokens() {
        TokenSet result = new TokenSet();

        for(AIPlayer obj: this) {
            result.with(obj.getTokens());
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect all contained objects with reference tokens pointing
     * to the object passed as parameter.
     * 
     * @param value The object required as tokens neighbor of the collected results.
     * 
     * @return Set of Token objects referring to value via tokens
     */
    public AIPlayerSet filterTokens(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        AIPlayerSet answer = new AIPlayerSet();

        for(AIPlayer obj: this) {
            if(!Collections.disjoint(neighbors, obj.getTokens())) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the AIPlayer object passed as parameter to the Tokens
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their Tokens attributes.
     */
    public AIPlayerSet withTokens(Token value) {
        for(AIPlayer obj: this) {
            obj.withTokens(value);
        }

        return this;
    }

    /**
     * Loop through current set of ModelType objects and remove the AIPlayer object passed as parameter from the Tokens
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now without the old neighbor.
     */
    public AIPlayerSet withoutTokens(Token value) {
        for(AIPlayer obj: this) {
            obj.withoutTokens(value);
        }

        return this;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect a set of the Player objects reached via previous.
     * 
     * @return Set of Player objects reachable via previous
     */
    public PlayerSet getPrevious() {
        PlayerSet result = new PlayerSet();

        for(AIPlayer obj: this) {
            result.with(obj.getPrevious());
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect all contained objects with reference previous
     * pointing to the object passed as parameter.
     * 
     * @param value The object required as previous neighbor of the collected results.
     * 
     * @return Set of Player objects referring to value via previous
     */
    public AIPlayerSet filterPrevious(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        AIPlayerSet answer = new AIPlayerSet();

        for(AIPlayer obj: this) {
            if(neighbors.contains(obj.getPrevious()) || (neighbors.isEmpty() && obj.getPrevious() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Follow previous reference zero or more times and collect all reachable objects. Detect cycles and deal with them.
     * 
     * @return Set of Player objects reachable via previous transitively (including the start set)
     */
    public PlayerSet getPreviousTransitive() {
        PlayerSet todo = new PlayerSet().with(this);

        PlayerSet result = new PlayerSet();

        while(!todo.isEmpty()) {
            Player current = todo.first();

            todo.remove(current);

            if(!result.contains(current)) {
                result.add(current);

                if(!result.contains(current.getPrevious())) {
                    todo.with(current.getPrevious());
                }
            }
        }

        return result;
    }

    /**
     * Loop through current set of ModelType objects and attach the AIPlayer object passed as parameter to the Previous
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their Previous attributes.
     */
    public AIPlayerSet withPrevious(Player value) {
        for(AIPlayer obj: this) {
            obj.withPrevious(value);
        }

        return this;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect a set of the Player objects reached via next.
     * 
     * @return Set of Player objects reachable via next
     */
    public PlayerSet getNext() {
        PlayerSet result = new PlayerSet();

        for(AIPlayer obj: this) {
            result.with(obj.getNext());
        }

        return result;
    }

    /**
     * Loop through the current set of AIPlayer objects and collect all contained objects with reference next pointing
     * to the object passed as parameter.
     * 
     * @param value The object required as next neighbor of the collected results.
     * 
     * @return Set of Player objects referring to value via next
     */
    public AIPlayerSet filterNext(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        AIPlayerSet answer = new AIPlayerSet();

        for(AIPlayer obj: this) {
            if(neighbors.contains(obj.getNext()) || (neighbors.isEmpty() && obj.getNext() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Follow next reference zero or more times and collect all reachable objects. Detect cycles and deal with them.
     * 
     * @return Set of Player objects reachable via next transitively (including the start set)
     */
    public PlayerSet getNextTransitive() {
        PlayerSet todo = new PlayerSet().with(this);

        PlayerSet result = new PlayerSet();

        while(!todo.isEmpty()) {
            Player current = todo.first();

            todo.remove(current);

            if(!result.contains(current)) {
                result.add(current);

                if(!result.contains(current.getNext())) {
                    todo.with(current.getNext());
                }
            }
        }

        return result;
    }

    /**
     * Loop through current set of ModelType objects and attach the AIPlayer object passed as parameter to the Next
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their Next attributes.
     */
    public AIPlayerSet withNext(Player value) {
        for(AIPlayer obj: this) {
            obj.withNext(value);
        }

        return this;
    }

}
