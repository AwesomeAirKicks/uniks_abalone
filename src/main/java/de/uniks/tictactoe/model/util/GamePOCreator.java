package de.uniks.tictactoe.model.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;

import de.uniks.networkparser.IdMap;
import de.uniks.tictactoe.model.Game;

public class GamePOCreator extends PatternObjectCreator {
    @Override
    public Object getSendableInstance(boolean reference) {
        if(reference) {
            return new GamePO(new Game[] {});
        } else {
            return new GamePO();
        }
    }

    public static IdMap createIdMap(String sessionID) {
        return de.uniks.tictactoe.model.util.CreatorCreator.createIdMap(sessionID);
    }
}
