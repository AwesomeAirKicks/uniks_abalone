package de.uniks.tictactoe.model.util;

import org.sdmlib.models.pattern.AttributeConstraint;
import org.sdmlib.models.pattern.Pattern;
import org.sdmlib.models.pattern.PatternObject;

import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Player;
import de.uniks.tictactoe.model.Token;

public class PlayerPO extends PatternObject<PlayerPO, Player> {

    public PlayerSet allMatches() {
        this.setDoAllMatches(true);

        PlayerSet matches = new PlayerSet();

        while(this.getPattern().getHasMatch()) {
            matches.add((Player) this.getCurrentMatch());

            this.getPattern().findMatch();
        }

        return matches;
    }

    public PlayerPO() {
        newInstance(null);
    }

    public PlayerPO(Player... hostGraphObject) {
        if(hostGraphObject == null || hostGraphObject.length < 1) {
            return;
        }
        newInstance(null, hostGraphObject);
    }

    public PlayerPO(String modifier) {
        this.setModifier(modifier);
    }

    public PlayerPO createNameCondition(String value) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_NAME)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public PlayerPO createNameCondition(String lower, String upper) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_NAME)
                .withTgtValue(lower)
                .withUpperTgtValue(upper)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public PlayerPO createNameAssignment(String value) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_NAME)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public String getName() {
        if(this.getPattern().getHasMatch()) {
            return ((Player) getCurrentMatch()).getName();
        }
        return null;
    }

    public PlayerPO withName(String value) {
        if(this.getPattern().getHasMatch()) {
            ((Player) getCurrentMatch()).setName(value);
        }
        return this;
    }

    public PlayerPO createSymboleCondition(String value) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_SYMBOLE)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public PlayerPO createSymboleCondition(String lower, String upper) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_SYMBOLE)
                .withTgtValue(lower)
                .withUpperTgtValue(upper)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public PlayerPO createSymboleAssignment(String value) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_SYMBOLE)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public String getSymbole() {
        if(this.getPattern().getHasMatch()) {
            return ((Player) getCurrentMatch()).getSymbole();
        }
        return null;
    }

    public PlayerPO withSymbole(String value) {
        if(this.getPattern().getHasMatch()) {
            ((Player) getCurrentMatch()).setSymbole(value);
        }
        return this;
    }

    public PlayerPO createScoreCondition(int value) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_SCORE)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public PlayerPO createScoreCondition(int lower, int upper) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_SCORE)
                .withTgtValue(lower)
                .withUpperTgtValue(upper)
                .withSrc(this)
                .withModifier(this.getPattern().getModifier())
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public PlayerPO createScoreAssignment(int value) {
        new AttributeConstraint()
                .withAttrName(Player.PROPERTY_SCORE)
                .withTgtValue(value)
                .withSrc(this)
                .withModifier(Pattern.CREATE)
                .withPattern(this.getPattern());

        super.filterAttr();

        return this;
    }

    public int getScore() {
        if(this.getPattern().getHasMatch()) {
            return ((Player) getCurrentMatch()).getScore();
        }
        return 0;
    }

    public PlayerPO withScore(int value) {
        if(this.getPattern().getHasMatch()) {
            ((Player) getCurrentMatch()).setScore(value);
        }
        return this;
    }

    public GamePO createGamePO() {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Player.PROPERTY_GAME, result);

        return result;
    }

    public GamePO createGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(modifier);
        super.hasLink(Player.PROPERTY_GAME, result);

        return result;
    }

    public PlayerPO createGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, Player.PROPERTY_GAME);
    }

    public PlayerPO createGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, Player.PROPERTY_GAME, modifier);
    }

    public Game getGame() {
        if(this.getPattern().getHasMatch()) {
            return ((Player) this.getCurrentMatch()).getGame();
        }
        return null;
    }

    public GamePO createCurrentGamePO() {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Player.PROPERTY_CURRENTGAME, result);

        return result;
    }

    public GamePO createCurrentGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(modifier);
        super.hasLink(Player.PROPERTY_CURRENTGAME, result);

        return result;
    }

    public PlayerPO createCurrentGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, Player.PROPERTY_CURRENTGAME);
    }

    public PlayerPO createCurrentGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, Player.PROPERTY_CURRENTGAME, modifier);
    }

    public Game getCurrentGame() {
        if(this.getPattern().getHasMatch()) {
            return ((Player) this.getCurrentMatch()).getCurrentGame();
        }
        return null;
    }

    public GamePO createWonGamePO() {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Player.PROPERTY_WONGAME, result);

        return result;
    }

    public GamePO createWonGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(modifier);
        super.hasLink(Player.PROPERTY_WONGAME, result);

        return result;
    }

    public PlayerPO createWonGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, Player.PROPERTY_WONGAME);
    }

    public PlayerPO createWonGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, Player.PROPERTY_WONGAME, modifier);
    }

    public Game getWonGame() {
        if(this.getPattern().getHasMatch()) {
            return ((Player) this.getCurrentMatch()).getWonGame();
        }
        return null;
    }

    public TokenPO createTokensPO() {
        TokenPO result = new TokenPO(new Token[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Player.PROPERTY_TOKENS, result);

        return result;
    }

    public TokenPO createTokensPO(String modifier) {
        TokenPO result = new TokenPO(new Token[] {});

        result.setModifier(modifier);
        super.hasLink(Player.PROPERTY_TOKENS, result);

        return result;
    }

    public PlayerPO createTokensLink(TokenPO tgt) {
        return hasLinkConstraint(tgt, Player.PROPERTY_TOKENS);
    }

    public PlayerPO createTokensLink(TokenPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Player.PROPERTY_TOKENS, modifier);
    }

    public TokenSet getTokens() {
        if(this.getPattern().getHasMatch()) {
            return ((Player) this.getCurrentMatch()).getTokens();
        }
        return null;
    }

    public PlayerPO createPreviousPO() {
        PlayerPO result = new PlayerPO(new Player[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Player.PROPERTY_PREVIOUS, result);

        return result;
    }

    public PlayerPO createPreviousPO(String modifier) {
        PlayerPO result = new PlayerPO(new Player[] {});

        result.setModifier(modifier);
        super.hasLink(Player.PROPERTY_PREVIOUS, result);

        return result;
    }

    public PlayerPO createPreviousLink(PlayerPO tgt) {
        return hasLinkConstraint(tgt, Player.PROPERTY_PREVIOUS);
    }

    public PlayerPO createPreviousLink(PlayerPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Player.PROPERTY_PREVIOUS, modifier);
    }

    public Player getPrevious() {
        if(this.getPattern().getHasMatch()) {
            return ((Player) this.getCurrentMatch()).getPrevious();
        }
        return null;
    }

    public PlayerPO createNextPO() {
        PlayerPO result = new PlayerPO(new Player[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Player.PROPERTY_NEXT, result);

        return result;
    }

    public PlayerPO createNextPO(String modifier) {
        PlayerPO result = new PlayerPO(new Player[] {});

        result.setModifier(modifier);
        super.hasLink(Player.PROPERTY_NEXT, result);

        return result;
    }

    public PlayerPO createNextLink(PlayerPO tgt) {
        return hasLinkConstraint(tgt, Player.PROPERTY_NEXT);
    }

    public PlayerPO createNextLink(PlayerPO tgt, String modifier) {
        return hasLinkConstraint(tgt, Player.PROPERTY_NEXT, modifier);
    }

    public Player getNext() {
        if(this.getPattern().getHasMatch()) {
            return ((Player) this.getCurrentMatch()).getNext();
        }
        return null;
    }

}
