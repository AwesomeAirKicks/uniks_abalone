package de.uniks.tictactoe.model.util;

import org.sdmlib.models.pattern.PatternObject;

import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Logic;

public class LogicPO extends PatternObject<LogicPO, Logic> {

    public LogicSet allMatches() {
        this.setDoAllMatches(true);

        LogicSet matches = new LogicSet();

        while(this.getPattern().getHasMatch()) {
            matches.add((Logic) this.getCurrentMatch());

            this.getPattern().findMatch();
        }

        return matches;
    }

    public LogicPO() {
        newInstance(null);
    }

    public LogicPO(Logic... hostGraphObject) {
        if(hostGraphObject == null || hostGraphObject.length < 1) {
            return;
        }
        newInstance(null, hostGraphObject);
    }

    public LogicPO(String modifier) {
        this.setModifier(modifier);
    }

    public GamePO createGamePO() {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(this.getPattern().getModifier());
        super.hasLink(Logic.PROPERTY_GAME, result);

        return result;
    }

    public GamePO createGamePO(String modifier) {
        GamePO result = new GamePO(new Game[] {});

        result.setModifier(modifier);
        super.hasLink(Logic.PROPERTY_GAME, result);

        return result;
    }

    public LogicPO createGameLink(GamePO tgt) {
        return hasLinkConstraint(tgt, Logic.PROPERTY_GAME);
    }

    public LogicPO createGameLink(GamePO tgt, String modifier) {
        return hasLinkConstraint(tgt, Logic.PROPERTY_GAME, modifier);
    }

    public Game getGame() {
        if(this.getPattern().getHasMatch()) {
            return ((Logic) this.getCurrentMatch()).getGame();
        }
        return null;
    }

}
