/*
 * Copyright (c) 2017 Ich Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.uniks.tictactoe.model.util;

import java.util.Collection;

import de.uniks.networkparser.interfaces.Condition;
import de.uniks.networkparser.list.NumberList;
import de.uniks.networkparser.list.ObjectSet;
import de.uniks.networkparser.list.SimpleSet;
import de.uniks.tictactoe.model.Board;
import de.uniks.tictactoe.model.Field;
import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Token;

public class FieldSet extends SimpleSet<Field> {
    protected Class<?> getTypClass() {
        return Field.class;
    }

    public FieldSet() {
        // empty
    }

    public FieldSet(Field... objects) {
        for(Field obj: objects) {
            this.add(obj);
        }
    }

    public FieldSet(Collection<Field> objects) {
        this.addAll(objects);
    }

    public static final FieldSet EMPTY_SET = new FieldSet().withFlag(FieldSet.READONLY);

    public FieldPO createFieldPO() {
        return new FieldPO(this.toArray(new Field[this.size()]));
    }

    public String getEntryType() {
        return "de.uniks.tictactoe.model.Field";
    }

    @Override
    public FieldSet getNewList(boolean keyValue) {
        return new FieldSet();
    }

    public FieldSet filter(Condition<Field> condition) {
        FieldSet filterList = new FieldSet();
        filterItems(filterList, condition);
        return filterList;
    }

    @SuppressWarnings("unchecked")
    public FieldSet with(Object value) {
        if(value == null) {
            return this;
        } else if(value instanceof java.util.Collection) {
            this.addAll((Collection<Field>) value);
        } else if(value != null) {
            this.add((Field) value);
        }

        return this;
    }

    public FieldSet without(Field value) {
        this.remove(value);
        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a list of the x attribute values.
     * 
     * @return List of int objects reachable via x attribute
     */
    public NumberList getX() {
        NumberList result = new NumberList();

        for(Field obj: this) {
            result.add(obj.getX());
        }

        return result;
    }

    /**
     * Loop through the current set of Field objects and collect those Field objects where the x attribute matches the
     * parameter value.
     * 
     * @param value Search value
     * 
     * @return Subset of Field objects that match the parameter
     */
    public FieldSet filterX(int value) {
        FieldSet result = new FieldSet();

        for(Field obj: this) {
            if(value == obj.getX()) {
                result.add(obj);
            }
        }

        return result;
    }

    /**
     * Loop through the current set of Field objects and collect those Field objects where the x attribute is between
     * lower and upper.
     * 
     * @param lower Lower bound
     * @param upper Upper bound
     * 
     * @return Subset of Field objects that match the parameter
     */
    public FieldSet filterX(int lower, int upper) {
        FieldSet result = new FieldSet();

        for(Field obj: this) {
            if(lower <= obj.getX() && obj.getX() <= upper) {
                result.add(obj);
            }
        }

        return result;
    }

    /**
     * Loop through the current set of Field objects and assign value to the x attribute of each of it.
     * 
     * @param value New attribute value
     * 
     * @return Current set of Field objects now with new attribute values.
     */
    public FieldSet withX(int value) {
        for(Field obj: this) {
            obj.setX(value);
        }

        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a list of the y attribute values.
     * 
     * @return List of int objects reachable via y attribute
     */
    public NumberList getY() {
        NumberList result = new NumberList();

        for(Field obj: this) {
            result.add(obj.getY());
        }

        return result;
    }

    /**
     * Loop through the current set of Field objects and collect those Field objects where the y attribute matches the
     * parameter value.
     * 
     * @param value Search value
     * 
     * @return Subset of Field objects that match the parameter
     */
    public FieldSet filterY(int value) {
        FieldSet result = new FieldSet();

        for(Field obj: this) {
            if(value == obj.getY()) {
                result.add(obj);
            }
        }

        return result;
    }

    /**
     * Loop through the current set of Field objects and collect those Field objects where the y attribute is between
     * lower and upper.
     * 
     * @param lower Lower bound
     * @param upper Upper bound
     * 
     * @return Subset of Field objects that match the parameter
     */
    public FieldSet filterY(int lower, int upper) {
        FieldSet result = new FieldSet();

        for(Field obj: this) {
            if(lower <= obj.getY() && obj.getY() <= upper) {
                result.add(obj);
            }
        }

        return result;
    }

    /**
     * Loop through the current set of Field objects and assign value to the y attribute of each of it.
     * 
     * @param value New attribute value
     * 
     * @return Current set of Field objects now with new attribute values.
     */
    public FieldSet withY(int value) {
        for(Field obj: this) {
            obj.setY(value);
        }

        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a set of the Game objects reached via game.
     * 
     * @return Set of Game objects reachable via game
     */
    public GameSet getGame() {
        GameSet result = new GameSet();

        for(Field obj: this) {
            result.with(obj.getGame());
        }

        return result;
    }

    /**
     * Loop through the current set of Field objects and collect all contained objects with reference game pointing to
     * the object passed as parameter.
     * 
     * @param value The object required as game neighbor of the collected results.
     * 
     * @return Set of Game objects referring to value via game
     */
    public FieldSet filterGame(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        FieldSet answer = new FieldSet();

        for(Field obj: this) {
            if(neighbors.contains(obj.getGame()) || (neighbors.isEmpty() && obj.getGame() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Field object passed as parameter to the Game
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their Game attributes.
     */
    public FieldSet withGame(Game value) {
        for(Field obj: this) {
            obj.withGame(value);
        }

        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a set of the Board objects reached via board.
     * 
     * @return Set of Board objects reachable via board
     */
    public BoardSet getBoard() {
        BoardSet result = new BoardSet();

        for(Field obj: this) {
            result.with(obj.getBoard());
        }

        return result;
    }

    /**
     * Loop through the current set of Field objects and collect all contained objects with reference board pointing to
     * the object passed as parameter.
     * 
     * @param value The object required as board neighbor of the collected results.
     * 
     * @return Set of Board objects referring to value via board
     */
    public FieldSet filterBoard(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        FieldSet answer = new FieldSet();

        for(Field obj: this) {
            if(neighbors.contains(obj.getBoard()) || (neighbors.isEmpty() && obj.getBoard() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Field object passed as parameter to the Board
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their Board attributes.
     */
    public FieldSet withBoard(Board value) {
        for(Field obj: this) {
            obj.withBoard(value);
        }

        return this;
    }

    /**
     * Loop through the current set of Field objects and collect a set of the Token objects reached via token.
     * 
     * @return Set of Token objects reachable via token
     */
    public TokenSet getToken() {
        TokenSet result = new TokenSet();

        for(Field obj: this) {
            result.with(obj.getToken());
        }

        return result;
    }

    /**
     * Loop through the current set of Field objects and collect all contained objects with reference token pointing to
     * the object passed as parameter.
     * 
     * @param value The object required as token neighbor of the collected results.
     * 
     * @return Set of Token objects referring to value via token
     */
    public FieldSet filterToken(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        FieldSet answer = new FieldSet();

        for(Field obj: this) {
            if(neighbors.contains(obj.getToken()) || (neighbors.isEmpty() && obj.getToken() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Field object passed as parameter to the Token
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their Token attributes.
     */
    public FieldSet withToken(Token value) {
        for(Field obj: this) {
            obj.withToken(value);
        }

        return this;
    }

}
