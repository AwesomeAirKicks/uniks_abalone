package de.uniks.tictactoe.model.util;

import org.sdmlib.models.pattern.util.PatternObjectCreator;

import de.uniks.networkparser.IdMap;
import de.uniks.tictactoe.model.Logic;

public class LogicPOCreator extends PatternObjectCreator {
    @Override
    public Object getSendableInstance(boolean reference) {
        if(reference) {
            return new LogicPO(new Logic[] {});
        } else {
            return new LogicPO();
        }
    }

    public static IdMap createIdMap(String sessionID) {
        return de.uniks.tictactoe.model.util.CreatorCreator.createIdMap(sessionID);
    }
}
