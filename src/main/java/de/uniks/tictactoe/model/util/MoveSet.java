/*
 * Copyright (c) 2017 Ich Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The
 * above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software. The Software shall be used for Good, not Evil. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.uniks.tictactoe.model.util;

import java.util.Collection;

import de.uniks.networkparser.interfaces.Condition;
import de.uniks.networkparser.list.ObjectSet;
import de.uniks.networkparser.list.SimpleSet;
import de.uniks.tictactoe.model.Field;
import de.uniks.tictactoe.model.Move;
import de.uniks.tictactoe.model.Player;

public class MoveSet extends SimpleSet<Move> {
    protected Class<?> getTypClass() {
        return Move.class;
    }

    public MoveSet() {
        // empty
    }

    public MoveSet(Move... objects) {
        for(Move obj: objects) {
            this.add(obj);
        }
    }

    public MoveSet(Collection<Move> objects) {
        this.addAll(objects);
    }

    public static final MoveSet EMPTY_SET = new MoveSet().withFlag(MoveSet.READONLY);

    public MovePO createMovePO() {
        return new MovePO(this.toArray(new Move[this.size()]));
    }

    public String getEntryType() {
        return "de.uniks.tictactoe.model.Move";
    }

    @Override
    public MoveSet getNewList(boolean keyValue) {
        return new MoveSet();
    }

    public MoveSet filter(Condition<Move> condition) {
        MoveSet filterList = new MoveSet();
        filterItems(filterList, condition);
        return filterList;
    }

    @SuppressWarnings("unchecked")
    public MoveSet with(Object value) {
        if(value == null) {
            return this;
        } else if(value instanceof java.util.Collection) {
            this.addAll((Collection<Move>) value);
        } else if(value != null) {
            this.add((Move) value);
        }

        return this;
    }

    public MoveSet without(Move value) {
        this.remove(value);
        return this;
    }

    /**
     * Loop through the current set of Move objects and collect a set of the Field objects reached via field.
     * 
     * @return Set of Field objects reachable via field
     */
    public FieldSet getField() {
        FieldSet result = new FieldSet();

        for(Move obj: this) {
            result.with(obj.getField());
        }

        return result;
    }

    /**
     * Loop through the current set of Move objects and collect all contained objects with reference field pointing to
     * the object passed as parameter.
     * 
     * @param value The object required as field neighbor of the collected results.
     * 
     * @return Set of Field objects referring to value via field
     */
    public MoveSet filterField(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        MoveSet answer = new MoveSet();

        for(Move obj: this) {
            if(neighbors.contains(obj.getField()) || (neighbors.isEmpty() && obj.getField() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Move object passed as parameter to the Field
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their Field attributes.
     */
    public MoveSet withField(Field value) {
        for(Move obj: this) {
            obj.withField(value);
        }

        return this;
    }

    /**
     * Loop through the current set of Move objects and collect a set of the Player objects reached via player.
     * 
     * @return Set of Player objects reachable via player
     */
    public PlayerSet getPlayer() {
        PlayerSet result = new PlayerSet();

        for(Move obj: this) {
            result.with(obj.getPlayer());
        }

        return result;
    }

    /**
     * Loop through the current set of Move objects and collect all contained objects with reference player pointing to
     * the object passed as parameter.
     * 
     * @param value The object required as player neighbor of the collected results.
     * 
     * @return Set of Player objects referring to value via player
     */
    public MoveSet filterPlayer(Object value) {
        ObjectSet neighbors = new ObjectSet();

        if(value instanceof Collection) {
            neighbors.addAll((Collection<?>) value);
        } else {
            neighbors.add(value);
        }

        MoveSet answer = new MoveSet();

        for(Move obj: this) {
            if(neighbors.contains(obj.getPlayer()) || (neighbors.isEmpty() && obj.getPlayer() == null)) {
                answer.add(obj);
            }
        }

        return answer;
    }

    /**
     * Loop through current set of ModelType objects and attach the Move object passed as parameter to the Player
     * attribute of each of it.
     * 
     * @return The original set of ModelType objects now with the new neighbor attached to their Player attributes.
     */
    public MoveSet withPlayer(Player value) {
        for(Move obj: this) {
            obj.withPlayer(value);
        }

        return this;
    }

}
