package de.uniks.modelgenerator;

import org.junit.Test;
import org.sdmlib.models.classes.ClassModel;

import de.uniks.networkparser.graph.Cardinality;
import de.uniks.networkparser.graph.Clazz;
import de.uniks.networkparser.graph.DataType;

public class TicTacToeModel {
    @Test
    public void generateModel() {
        // New class model
        ClassModel model = new ClassModel("de.uniks.tictactoe.model");

        // Classes
        Clazz game = model.createClazz("Game");
        Clazz board = model.createClazz("Board");
        Clazz field = model.createClazz("Field");
        Clazz token = model.createClazz("Token");
        Clazz player = model.createClazz("Player");
        Clazz move = model.createClazz("Move");
        Clazz logic = model.createClazz("Logic");
        Clazz aiplayer = model.createClazz("AIPlayer");

        // Attributes
        player.withAttribute("name", DataType.STRING).withAttribute("symbole", DataType.STRING).withAttribute("score",
                DataType.INT);
        field.withAttribute("x", DataType.INT).withAttribute("y", DataType.INT);
        aiplayer.withAttribute("strength", DataType.INT);

        // Associations
        game.withBidirectional(board, "board", Cardinality.ONE, "game", Cardinality.ONE)
                .withBidirectional(field, "fields", Cardinality.MANY, "game", Cardinality.ONE)
                .withBidirectional(token, "tokens", Cardinality.MANY, "game", Cardinality.ONE)
                .withBidirectional(player, "players", Cardinality.MANY, "game", Cardinality.ONE)
                .withBidirectional(player, "currentPlayer", Cardinality.ONE, "currentGame", Cardinality.ONE)
                .withBidirectional(player, "winner", Cardinality.ONE, "wonGame", Cardinality.ONE)
                .withBidirectional(logic, "logic", Cardinality.ONE, "game", Cardinality.ONE);
        board.withBidirectional(field, "fields", Cardinality.MANY, "board", Cardinality.ONE);
        field.withBidirectional(token, "token", Cardinality.ONE, "field", Cardinality.ONE);
        player.withBidirectional(player, "next", Cardinality.ONE, "previous", Cardinality.ONE)
                .withBidirectional(token, "tokens", Cardinality.MANY, "player", Cardinality.ONE);
        move.withUniDirectional(field, "field", Cardinality.ONE);
        move.withUniDirectional(player, "player", Cardinality.ONE);

        // Inheritances
        player.withKidClazzes(aiplayer);

        // Generate model and class diagram
        // model.dumpHTML("TicTacToeClassDiagram");
        // model.generate("src/main/java");
    }
}