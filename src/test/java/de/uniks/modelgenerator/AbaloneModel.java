package de.uniks.modelgenerator;

import org.junit.Test;
import org.sdmlib.models.classes.ClassModel;

import de.uniks.networkparser.graph.Cardinality;
import de.uniks.networkparser.graph.Clazz;
import de.uniks.networkparser.graph.DataType;

public class AbaloneModel {
    @Test
    public void generateModel() {
        // New class model
        ClassModel model = new ClassModel("de.uniks.abalone.model");

        // Classes
        Clazz game = model.createClazz("Game");
        Clazz board = model.createClazz("Board");
        Clazz field = model.createClazz("Field");
        Clazz token = model.createClazz("Token");
        Clazz player = model.createClazz("Player");
        Clazz move = model.createClazz("Move");
        Clazz aiplayer = model.createClazz("AIPlayer");
        Clazz logic = model.createClazz("Logic");

        // Attributes
        player.withAttribute("name", DataType.STRING)
                .withAttribute("color", DataType.STRING)
                .withAttribute("scores", DataType.INT);
        field.withAttribute("x", DataType.INT)
                .withAttribute("y", DataType.INT)
                .withAttribute("selected", DataType.BOOLEAN)
                .withAttribute("lastMoved", DataType.BOOLEAN);
        move.withAttribute("direction", DataType.INT);
        aiplayer.withAttribute("strength", DataType.INT);

        // Associations
        game.withBidirectional(board, "board", Cardinality.ONE, "game", Cardinality.ONE)
                .withBidirectional(field, "fields", Cardinality.MANY, "game", Cardinality.ONE)
                .withBidirectional(token, "tokens", Cardinality.MANY, "game", Cardinality.ONE)
                .withBidirectional(player, "players", Cardinality.MANY, "game", Cardinality.ONE)
                .withBidirectional(player, "currentPlayer", Cardinality.ONE, "currentGame", Cardinality.ONE)
                .withBidirectional(player, "winner", Cardinality.ONE, "wonGame", Cardinality.ONE)
                .withBidirectional(logic, "logic", Cardinality.ONE, "game", Cardinality.ONE)
                .withUniDirectional(move, "history", Cardinality.MANY);
        board.withBidirectional(field, "fields", Cardinality.MANY, "board", Cardinality.ONE);
        field.withBidirectional(token, "token", Cardinality.ONE, "field", Cardinality.ONE)
                .withBidirectional(field, "left", Cardinality.ONE, "right", Cardinality.ONE)
                .withBidirectional(field, "topRight", Cardinality.ONE, "downLeft", Cardinality.ONE)
                .withBidirectional(field, "topLeft", Cardinality.ONE, "downRight", Cardinality.ONE);
        player.withBidirectional(player, "next", Cardinality.ONE, "previous", Cardinality.ONE)
                .withBidirectional(token, "tokens", Cardinality.MANY, "player", Cardinality.ONE);
        player.withKidClazzes(aiplayer);
        move.withUniDirectional(token, "tokens", Cardinality.MANY)
                .withUniDirectional(token, "opponentTokens", Cardinality.MANY)
                .withUniDirectional(field, "previousPosition", Cardinality.ONE);
        logic.withUniDirectional(move, "currentMove", Cardinality.ONE);

        // Generate model and class diagram
        // model.dumpHTML("AbaloneClassDiagram");
        // model.generate("src/main/java");
    }
}