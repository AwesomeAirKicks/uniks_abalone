package de.uniks.abalone;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;
import de.uniks.abalone.tools.Layouts;

public class CheckWinnerTest {
    @Test
    public void noWinner() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]  --
        --------------------------------------------
        0         B B B B B
        1        B B B B B B
        2       O O B B B O O
        3      O O O O O O O O
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O W W W O O 7
        7        W W W W W W 6
        8         W W W W W 5
                   0 1 2 3 4
        --------------------------------------------*/
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, Layouts.DEFAULT_TOKEN_LAYOUT);
        // action
        Player winner = game.getLogic().checkWinner(game.getBoard());
        // -----
        game.getBoard().printBoard();
        assertEquals(null, winner);
    }

    @Test
    public void noWinner2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]  --
        --------------------------------------------
        0         B B B B B
        1        B B B B B B
        2       O O B B B O O
        3      O O O O O O O O
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O W O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 1, 1, 1, 1, 1},
                {0, 0, 0, 1, 1, 1, 1, 1, 1},
                {0, 0, 0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, tokenLayout);
        // action
        Player winner = game.getLogic().checkWinner(game.getBoard());
        // -----
        game.getBoard().printBoard();
        assertEquals(null, winner);
    }

    @Test
    public void winnerP1() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]  --
        --------------------------------------------
        0         B B B B B
        1        B B B B B B
        2       O O B B B O O
        3      O O O O O O O O
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 1, 1, 1, 1, 1},
                {0, 0, 0, 1, 1, 1, 1, 1, 1},
                {0, 0, 0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(p1, p2, tokenLayout);
        p2.withTokens(new Token(), new Token(), new Token(), new Token(), new Token(), new Token());
        // action
        Player winner = game.getLogic().checkWinner(game.getBoard());
        // -----
        game.getBoard().printBoard();
        assertEquals(p1, winner);
    }

    @Test
    public void winnerP2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]  --
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O O O O
        4     O O O O O O O O O
        5      O O O O W O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 2, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        p1.withTokens(new Token(), new Token(), new Token(), new Token(), new Token(), new Token());
        p2.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.init(p1, p2, tokenLayout);
        // action
        Player winner = game.getLogic().checkWinner(game.getBoard());
        // -----
        game.getBoard().printBoard();
        assertEquals(p2, winner);
    }
}
