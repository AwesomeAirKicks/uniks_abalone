package de.uniks.abalone;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.uniks.abalone.model.AIPlayer;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Player;

public class EvaluateAbaloneTest {
    @Test
    public void centerDistanceTest() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         B B B B B
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O O O O 
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         W W W W W 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {5, 5, 5, 5, 1, 1, 1, 1, 1},
                {5, 5, 5, 0, 0, 0, 0, 0, 0},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {0, 0, 0, 0, 0, 0, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        int[][] tokenLayout2 = {
                {5, 5, 5, 5, 1, 1, 1, 1, 0},
                {5, 5, 5, 0, 0, 0, 0, 1, 0},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {0, 0, 0, 0, 0, 0, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(aiPlayer, player, tokenLayout);
        int score1 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score1);
        aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        player = new Player().withColor("white").withName("Bob");
        game = new Game();
        game.init(aiPlayer, player, tokenLayout2);
        int score2 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score2);
        assertEquals(true, score2 > score1);
    }

    @Test
    public void centerDistanceTest2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         B B B B B
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O O O O 
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         W W W W W 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {5, 5, 5, 5, 1, 1, 1, 1, 1},
                {5, 5, 5, 0, 0, 0, 0, 0, 0},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {0, 0, 0, 0, 0, 0, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        int[][] tokenLayout2 = {
                {5, 5, 5, 5, 1, 1, 1, 0, 1},
                {5, 5, 5, 0, 0, 0, 0, 1, 0},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {0, 0, 0, 0, 0, 0, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(aiPlayer, player, tokenLayout);
        int score1 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score1);
        aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        player = new Player().withColor("white").withName("Bob");
        game = new Game();
        game.init(aiPlayer, player, tokenLayout2);
        int score2 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score2);
        assertEquals(true, score2 > score1);
    }

    @Test
    public void centerDistanceTest3() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         B B B B B
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O O O O 
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         W W W W W 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {5, 5, 5, 5, 1, 1, 1, 1, 1},
                {5, 5, 5, 0, 0, 0, 0, 0, 0},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {0, 0, 0, 0, 0, 0, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        int[][] tokenLayout2 = {
                {5, 5, 5, 5, 1, 1, 0, 1, 1},
                {5, 5, 5, 0, 0, 0, 1, 0, 0},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {0, 0, 0, 0, 0, 0, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(aiPlayer, player, tokenLayout);
        int score1 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score1);
        aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        player = new Player().withColor("white").withName("Bob");
        game = new Game();
        game.init(aiPlayer, player, tokenLayout2);
        int score2 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score2);
        assertEquals(true, score2 > score1);
    }

    @Test
    public void centerDistanceTest4() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         B B B B B
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O O O O 
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         W W W W W 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {5, 5, 5, 5, 1, 1, 1, 1, 1},
                {5, 5, 5, 0, 0, 0, 0, 0, 0},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {0, 0, 0, 0, 0, 0, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        int[][] tokenLayout2 = {
                {5, 5, 5, 5, 1, 0, 1, 1, 1},
                {5, 5, 5, 0, 0, 1, 0, 0, 0},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {0, 0, 0, 0, 0, 0, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(aiPlayer, player, tokenLayout);
        int score1 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score1);
        aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        player = new Player().withColor("white").withName("Bob");
        game = new Game();
        game.init(aiPlayer, player, tokenLayout2);
        int score2 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score2);
        assertEquals(true, score2 > score1);
    }

    @Test
    public void centerDistanceTest5() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         B B B B B
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O O O O 
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         W W W W W 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {5, 5, 5, 5, 1, 1, 1, 1, 1},
                {5, 5, 5, 0, 0, 0, 0, 0, 0},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {0, 0, 0, 0, 0, 0, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        int[][] tokenLayout2 = {
                {5, 5, 5, 5, 0, 1, 1, 1, 1},
                {5, 5, 5, 0, 1, 0, 0, 0, 0},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {0, 0, 0, 0, 0, 0, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(aiPlayer, player, tokenLayout);
        int score1 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score1);
        aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        player = new Player().withColor("white").withName("Bob");
        game = new Game();
        game.init(aiPlayer, player, tokenLayout2);
        int score2 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score2);
        assertEquals(true, score2 > score1);
    }

    @Test
    public void centerDistanceTest8() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         B B B B B
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O O O O 
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         W W W W W 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {5, 5, 5, 5, 1, 1, 1, 1, 1},
                {5, 5, 5, 0, 0, 0, 0, 0, 0},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {0, 0, 0, 0, 0, 0, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        int[][] tokenLayout2 = {
                {5, 5, 5, 5, 1, 1, 1, 1, 1},
                {5, 5, 5, 1, 0, 0, 0, 0, 1},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {2, 0, 0, 0, 0, 2, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(aiPlayer, player, tokenLayout);
        int score1 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score1);
        aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        player = new Player().withColor("white").withName("Bob");
        game = new Game();
        game.init(aiPlayer, player, tokenLayout2);
        int score2 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score2);
        assertEquals(score2, score1, 2.0d);
    }

    @Test
    public void centerDistanceTest9() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         B B B B B
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O O O O 
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         W W W W W 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {5, 5, 5, 5, 1, 1, 1, 1, 1},
                {5, 5, 5, 0, 0, 0, 0, 0, 0},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {0, 0, 0, 0, 0, 0, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        int[][] tokenLayout2 = {
                {5, 5, 5, 5, 1, 1, 1, 1, 1},
                {5, 5, 5, 0, 0, 0, 0, 0, 1},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {2, 0, 0, 0, 0, 0, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(aiPlayer, player, tokenLayout);
        int score1 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score1);
        aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        player = new Player().withColor("white").withName("Bob");
        game = new Game();
        game.init(aiPlayer, player, tokenLayout2);
        int score2 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score2);
        assertEquals(score2, score1, 2.0d);
    }

    @Test
    public void centerDistanceTest10() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         B B B B B
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O O O O 
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         W W W W W 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {5, 5, 5, 5, 1, 1, 1, 1, 1},
                {5, 5, 5, 0, 0, 0, 0, 0, 0},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {0, 0, 0, 0, 0, 0, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        int[][] tokenLayout2 = {
                {5, 5, 5, 5, 1, 1, 1, 1, 1},
                {5, 5, 5, 1, 0, 0, 0, 0, 0},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {2, 0, 0, 0, 0, 0, 5, 5, 5},
                {2, 2, 2, 2, 2, 5, 5, 5, 5}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(aiPlayer, player, tokenLayout);
        int score1 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score1);
        aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        player = new Player().withColor("white").withName("Bob");
        game = new Game();
        game.init(aiPlayer, player, tokenLayout2);
        int score2 = game.getLogic().evaluate(game.getBoard(), player);
        System.out.println(score2);
        assertEquals(score2, score1, 2.0d);
    }
}
