package de.uniks.abalone;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.tools.Layouts;

public class InitGameTest {
    @Test
    public void initGameTest() {
        // ----setup---
        Player p1 = new Player().withColor("black").withName("Alice");
        Player p2 = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        // ---action---
        game.init(p1, p2, Layouts.DEFAULT_TOKEN_LAYOUT);
        // ---result---
        game.getBoard().print2DimArray();
        game.getBoard().printBoard();
        // 61 fields
        assertEquals(61, game.getBoard().getFields().size());
        // 28 tokens
        assertEquals(28, game.getTokens().size());
        // 2 player
        assertEquals(2, game.getPlayers().size());
        // 14 tokens per player
        assertEquals(14, game.getPlayers().get(0).getTokens().size());
        assertEquals(14, game.getPlayers().get(1).getTokens().size());
    }
}
