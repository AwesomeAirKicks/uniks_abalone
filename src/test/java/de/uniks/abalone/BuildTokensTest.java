package de.uniks.abalone;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.uniks.abalone.model.Board;
import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.tools.Layouts;

public class BuildTokensTest {
    /*-
     * --------------------------------------------
     * -- row = y        dia = x         a[y][x] --
     * --------------------------------------------
     * 0           B B B B B
     * 1          B B B B B B
     * 2         O O B B B O O
     * 3        O O O O O O O O
     * 4       O O O O O O O O O
     * 5        O O O O O O O O 8
     * 6         O O Y Y Y O O 7
     * 7          Y Y Y Y Y Y 6
     * 8           Y Y Y Y Y 5
     * 0            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void DefaultLayoutTest() {
        // init
        Board board = new Board();
        board.buildFields();
        Player bottom = new Player().withColor("Blue");
        Player top = new Player().withColor("Yellow");
        // action
        board.buildTokens(bottom, top, Layouts.DEFAULT_TOKEN_LAYOUT);
        // check
        board.print2DimArray();
        board.printBoard();
        Field[][] fields = board.getFieldsAs2DimArray();
        // Player "top" tokens
        assertEquals(bottom, fields[0][4].getToken().getPlayer());
        assertEquals(bottom, fields[0][5].getToken().getPlayer());
        assertEquals(bottom, fields[0][6].getToken().getPlayer());
        assertEquals(bottom, fields[0][7].getToken().getPlayer());
        assertEquals(bottom, fields[0][8].getToken().getPlayer());
        assertEquals(bottom, fields[1][3].getToken().getPlayer());
        assertEquals(bottom, fields[1][4].getToken().getPlayer());
        assertEquals(bottom, fields[1][5].getToken().getPlayer());
        assertEquals(bottom, fields[1][6].getToken().getPlayer());
        assertEquals(bottom, fields[1][7].getToken().getPlayer());
        assertEquals(bottom, fields[1][8].getToken().getPlayer());
        assertEquals(bottom, fields[2][4].getToken().getPlayer());
        assertEquals(bottom, fields[2][5].getToken().getPlayer());
        assertEquals(bottom, fields[2][6].getToken().getPlayer());
        // Player "bottom" tokens
        assertEquals(top, fields[8][0].getToken().getPlayer());
        assertEquals(top, fields[8][1].getToken().getPlayer());
        assertEquals(top, fields[8][2].getToken().getPlayer());
        assertEquals(top, fields[8][3].getToken().getPlayer());
        assertEquals(top, fields[8][4].getToken().getPlayer());
        assertEquals(top, fields[7][0].getToken().getPlayer());
        assertEquals(top, fields[7][1].getToken().getPlayer());
        assertEquals(top, fields[7][2].getToken().getPlayer());
        assertEquals(top, fields[7][3].getToken().getPlayer());
        assertEquals(top, fields[7][4].getToken().getPlayer());
        assertEquals(top, fields[7][5].getToken().getPlayer());
        assertEquals(top, fields[6][2].getToken().getPlayer());
        assertEquals(top, fields[6][3].getToken().getPlayer());
        assertEquals(top, fields[6][4].getToken().getPlayer());
    }

    /*-
     * --------------------------------------------
     * -- row = y        dia = x         a[y][x] --
     * --------------------------------------------
     * 0           Y Y O B B
     * 1          Y Y Y B B B
     * 2         O Y Y O B B O
     * 3        O O O O O O O O
     * 4       O O O O O O O O O
     * 5        O O O O O O O O 8
     * 6         O B B O Y Y O 7
     * 7          B B B Y Y Y 6
     * 8           B B O Y Y 5
     * 0            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void BelgianDaisyLayoutTest() {
        // init
        Board board = new Board();
        board.buildFields();
        Player p1 = new Player().withColor("Blue");
        Player p2 = new Player().withColor("Yellow");
        // action
        board.buildTokens(p1, p2, Layouts.BELGIAN_DAISY_TOKEN_LAYOUT);
        // check
        board.print2DimArray();
        board.printBoard();
        Field[][] fields = board.getFieldsAs2DimArray();
        // Player "p1" tokens
        assertEquals(p2, fields[0][4].getToken().getPlayer());
        assertEquals(p2, fields[0][5].getToken().getPlayer());
        assertEquals(p2, fields[1][3].getToken().getPlayer());
        assertEquals(p2, fields[1][4].getToken().getPlayer());
        assertEquals(p2, fields[1][5].getToken().getPlayer());
        assertEquals(p2, fields[2][3].getToken().getPlayer());
        assertEquals(p2, fields[2][4].getToken().getPlayer());
        assertEquals(p2, fields[6][4].getToken().getPlayer());
        assertEquals(p2, fields[6][5].getToken().getPlayer());
        assertEquals(p2, fields[7][3].getToken().getPlayer());
        assertEquals(p2, fields[7][4].getToken().getPlayer());
        assertEquals(p2, fields[7][5].getToken().getPlayer());
        assertEquals(p2, fields[8][3].getToken().getPlayer());
        assertEquals(p2, fields[8][4].getToken().getPlayer());
        // Player "p2" tokens
        assertEquals(p1, fields[0][7].getToken().getPlayer());
        assertEquals(p1, fields[0][8].getToken().getPlayer());
        assertEquals(p1, fields[1][6].getToken().getPlayer());
        assertEquals(p1, fields[1][7].getToken().getPlayer());
        assertEquals(p1, fields[1][8].getToken().getPlayer());
        assertEquals(p1, fields[2][6].getToken().getPlayer());
        assertEquals(p1, fields[2][7].getToken().getPlayer());
        assertEquals(p1, fields[6][1].getToken().getPlayer());
        assertEquals(p1, fields[6][2].getToken().getPlayer());
        assertEquals(p1, fields[7][0].getToken().getPlayer());
        assertEquals(p1, fields[7][1].getToken().getPlayer());
        assertEquals(p1, fields[7][2].getToken().getPlayer());
        assertEquals(p1, fields[8][0].getToken().getPlayer());
        assertEquals(p1, fields[8][1].getToken().getPlayer());
    }
}
