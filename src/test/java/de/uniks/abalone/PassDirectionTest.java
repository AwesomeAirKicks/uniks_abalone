package de.uniks.abalone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;
import de.uniks.abalone.tools.Layouts;

/**
 * This test verifies the correct functionality of the {@code passDirection()} method of the game logic. Some of the
 * tests need a little bit of preparation to construct the necessary board situation.
 */
public class PassDirectionTest {
    // This is the game object with which all the tests will operate
    private Game game;

    /*
     * This method reinitializes the game object before every test. There are two players called Alice and Bob. Bob
     * plays with the white tokens. Alice plays with the black ones and for the tests she is the active player. The game
     * starts with the classic configuration.
     */
    @Before
    public void initTestBench() {
        Player alice = new Player().withName("Alice").withColor("BLACK");
        Player bob = new Player().withName("Bob").withColor("WHITE");
        this.game = new Game();
        this.game.init(alice, bob, Layouts.DEFAULT_TOKEN_LAYOUT);
    }

    @Test
    public void passADirectionRightAtTheBeginning() {
        for(int i = 0; i < 6; i++) {
            this.game.getLogic().passDirection(i);
            assertNull(this.game.getLogic().getCurrentMove());
            assertEquals(-1, this.game.getLogic().getAlignment());
            assertFalse(this.game.getLogic().isValdidMove());
        }
    }

    @Test
    public void passADirectionWithAnInvalidMove() {
        Field field1 = this.game.getFields().filterX(4).filterY(2).first();
        Field field2 = field1.getRight().getRight();
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passDirection(2);
        assertEquals(2, this.game.getLogic().getCurrentMove().getTokens().size());
        assertFalse(this.game.getLogic().isValdidMove());
    }

    // Legal move with one token
    @Test
    public void moveASingleTokenToAnEmptyField() {
        Field field = this.game.getFields().filterX(8).filterY(1).first();
        this.game.getLogic().passField(field);
        this.game.getLogic().passDirection(Move.DOWN_LEFT);
        assertNull(field.getToken());
        assertNotNull(field.getDownLeft().getToken());
        assertEquals("Bob", this.game.getCurrentPlayer().getName());
        // Also test the reset of the Logic
        assertNull(this.game.getLogic().getCurrentMove());
        assertEquals(-1, this.game.getLogic().getAlignment());
        assertFalse(this.game.getLogic().isValdidMove());
    }

    // Illegal moves with one token
    @Test
    public void tryToMoveASingleTokenToAFieldWithAnOwnToken() {
        Field field = this.game.getFields().filterX(8).filterY(1).first();
        this.game.getLogic().passField(field);
        this.game.getLogic().passDirection(Move.TOP_LEFT);
        assertNotNull(field.getToken());
        assertNotNull(field.getTopLeft().getToken());
        assertEquals("Alice", this.game.getCurrentPlayer().getName());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B B B
     * 2       O O B B B O W
     * 3      O O O O O O O O
     * 4     O O O O O O O O O
     * 5      O O O O O O O O 8
     * 6       O O W W W O O 7
     * 7        W W W W W O 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void tryToMoveASingleTokenToAFieldWithAnEnemyToken() {
        // Preparation: Move (5, 7) to (8, 2)
        Field enemy = this.game.getFields().filterX(5).filterY(7).first();
        Field field = this.game.getFields().filterX(8).filterY(1).first();
        field.getDownRight().withToken(enemy.getToken());
        this.game.getLogic().passField(field);
        this.game.getLogic().passDirection(Move.DOWN_RIGHT);
        assertNotNull(field.getToken());
        assertNotNull(field.getDownRight().getToken());
        assertEquals("Alice", this.game.getCurrentPlayer().getName());
    }

    @Test
    public void tryToMoveASingleTokenOverTheEdge() {
        Field field = this.game.getFields().filterX(8).filterY(1).first();
        this.game.getLogic().passField(field);
        this.game.getLogic().passDirection(Move.RIGHT);
        assertNotNull(field.getToken());
        assertEquals("Alice", this.game.getCurrentPlayer().getName());
    }

    // Legal moves with two tokens
    @Test
    public void moveTwoTokensAlongTheirAlignmentOnEmptyFields() {
        Field field1 = this.game.getFields().filterX(8).filterY(0).first();
        Field field2 = field1.getDownRight();
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passDirection(Move.DOWN_RIGHT);
        assertNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(field2.getDownRight().getToken());
        assertEquals("Bob", this.game.getCurrentPlayer().getName());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B B B
     * 2       O O B B B W O
     * 3      O O O O O O O O
     * 4     O O O O O O O O O
     * 5      O O O O O O O O 8
     * 6       O O W W W O O 7
     * 7        W W W W W O 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void pushOneEnemyTokenToAnEmptyFieldWithTwoTokens() {
        // Preparation: Move (5, 7) to (7, 2)
        Field enemy = this.game.getFields().filterX(5).filterY(7).first();
        Field field1 = this.game.getFields().filterX(7).filterY(0).first();
        Field field2 = field1.getDownRight();
        field2.getDownRight().withToken(enemy.getToken());
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passDirection(Move.DOWN_RIGHT);
        assertNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertEquals("Alice", field2.getToken().getPlayer().getName());
        assertNotNull(field2.getDownRight().getToken());
        assertEquals("Alice", field2.getDownRight().getToken().getPlayer().getName());
        assertNotNull(field2.getDownRight().getDownRight().getToken());
        assertEquals("Bob", field2.getDownRight().getDownRight().getToken().getPlayer().getName());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B B B
     * 2       O O B O B B W
     * 3      O O O O O O O O
     * 4     O O O O O O O O O
     * 5      O O O O O O O O 8
     * 6       O O W W W O O 7
     * 7        W W W W W O 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void pushOneEnemyTokenOverTheEdgeWithTwoTokens() {
        // Preparation: Move (5, 7) to (8, 2) and (5, 2) to (7, 2)
        Field enemy = this.game.getFields().filterX(5).filterY(7).first();
        Field field1 = this.game.getFields().filterX(6).filterY(2).first();
        Field field2 = field1.getRight();
        Token enemysToken = enemy.getToken();
        field2.getRight().withToken(enemysToken);
        field2.withToken(field1.getLeft().getToken());
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passDirection(Move.RIGHT);
        assertNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertEquals("Alice", field2.getToken().getPlayer().getName());
        assertNotNull(field2.getRight().getToken());
        assertEquals("Alice", field2.getRight().getToken().getPlayer().getName());
        assertNull(enemysToken.getField());
        assertEquals(1, this.game.getPlayers().filterName("Alice").first().getScores());
    }

    @Test
    public void moveTwoTokensParallelToTheirAlignmentToEmptyFiels() {
        Field field1 = this.game.getFields().filterX(6).filterY(2).first();
        Field field2 = field1.getLeft();
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passDirection(Move.DOWN_LEFT);
        assertNull(field1.getToken());
        assertNull(field2.getToken());
        assertNotNull(field1.getDownLeft().getToken());
        assertEquals("Alice", field1.getDownLeft().getToken().getPlayer().getName());
        assertNotNull(field2.getDownLeft().getToken());
        assertEquals("Alice", field2.getDownLeft().getToken().getPlayer().getName());
    }

    // Illegal moves with two tokens
    @Test
    public void tryToPushAnOwnTokenOverTheEdgeWithTwoTokens() {
        Field field1 = this.game.getFields().filterX(6).filterY(0).first();
        Field field2 = field1.getLeft();
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passDirection(Move.LEFT);
        assertNotNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(field2.getLeft().getToken());
        assertNotNull(this.game.getLogic().getCurrentMove());
    }

    @Test
    public void tryToPushAnOwnTokenWithTwoOwnTokens() {
        Field field1 = this.game.getFields().filterX(4).filterY(2).first();
        Field field2 = field1.getRight();
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passDirection(Move.RIGHT);
        assertNotNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(field2.getRight().getToken());
        assertNotNull(this.game.getLogic().getCurrentMove());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B B B
     * 2       O B W B B O O
     * 3      O O O O O O O O
     * 4     O O O O O O O O O
     * 5      O O O O O O O O 8
     * 6       O O W W O O O 7
     * 7        W W W W W W 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void tryToPushAnEnemyTokenAgainstAnotherOwnToken() {
        // Preparation: Move (4, 2) to (3, 2) and (4, 6) to (4, 2)
        Field enemy = this.game.getFields().filterX(4).filterY(6).first();
        Field field1 = this.game.getFields().filterX(6).filterY(2).first();
        Field field2 = field1.getLeft();
        field2.getLeft().getLeft().withToken(field2.getLeft().getToken());
        field2.getLeft().withToken(enemy.getToken());
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passDirection(Move.LEFT);
        assertNotNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(field2.getLeft().getToken());
        assertEquals("Bob", field2.getLeft().getToken().getPlayer().getName());
        assertNotNull(this.game.getLogic().getCurrentMove());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B B B
     * 2       O O B B B O O
     * 3      O O O O O W O O
     * 4     O O O O O O W O O
     * 5      O O O O O O O O 8
     * 6       O O W O O O O 7
     * 7        W W W W W W 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void tryToPushTwoEnemyTokensWithTwoTokens() {
        // Preparation: Move (4, 6) to (6, 3) and (3, 6) to (6, 4)
        Field enemy1 = this.game.getFields().filterX(4).filterY(6).first();
        Field enemy2 = enemy1.getLeft();
        Field field1 = this.game.getFields().filterX(6).filterY(1).first();
        Field field2 = field1.getDownRight();
        field2.getDownRight().withToken(enemy1.getToken());
        field2.getDownRight().getDownRight().withToken(enemy2.getToken());
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passDirection(Move.DOWN_RIGHT);
        assertNotNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(field2.getDownRight().getToken());
        assertNotNull(field2.getDownRight().getDownRight().getToken());
        assertNotNull(this.game.getLogic().getCurrentMove());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B B B
     * 2       O O B B B O O
     * 3      O O O O O W O O
     * 4     O O O O O O W O O
     * 5      O O O O O O W O 8
     * 6       O O O O O O O 7
     * 7        W W W W W W 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void tryToPushThreeEnemyTokensWithTwoTokens() {
        // Preparation: Move (4, 6) to (6, 3), (3, 6) to (6, 4) and (2, 6) to (6, 5)
        Field enemy1 = this.game.getFields().filterX(4).filterY(6).first();
        Field enemy2 = enemy1.getLeft();
        Field enemy3 = enemy2.getLeft();
        Field field1 = this.game.getFields().filterX(6).filterY(1).first();
        Field field2 = field1.getDownRight();
        field2.getDownRight().withToken(enemy1.getToken());
        field2.getDownRight().getDownRight().withToken(enemy2.getToken());
        field2.getDownRight().getDownRight().getDownRight().withToken(enemy3.getToken());
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passDirection(Move.DOWN_RIGHT);
        assertNotNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(field2.getDownRight().getToken());
        assertNotNull(field2.getDownRight().getDownRight().getToken());
        assertNotNull(field2.getDownRight().getDownRight().getDownRight().getToken());
        assertNotNull(this.game.getLogic().getCurrentMove());
    }

    @Test
    public void tryToMoveTwoTokensParallelToTheirAlignmentToBlockedFields() {
        Field field1 = this.game.getFields().filterX(6).filterY(2).first();
        Field field2 = field1.getLeft();
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passDirection(Move.TOP_LEFT);
        assertNotNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(this.game.getLogic().getCurrentMove());
    }

    @Test
    public void tryToMoveTwoTokensParallelToTheirAlignmentOverTheEdge() {
        Field field1 = this.game.getFields().filterX(8).filterY(1).first();
        Field field2 = field1.getTopLeft();
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passDirection(Move.RIGHT);
        assertNotNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(this.game.getLogic().getCurrentMove());
    }

    // Legal moves with three tokens
    @Test
    public void moveThreeTokensAlongTheirAlignmentOnEmptyFields() {
        Field field1 = this.game.getFields().filterX(6).filterY(0).first();
        Field field2 = field1.getDownRight();
        Field field3 = field2.getDownRight();
        this.game.getLogic().passField(field3);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passDirection(Move.DOWN_RIGHT);
        assertNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(field3.getToken());
        assertNotNull(field3.getDownRight().getToken());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B B B
     * 2       O O B B B W O
     * 3      O O O O O O O O
     * 4     O O O O O O O O O
     * 5      O O O O O O O O 8
     * 6       O O W W W O O 7
     * 7        W W W W W O 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void pushOneEnemyTokenToAnEmptyFieldWithThreeTokens() {
        // Preparation: Move (5, 7) to (7, 2)
        Field enemy = this.game.getFields().filterX(5).filterY(7).first();
        Field field1 = this.game.getFields().filterX(4).filterY(2).first();
        Field field2 = field1.getRight();
        Field field3 = field2.getRight();
        field3.getRight().withToken(enemy.getToken());
        this.game.getLogic().passField(field3);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passDirection(Move.RIGHT);
        assertNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertEquals("Alice", field2.getToken().getPlayer().getName());
        assertNotNull(field3.getToken());
        assertEquals("Alice", field3.getToken().getPlayer().getName());
        assertNotNull(field3.getRight().getToken());
        assertEquals("Alice", field3.getRight().getToken().getPlayer().getName());
        assertNotNull(field3.getRight().getRight().getToken());
        assertEquals("Bob", field3.getRight().getRight().getToken().getPlayer().getName());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B B B
     * 2       O O B B B O O
     * 3      O O O O O W O O
     * 4     O O O O O O W O O
     * 5      O O O O O O O O 8
     * 6       O O W O O O O 7
     * 7        W W W W W W 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void pushTwoEnemyTokensToEmptyFields() {
        // Preparation: Move (3, 6) to (6, 3) and (4, 6) to (6, 4)
        Field enemy1 = this.game.getFields().filterX(3).filterY(6).first();
        Field enemy2 = enemy1.getRight();
        Field field1 = this.game.getFields().filterX(6).filterY(0).first();
        Field field2 = field1.getDownRight();
        Field field3 = field2.getDownRight();
        field3.getDownRight().withToken(enemy1.getToken());
        field3.getDownRight().getDownRight().withToken(enemy2.getToken());
        this.game.getLogic().passField(field3);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passDirection(Move.DOWN_RIGHT);
        assertNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertEquals("Alice", field2.getToken().getPlayer().getName());
        assertNotNull(field3.getToken());
        assertEquals("Alice", field3.getToken().getPlayer().getName());
        assertNotNull(field3.getDownRight().getToken());
        assertEquals("Alice", field3.getDownRight().getToken().getPlayer().getName());
        assertNotNull(field3.getDownRight().getDownRight().getToken());
        assertEquals("Bob", field3.getDownRight().getDownRight().getToken().getPlayer().getName());
        assertNotNull(field3.getDownRight().getDownRight().getDownRight().getToken());
        assertEquals("Bob", field3.getDownRight().getDownRight().getDownRight().getToken().getPlayer().getName());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B B B
     * 2       O O O B B B W
     * 3      O O O O O O O O
     * 4     O O O O O O O O O
     * 5      O O O O O O O O 8
     * 6       O O W W W O O 7
     * 7        W W W W W O 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void pushOneEnemyTokenOverTheEdgeWithThreeTokens() {
        // Preparation: Move (5, 7) to (8, 2) and (4, 2) to (7, 2)
        Field enemy = this.game.getFields().filterX(5).filterY(7).first();
        Token enemyToken = enemy.getToken();
        Field field1 = this.game.getFields().filterX(7).filterY(2).first();
        Field field2 = field1.getLeft();
        Field field3 = field2.getLeft();
        field1.getRight().withToken(enemy.getToken());
        field1.withToken(field3.getLeft().getToken());
        this.game.getLogic().passField(field3);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passDirection(Move.RIGHT);
        assertNull(field3.getToken());
        assertNotNull(field2.getToken());
        assertEquals("Alice", field2.getToken().getPlayer().getName());
        assertNotNull(field1.getToken());
        assertEquals("Alice", field1.getToken().getPlayer().getName());
        assertNotNull(field1.getRight().getToken());
        assertEquals("Alice", field1.getRight().getToken().getPlayer().getName());
        assertNull(enemyToken.getField());
        assertEquals(1, this.game.getPlayers().filterName("Alice").first().getScores());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B B B
     * 2       W W B B B O O
     * 3      O O O O O O O O
     * 4     O O O O O O O O O
     * 5      O O O O O O O O 8
     * 6       O O O O W O O 7
     * 7        W W W W W W 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void pushTwoEnemyTokensAndTheLastOneOverTheEdge() {
        // Preparation: Move (3, 6) to (3, 2) and (2, 6) to (2, 2)
        Field enemy1 = this.game.getFields().filterX(3).filterY(6).first();
        Field enemy2 = enemy1.getLeft();
        Token enemyToken = enemy2.getToken();
        Field field1 = this.game.getFields().filterX(4).filterY(2).first();
        Field field2 = field1.getRight();
        Field field3 = field2.getRight();
        field1.getLeft().withToken(enemy1.getToken());
        field1.getLeft().getLeft().withToken(enemy2.getToken());
        this.game.getLogic().passField(field2);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field3);
        this.game.getLogic().passDirection(Move.LEFT);
        assertNull(field3.getToken());
        assertNotNull(field2.getToken());
        assertEquals("Alice", field2.getToken().getPlayer().getName());
        assertNotNull(field1.getToken());
        assertEquals("Alice", field1.getToken().getPlayer().getName());
        assertNotNull(field1.getLeft().getToken());
        assertEquals("Alice", field1.getLeft().getToken().getPlayer().getName());
        assertNotNull(field1.getLeft().getLeft().getToken());
        assertEquals("Bob", field1.getLeft().getLeft().getToken().getPlayer().getName());
        assertNull(enemyToken.getField());
        assertEquals(1, this.game.getPlayers().filterName("Alice").first().getScores());
    }

    @Test
    public void moveThreeTokensParallelToTheirAlignmentToEmptyFields() {
        Field field1 = this.game.getFields().filterX(4).filterY(2).first();
        Field field2 = field1.getRight();
        Field field3 = field2.getRight();
        this.game.getLogic().passField(field3);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passDirection(Move.DOWN_LEFT);
        assertNull(field1.getToken());
        assertNull(field2.getToken());
        assertNull(field3.getToken());
        assertNotNull(field1.getDownLeft().getToken());
        assertNotNull(field2.getDownLeft().getToken());
        assertNotNull(field3.getDownLeft().getToken());
    }

    // Illegal moves with three tokens
    @Test
    public void tryToPushAnOwnTokenOverTheEdgeWithThreeTokens() {
        Field field1 = this.game.getFields().filterX(6).filterY(0).first();
        Field field2 = field1.getRight();
        Field field3 = field2.getRight();
        this.game.getLogic().passField(field3);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passDirection(Move.RIGHT);
        assertNotNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(field3.getToken());
        assertNotNull(this.game.getLogic().getCurrentMove());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B B O
     * 2       O O B B B O O
     * 3      O O O O O B O O
     * 4     O O O O O O O O O
     * 5      O O O O O O O O 8
     * 6       O O W W W O O 7
     * 7        W W W W W W 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void tryToPushAnOwnTokenWithThreeOwnTokens() {
        // Preparation: Move (8, 1) to (6, 3)
        Field field1 = this.game.getFields().filterX(6).filterY(2).first();
        Field field2 = field1.getTopLeft();
        Field field3 = field2.getTopLeft();
        field1.getDownRight().withToken(field2.getRight().getRight().getToken());
        this.game.getLogic().passField(field3);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passDirection(Move.DOWN_RIGHT);
        assertNotNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(field3.getToken());
        assertNotNull(field1.getDownRight().getToken());
        assertNotNull(this.game.getLogic().getCurrentMove());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B O B
     * 2       O O B B B O O
     * 3      O O O O O W O O
     * 4     O O O O O O B O O
     * 5      O O O O O O O O 8
     * 6       O O W W O O O 7
     * 7        W W W W W W 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void tryToPushOneEnemyTokenAgainstAnotherOwnToken() {
        // Preparation: Move (4, 6) to (6, 3) and (7, 1) to (6, 4)
        Field enemy = this.game.getFields().filterX(4).filterY(6).first();
        Field field1 = this.game.getFields().filterX(6).filterY(2).first();
        Field field2 = field1.getTopLeft();
        Field field3 = field2.getTopLeft();
        field1.getDownRight().withToken(enemy.getToken());
        field1.getDownRight().getDownRight().withToken(field2.getRight().getToken());
        this.game.getLogic().passField(field3);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passDirection(Move.DOWN_RIGHT);
        assertNotNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(field3.getToken());
        assertNotNull(field1.getDownRight().getToken());
        assertNotNull(field1.getDownRight().getDownRight().getToken());
        assertNotNull(this.game.getLogic().getCurrentMove());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B O B
     * 2       O O B B B O O
     * 3      O O O O O W O O
     * 4     O O O O O O W O O
     * 5      O O O O O O B O 8
     * 6       O O W O O O O 7
     * 7        W W W W W W 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void tryToPushTwoEnemyTokensAgainstAnotherOwnToken() {
        // Preparation: Move (4, 6) to (6, 3), (3, 6) to (6, 4) and (7, 1) to (6, 5)
        Field field1 = this.game.getFields().filterX(6).filterY(2).first();
        Field field2 = field1.getTopLeft();
        Field field3 = field2.getTopLeft();
        Field enemy1 = this.game.getFields().filterX(4).filterY(6).first();
        Field enemy2 = enemy1.getLeft();
        field1.getDownRight().withToken(enemy1.getToken());
        field1.getDownRight().getDownRight().withToken(enemy2.getToken());
        field1.getDownRight().getDownRight().getDownRight().withToken(field2.getRight().getToken());
        this.game.getLogic().passField(field3);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passDirection(Move.DOWN_RIGHT);
        assertNotNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(field3.getToken());
        assertNotNull(field1.getDownRight().getToken());
        assertNotNull(field1.getDownRight().getDownRight().getToken());
        assertNotNull(field1.getDownRight().getDownRight().getDownRight().getToken());
        assertNotNull(this.game.getLogic().getCurrentMove());
    }

    /*-
     * --------------------------------------------
     * --    row = y       dia = x      a[y][x]  --
     * --------------------------------------------
     * 0         B B B B B
     * 1        B B B B B B
     * 2       O O B B B O O
     * 3      O O O O O W O O
     * 4     O O O O O O W O O
     * 5      O O O O O O W O 8
     * 6       O O O O O O O 7
     * 7        W W W W W W 6
     * 8         W W W W W 5
     *            0 1 2 3 4
     * --------------------------------------------
     */
    @Test
    public void tryToPushThreeEnemyTokensWithThreeTokens() {
        // Preparation: Move (2, 6) to (6, 3), (3, 6) to (6, 4) and (4, 6) to (6, 5)
        Field field1 = this.game.getFields().filterX(6).filterY(2).first();
        Field field2 = field1.getTopLeft();
        Field field3 = field2.getTopLeft();
        Field enemy1 = this.game.getFields().filterX(2).filterY(6).first();
        Field enemy2 = enemy1.getRight();
        Field enemy3 = enemy2.getRight();
        field1.getDownRight().withToken(enemy1.getToken());
        field1.getDownRight().getDownRight().withToken(enemy2.getToken());
        field1.getDownRight().getDownRight().getDownRight().withToken(enemy3.getToken());
        this.game.getLogic().passField(field3);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passDirection(Move.DOWN_RIGHT);
        assertNotNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(field3.getToken());
        assertNotNull(field1.getDownRight().getToken());
        assertNotNull(field1.getDownRight().getDownRight().getToken());
        assertNotNull(field1.getDownRight().getDownRight().getDownRight().getToken());
        assertNotNull(this.game.getLogic().getCurrentMove());
    }

    @Test
    public void tryToMoveThreeTokensParallelToTheirAlignmentToBlockedFields() {
        Field field1 = this.game.getFields().filterX(6).filterY(1).first();
        Field field2 = field1.getRight();
        Field field3 = field2.getRight();
        this.game.getLogic().passField(field3);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passDirection(Move.DOWN_RIGHT);
        assertNotNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(field3.getToken());
        assertNotNull(this.game.getLogic().getCurrentMove());
    }

    @Test
    public void tryToMoveThreeTokensParallelToTheirAlignmentOverTheEdge() {
        Field field1 = this.game.getFields().filterX(6).filterY(0).first();
        Field field2 = field1.getRight();
        Field field3 = field2.getRight();
        this.game.getLogic().passField(field3);
        this.game.getLogic().passField(field1);
        this.game.getLogic().passField(field2);
        this.game.getLogic().passDirection(Move.TOP_RIGHT);
        assertNotNull(field1.getToken());
        assertNotNull(field2.getToken());
        assertNotNull(field3.getToken());
        assertNotNull(this.game.getLogic().getCurrentMove());
    }
}