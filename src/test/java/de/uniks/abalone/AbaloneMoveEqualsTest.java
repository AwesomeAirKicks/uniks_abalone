package de.uniks.abalone;

import static org.junit.Assert.*;

import org.junit.Test;

import de.uniks.abalone.model.Field;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Token;

public class AbaloneMoveEqualsTest {

    @Test
    public void twoMovesAreEqualTest() {

        Move move1 = new Move();

        Token token1 = new Token();
        Field field1 = new Field().withX(2).withY(1);
        token1.withField(field1);

        Token token2 = new Token();
        Field field2 = new Field().withX(3).withY(2);
        token2.withField(field2);

        Token token3 = new Token();
        Field field3 = new Field().withX(4).withY(3);
        token3.withField(field3);

        move1.withTokens(token1).withTokens(token2);
        move1.withOpponentTokens(token3);

        Move move2 = new Move();

        Token token4 = new Token();
        Field field4 = new Field().withX(2).withY(1);
        token4.withField(field4);

        Token token5 = new Token();
        Field field5 = new Field().withX(3).withY(2);
        token5.withField(field5);

        Token token6 = new Token();
        Field field6 = new Field().withX(4).withY(3);
        token6.withField(field6);

        move2.withTokens(token4).withTokens(token5);
        move2.withOpponentTokens(token6);

        boolean equals1 = move1.equals(move2);
        assertTrue(equals1);
        boolean equals2 = move2.equals(move1);
        assertTrue(equals2);

    }

    @Test
    public void twoMovesAreNotEqualTest() {
        Move move1 = new Move();

        Token token1 = new Token();
        Field field1 = new Field().withX(2).withY(1);
        token1.withField(field1);

        Token token2 = new Token();
        Field field2 = new Field().withX(3).withY(2);
        token2.withField(field2);

        Token token3 = new Token();
        Field field3 = new Field().withX(4).withY(3);
        token3.withField(field3);

        move1.withTokens(token1).withTokens(token2);
        move1.withOpponentTokens(token3);

        Move move2 = new Move();

        Token token4 = new Token();
        Field field4 = new Field().withX(2).withY(1);
        token4.withField(field4);

        Token token5 = new Token();
        Field field5 = new Field().withX(3).withY(2);
        token5.withField(field5);

        Token token6 = new Token();
        Field field6 = new Field().withX(4).withY(4);
        token6.withField(field6);

        move2.withTokens(token4).withTokens(token5);
        move2.withOpponentTokens(token6);

        boolean equals1 = move1.equals(move2);
        assertFalse(equals1);
        boolean equals2 = move2.equals(move1);
        assertFalse(equals2);
    }

    @Test
    public void twoMovesAreEqualButHaveDiffrentTokenOrderTest() {
        Move move1 = new Move();

        Token token1 = new Token();
        Field field1 = new Field().withX(2).withY(1);
        token1.withField(field1);

        Token token2 = new Token();
        Field field2 = new Field().withX(3).withY(2);
        token2.withField(field2);

        Token token3 = new Token();
        Field field3 = new Field().withX(4).withY(3);
        token3.withField(field3);

        move1.withTokens(token2).withTokens(token1);
        move1.withOpponentTokens(token3);

        Move move2 = new Move();

        Token token4 = new Token();
        Field field4 = new Field().withX(2).withY(1);
        token4.withField(field4);

        Token token5 = new Token();
        Field field5 = new Field().withX(3).withY(2);
        token5.withField(field5);

        Token token6 = new Token();
        Field field6 = new Field().withX(4).withY(3);
        token6.withField(field6);

        move2.withTokens(token4).withTokens(token5);
        move2.withOpponentTokens(token6);

        boolean equals1 = move1.equals(move2);
        assertTrue(equals1);
        boolean equals2 = move2.equals(move1);
        assertTrue(equals2);
    }

}
