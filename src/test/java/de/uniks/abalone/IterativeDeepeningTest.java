package de.uniks.abalone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.uniks.abalone.model.AIPlayer;
import de.uniks.abalone.model.Board;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Logic;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;
import de.uniks.abalone.tools.Layouts;
import de.uniks.ai.IterativeDeepening;

public class IterativeDeepeningTest {

    @Test
    public void defaultLayout_TimeSetTo1000_maxDepth10() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         B B B B B
        1        B B B B B B
        2       O O B B B O O
        3      O O O O O O O O
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O W W W O O 7
        7        W W W W W W 6
        8         W W W W W 5
                   0 1 2 3 4
        --------------------------------------------
        */

        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> id = new IterativeDeepening<>();
        int timeLimit = 1000;
        id.setTimeLimit(timeLimit);
        game.init(aiPlayer, player, Layouts.DEFAULT_TOKEN_LAYOUT);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        long start = System.currentTimeMillis();
        Move bestMove = id.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 10);
        long end = System.currentTimeMillis();
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(timeLimit, end - start, 300d);
    }

    @Test
    public void endGameSituation_TimeSetTo1000_maxDepth10() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> id = new IterativeDeepening<>();
        int timeLimit = 1000;
        id.setTimeLimit(timeLimit);
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        long start = System.currentTimeMillis();
        Move bestMove = id.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 10);
        long end = System.currentTimeMillis();
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(timeLimit, end - start, 300d);
        assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));

    }

    @Test
    public void defaultLayout_NoTimeLimit_maxDepth10_abortAfter1Sec() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         B B B B B
        1        B B B B B B
        2       O O B B B O O
        3      O O O O O O O O
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O W W W O O 7
        7        W W W W W W 6
        8         W W W W W 5
                   0 1 2 3 4
        --------------------------------------------
        */

        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> ab = new IterativeDeepening<>();
        int timeLimit = 1000;
        game.init(aiPlayer, player, Layouts.DEFAULT_TOKEN_LAYOUT);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());

        new Thread("AbortThread") {
            @Override
            public void run() {
                try {
                    Thread.sleep(timeLimit);
                    ab.stopAiThread();
                } catch(InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();

        long start = System.currentTimeMillis();
        Move bestMove = ab.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 10);
        long end = System.currentTimeMillis();
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(timeLimit, end - start, 300d);
    }

    @Test
    public void endGameSituation_NoTimeLimit_maxDepth10_AbortAfter1Sec() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> ab = new IterativeDeepening<>();
        int timeLimit = 1000;
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());

        new Thread("AbortThread") {
            @Override
            public void run() {
                try {
                    Thread.sleep(timeLimit);
                    ab.stopAiThread();
                } catch(InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();

        long start = System.currentTimeMillis();
        Move bestMove = ab.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 10);
        long end = System.currentTimeMillis();
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(timeLimit, end - start, 300d);
        assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));

    }

    @Test
    public void defaultLayout_NoTimeLimit_maxDepth4_NoAbort() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         B B B B B
        1        B B B B B B
        2       O O B B B O O
        3      O O O O O O O O
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O W W W O O 7
        7        W W W W W W 6
        8         W W W W W 5
                   0 1 2 3 4
        --------------------------------------------
        */

        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> ab = new IterativeDeepening<>();
        game.init(aiPlayer, player, Layouts.DEFAULT_TOKEN_LAYOUT);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());

        Move bestMove = ab.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 4);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        // this test just has to terminate without exceptions
        assertTrue(true && false || true);
    }

    @Test
    public void endGameSituation_NoTimeLimit_maxDepth4_NoAbort() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> ab = new IterativeDeepening<>();
        int timeLimit = 1000;
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());

        new Thread("AbortThread") {
            @Override
            public void run() {
                try {
                    Thread.sleep(timeLimit);
                    ab.stopAiThread();
                } catch(InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();

        Move bestMove = ab.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 4);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));

    }

    //////////////////////////////////////////////
    //////////////////////////////////////////////
    // Tests copied from AlphaBetaGenericTest.java
    // all without timeLimit //////////////////
    //////////////////////////////////////////////
    @Test
    public void alphaBetaCheckWinnerTest_depth1() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> ab = new IterativeDeepening<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        Move bestMove = ab.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 1);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));
    }

    @Test
    public void alphaBetaCheckWinnerTest_depth2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> mm = new IterativeDeepening<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 2);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));
    }

    @Test
    public void alphaBetaCheckWinnerTest_depth3() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> mm = new IterativeDeepening<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));
    }

    @Test
    public void alphaBetaCheckWinnerTest_depth4() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> mm = new IterativeDeepening<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 4);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));
    }

    @Test
    public void alphaBetaDodgeTest_depth1() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O O O $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> mm = new IterativeDeepening<>();
        game.init(aiPlayer, player, tokenLayout);
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 1);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        boolean directionIs3Or2 = bestMove.getDirection() == 2 || bestMove.getDirection() == 3;
        assertTrue(directionIs3Or2);
    }

    @Test
    public void alphaBetaDodgeTest_depth2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O O O $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> mm = new IterativeDeepening<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 2);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void alphaBetaDodgeTest_depth3() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O O O $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> mm = new IterativeDeepening<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void alphaBetaDodgeTest_depth4() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O O O $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> mm = new IterativeDeepening<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 4);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void alphaBetaDelayLoseTest_depth2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O B B $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> mm = new IterativeDeepening<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 2);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void alphaBetaDelayLoseTest_depth3() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O B B $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> mm = new IterativeDeepening<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void alphaBetaDelayLoseTest_depth4() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O B B $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> mm = new IterativeDeepening<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 4);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void alphaBetaDodgeTest2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O B O O
        1        O O O B B W
        2       O O O B O O O
        3      O O O O O O O O
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> mm = new IterativeDeepening<>();
        game.init(aiPlayer, player, tokenLayout);
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 4);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        // assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));
    }

    @Test
    public void alphaBetaSomeTest() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {2, 2, 2, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        IterativeDeepening<Logic, Board, Move, Player> mm = new IterativeDeepening<>();
        game.init(aiPlayer, player, tokenLayout);
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 2);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(null, game.getPlayers().get(1).getTokens().get(0).getField());
    }
}
