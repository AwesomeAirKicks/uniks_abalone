package de.uniks.abalone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import de.uniks.abalone.model.AIPlayer;
import de.uniks.abalone.model.Board;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Logic;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;
import de.uniks.ai.AlphaBeta;
import de.uniks.ai.MiniMax;

public class CompareMinimaxAlphabetaAbaloneTest {
    @Test
    public void compareMinimaxAlphabetaAbaloneTest() {

        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O O O $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();

        int depth = 3;

        game.init(aiPlayer, player, tokenLayout);

        AlphaBeta<Logic, Board, Move, Player> ab = new AlphaBeta<>();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();

        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();

        Move bestMMMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), depth);
        Move bestABMove = ab.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 3);

        assertNotNull(bestMMMove);
        assertNotNull(bestABMove);

        // Compare the score of both strategies
        assertEquals(mm.getBestStrategy().getScore(), ab.getBestStrategy().getScore());

        // Get the first move of the strategy
        Move mMMove = (Move) mm.getBestStrategy().getMoveList().get(0);
        Move aBMove = (Move) ab.getBestStrategy().getMoveList().get(0);

        // compare moves
        assertEquals(mMMove.getTokens().get(0).getField().getX(), aBMove.getTokens().get(0).getField().getX());
        assertEquals(mMMove.getTokens().get(0).getField().getY(), aBMove.getTokens().get(0).getField().getY());

        // Get the first move of the strategy
        mMMove = (Move) mm.getBestStrategy().getMoveList().get(1);
        aBMove = (Move) ab.getBestStrategy().getMoveList().get(1);

        // compare moves
        assertEquals(mMMove.getTokens().get(0).getField().getX(), aBMove.getTokens().get(0).getField().getX());
        assertEquals(mMMove.getTokens().get(0).getField().getY(), aBMove.getTokens().get(0).getField().getY());

        // Get the first move of the strategy
        mMMove = (Move) mm.getBestStrategy().getMoveList().get(2);
        aBMove = (Move) ab.getBestStrategy().getMoveList().get(2);

        // compare moves
        assertEquals(mMMove.getTokens().get(0).getField().getX(), aBMove.getTokens().get(0).getField().getX());
        assertEquals(mMMove.getTokens().get(0).getField().getY(), aBMove.getTokens().get(0).getField().getY());
    }

}
