package de.uniks.abalone;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Test;

import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Token;

public class AbaloneMoveComparatorTest {
    @Test
    public void abaloneMoveComparatorTest() {

        // Player: two tokens, Opponent: one token
        Move move1 = new Move();
        move1.withTokens(new Token(), new Token());
        move1.withOpponentTokens(new Token());

        // Player: three tokens, Opponent: one token
        Move move2 = new Move();
        move2.withTokens(new Token(), new Token(), new Token());
        move2.withOpponentTokens(new Token());

        // Player: three tokens, Opponent: two token
        Move move3 = new Move();
        move3.withTokens(new Token(), new Token(), new Token());
        move3.withOpponentTokens(new Token(), new Token());

        // Player: three tokens, Opponent: two token
        Move move4 = new Move();
        move4.withTokens(new Token(), new Token(), new Token());
        move4.withOpponentTokens(new Token(), new Token());

        // Player: two tokens, Opponent: one token
        Move move5 = new Move();
        move5.withTokens(new Token(), new Token());

        ArrayList<Move> allMoves = new ArrayList<>();

        allMoves.add(move1);
        allMoves.add(move2);
        allMoves.add(move3);
        allMoves.add(move4);
        allMoves.add(move5);

        Collections.sort(allMoves, (m1, m2) -> m1.compareTo(m2));

        assertEquals(move3, allMoves.get(0));
        assertEquals(move4, allMoves.get(1));
        assertEquals(move2, allMoves.get(2));
        assertEquals(move1, allMoves.get(3));
        assertEquals(move5, allMoves.get(4));
    }
}