package de.uniks.abalone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.BitSet;

import org.junit.Test;

import de.uniks.abalone.model.AIPlayer;
import de.uniks.abalone.model.Board;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Logic;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;
import de.uniks.abalone.tools.Layouts;
import de.uniks.ai.MiniMax;
import de.uniks.ai.Strategy;
import de.uniks.ai.TTEntry;
import de.uniks.ai.TranspositionTableArray;

public class TranspositionTableTest {
    @Test
    public void equalityTest1() {
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        mm.setTraTableActive(false);
        game.init(aiPlayer, player, Layouts.BELGIAN_DAISY_TOKEN_LAYOUT);
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        BitSet producedBoardMinimax = game.getBoard().encode();

        aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        player = new Player().withColor("white").withName("Bob");
        game = new Game();
        mm = new MiniMax<>();
        mm.setTraTableActive(true);
        game.init(aiPlayer, player, Layouts.BELGIAN_DAISY_TOKEN_LAYOUT);
        bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        BitSet producedBoardMinimaxTranspoTable = game.getBoard().encode();

        assertEquals(producedBoardMinimax, producedBoardMinimaxTranspoTable);
    }

    @Test
    public void equalityTest2() {
        int[][] tokenLayout = {
                {5, 5, 5, 5, 0, 0, 1, 0, 0},
                {5, 5, 5, 0, 0, 0, 0, 0, 0},
                {5, 5, 0, 0, 0, 0, 0, 0, 0},
                {5, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 5},
                {0, 0, 0, 0, 0, 0, 0, 5, 5},
                {0, 0, 0, 0, 0, 0, 5, 5, 5},
                {0, 0, 2, 0, 0, 5, 5, 5, 5}};

        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        mm.setTraTableActive(false);
        game.init(aiPlayer, player, tokenLayout);
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        BitSet producedBoardMinimax = game.getBoard().encode();

        aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        player = new Player().withColor("white").withName("Bob");
        game = new Game();
        mm = new MiniMax<>();
        mm.setTraTableActive(true);
        game.init(aiPlayer, player, tokenLayout);
        bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        BitSet producedBoardMinimaxTranspoTable = game.getBoard().encode();

        assertEquals(producedBoardMinimax, producedBoardMinimaxTranspoTable);
    }

    @Test
    public void boardEncodeTest1() {
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(aiPlayer, player, Layouts.DEFAULT_TOKEN_LAYOUT);
        System.out.println(game.getBoard().encode());
        assertEquals(
                "{0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 26, 28, 30, 91, 93, 95, 101, 103, 105, 107, 109, 111, 113, 115, 117, 119, 121}",
                game.getBoard().encode().toString());
    }

    @Test
    public void boardEncodeTest2() {
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(aiPlayer, player, Layouts.BELGIAN_DAISY_TOKEN_LAYOUT);
        System.out.println(game.getBoard().encode());
        assertEquals(
                "{1, 3, 6, 8, 11, 13, 15, 16, 18, 20, 25, 27, 30, 32, 88, 90, 95, 97, 100, 102, 104, 107, 109, 111, 112, 114, 119, 121}",
                game.getBoard().encode().toString());
    }

    @Test
    public void boardEncodeTest3() {
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(aiPlayer, player, Layouts.DEFAULT_TOKEN_LAYOUT);
        System.out.println(game.getBoard().encode());
        assertEquals(
                "{0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 26, 28, 30, 91, 93, 95, 101, 103, 105, 107, 109, 111, 113, 115, 117, 119, 121}",
                game.getBoard().encode().toString());
    }

    @Test
    public void getEntry_correctDepth() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O B O O
        1        O O O B B W
        2       O O O B O O O
        3      O O O O O O O O
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(aiPlayer, player, Layouts.DEFAULT_TOKEN_LAYOUT);
        game.getBoard().printBoard();
        TranspositionTableArray<Move> tt = new TranspositionTableArray<>();
        TTEntry<Move> entry = new TTEntry<Move>(game.getBoard().encode(), new Strategy<Move>(), 7, true, 1);
        tt.put(entry);
        assertEquals(entry, tt.get(game.getBoard().encode(), 7, true));
    }

    @Test
    public void getEntry_lowerDepth() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O B O O
        1        O O O B B W
        2       O O O B O O O
        3      O O O O O O O O
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(aiPlayer, player, Layouts.DEFAULT_TOKEN_LAYOUT);
        game.getBoard().printBoard();
        TranspositionTableArray<Move> tt = new TranspositionTableArray<>();
        TTEntry<Move> entry = new TTEntry<Move>(game.getBoard().encode(), new Strategy<Move>(), 7, true, 1);
        tt.put(entry);
        assertEquals(entry, tt.get(game.getBoard().encode(), 6, true));
    }

    @Test
    public void getEntry_higherDepth() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O B O O
        1        O O O B B W
        2       O O O B O O O
        3      O O O O O O O O
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        game.init(aiPlayer, player, Layouts.DEFAULT_TOKEN_LAYOUT);
        game.getBoard().printBoard();
        TranspositionTableArray<Move> tt = new TranspositionTableArray<>();
        TTEntry<Move> entry = new TTEntry<Move>(game.getBoard().encode(), new Strategy<Move>(), 7, true, 1);
        tt.put(entry);
        assertEquals(null, tt.get(game.getBoard().encode(), 8, true));
    }
    /////////////////////////////

    @Test
    public void minimaxCheckWinnerTest_depth1() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        mm.setTraTableActive(true);
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 1);
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 1);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));
    }

    @Test
    public void minimaxCheckWinnerTest_depth2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        mm.setTraTableActive(true);
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 2);
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 2);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));
    }

    @Test
    public void minimaxCheckWinnerTest_depth3() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        mm.setTraTableActive(true);
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 3);
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));
    }

    @Test
    public void minimaxCheckWinnerTest_depth4() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        mm.setTraTableActive(true);
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 4);
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 4);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));
    }

    @Test
    public void dodgeTest_depth1() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O O O $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        mm.setTraTableActive(true);
        game.init(aiPlayer, player, tokenLayout);
        game.getBoard().printBoard();
        mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 1);
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 1);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        boolean directionIs3Or2 = bestMove.getDirection() == 2 || bestMove.getDirection() == 3;
        assertTrue(directionIs3Or2);
    }

    @Test
    public void dodgeTest_depth2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O O O $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        mm.setTraTableActive(true);
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 2);
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 2);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void dodgeTest_depth3() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O O O $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        mm.setTraTableActive(true);
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 3);
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void dodgeTest_depth4() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O O O $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        mm.setTraTableActive(true);
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 4);
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 4);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void delayLoseTest_depth2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O B B $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        mm.setTraTableActive(true);
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 2);
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 2);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void delayLoseTest_depth3() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O B B $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        mm.setTraTableActive(true);
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 3);
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void delayLoseTest_depth4() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O B B $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        mm.setTraTableActive(true);
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 4);
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 4);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void dodgeTest2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O B O O
        1        O O O B B W
        2       O O O B O O O
        3      O O O O O O O O
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        mm.setTraTableActive(true);
        game.init(aiPlayer, player, tokenLayout);
        game.getBoard().printBoard();
        mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 4);
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 4);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }
}
