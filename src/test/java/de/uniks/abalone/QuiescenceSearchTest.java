package de.uniks.abalone;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.uniks.abalone.model.AIPlayer;
import de.uniks.abalone.model.Board;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Logic;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;
import de.uniks.ai.AlphaBeta;
import de.uniks.ai.MiniMax;

public class QuiescenceSearchTest {
    @Test
    public void minimax_DontKickToken_depth1() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W 
        4     O O O O O O O W O
        5      O O O O O O W O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 0, 2, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        game.init(aiPlayer, player, tokenLayout);
        Token token1 = new Token().withGame(game).withPlayer(player);
        Token token2 = new Token().withGame(game).withPlayer(player);
        Token token3 = player.getTokens().get(0);
        token3.getField().getTopRight().withToken(token1);
        token3.getField().getDownLeft().withToken(token2);
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 1);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertTrue(token1.getField() != null);
    }

    @Test
    public void alphaBeta_DontKickToken_depth1() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W 
        4     O O O O O O O W O
        5      O O O O O O W O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 0, 2, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> mm = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        Token token1 = new Token().withGame(game).withPlayer(player);
        Token token2 = new Token().withGame(game).withPlayer(player);
        Token token3 = player.getTokens().get(0);
        token3.getField().getTopRight().withToken(token1);
        token3.getField().getDownLeft().withToken(token2);
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 1);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertTrue(token1.getField() != null);
    }

    @Test
    public void minimax_kickToken_depth1() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O B B B W 
        4     O O O O O O O W O
        5      O O O O O O W O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 0, 2, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        game.init(aiPlayer, player, tokenLayout);
        Token token1 = new Token().withGame(game).withPlayer(player);
        Token token2 = new Token().withGame(game).withPlayer(player);
        Token token3 = player.getTokens().get(0);
        token3.getField().getTopRight().withToken(token1);
        token3.getField().getDownLeft().withToken(token2);
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 1);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertTrue(token1.getField() == null);
    }

    @Test
    public void alphaBeta_kickToken_depth1() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W 
        4     O O O O O O O W O
        5      O O O O O O W O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 0, 2, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> mm = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        Token token1 = new Token().withGame(game).withPlayer(player);
        Token token2 = new Token().withGame(game).withPlayer(player);
        Token token3 = player.getTokens().get(0);
        token3.getField().getTopRight().withToken(token1);
        token3.getField().getDownLeft().withToken(token2);
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 1);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertTrue(token1.getField() == null);

    }

    @Test
    public void minimax_DontKickToken_new_depth1() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O B B B W 
        4     O O O O O O O W O
        5      O O O O O O W O 8
        6       O O O O O W O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 0, 2, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<>();
        game.init(aiPlayer, player, tokenLayout);
        Token token1 = new Token().withGame(game).withPlayer(player);
        Token token2 = new Token().withGame(game).withPlayer(player);
        Token token4 = new Token().withGame(game).withPlayer(player);
        Token token3 = player.getTokens().get(0);
        token3.getField().getTopRight().withToken(token1);
        token3.getField().getDownLeft().withToken(token2);
        token2.getField().getDownLeft().withToken(token4);
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 1);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertTrue(token1.getField() != null);
    }

    @Test
    public void alphaBeta_DontKickToken_new_depth1() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O B B B W 
        4     O O O O O O O W O
        5      O O O O O O W O 8
        6       O O O O O W O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 0, 2, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> mm = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        Token token1 = new Token().withGame(game).withPlayer(player);
        Token token2 = new Token().withGame(game).withPlayer(player);
        Token token4 = new Token().withGame(game).withPlayer(player);
        Token token3 = player.getTokens().get(0);
        token3.getField().getTopRight().withToken(token1);
        token3.getField().getDownLeft().withToken(token2);
        token2.getField().getDownLeft().withToken(token4);
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 1);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertTrue(token1.getField() != null);
    }
}
