package de.uniks.abalone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.uniks.abalone.model.AIPlayer;
import de.uniks.abalone.model.Board;
import de.uniks.abalone.model.Game;
import de.uniks.abalone.model.Logic;
import de.uniks.abalone.model.Move;
import de.uniks.abalone.model.Player;
import de.uniks.abalone.model.Token;
import de.uniks.ai.AlphaBeta;

public class AlphaBetaGenericTest {
    @Test
    public void alphaBetaCheckWinnerTest_depth1() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> ab = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        Move bestMove = ab.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 1);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));
    }

    @Test
    public void alphaBetaCheckWinnerTest_depth2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> mm = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 2);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));
    }

    @Test
    public void alphaBetaCheckWinnerTest_depth3() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> mm = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));
    }

    @Test
    public void alphaBetaCheckWinnerTest_depth4() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> mm = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 4);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));
    }

    @Test
    public void alphaBetaDodgeTest_depth1() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O O O $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> mm = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 1);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        boolean directionIs3Or2 = bestMove.getDirection() == 2 || bestMove.getDirection() == 3;
        assertTrue(directionIs3Or2);
    }

    @Test
    public void alphaBetaDodgeTest_depth2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O O O $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> mm = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 2);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void alphaBetaDodgeTest_depth3() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O O O $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> mm = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void alphaBetaDodgeTest_depth4() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O O O $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> mm = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 4);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void alphaBetaDelayLoseTest_depth2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O B B $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> mm = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 2);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void alphaBetaDelayLoseTest_depth3() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O B B $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> mm = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 3);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void alphaBetaDelayLoseTest_depth4() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O B B O
        3      O O O O O B B W
        4     O O O O O B B $ O
        5      O O O O O O O B 8
        6       O O O O O O B 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> mm = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        player.withTokens(new Token(), new Token(), new Token(), new Token(), new Token());
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 4);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(3, bestMove.getDirection());
    }

    @Test
    public void alphaBetaDodgeTest2() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     expectedTarget = $
        --------------------------------------------
        0         O O B O O
        1        O O O B B W
        2       O O O B O O O
        3      O O O O O O O O
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> mm = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        game.getBoard().printBoard();
        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 4);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        // assertEquals(aiPlayer, game.getLogic().checkWinner(game.getBoard()));
    }

    @Test
    public void alphaBetaSomeTest() {
        /*--------------------------------------------
        --    row = y       dia = x      a[y][x]     targetField = $
        --------------------------------------------
        0         O O O O O
        1        O O O O O O
        2       O O O O O O O
        3      O O O O O B B W $
        4     O O O O O O O O O
        5      O O O O O O O O 8
        6       O O O O O O O 7
        7        O O O O O O 6
        8         O O O O O 5
                   0 1 2 3 4
        --------------------------------------------
        */
        int[][] tokenLayout = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 2},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {2, 2, 2, 0, 0, 0, 0, 0, 0}};
        AIPlayer aiPlayer = (AIPlayer) new AIPlayer().withColor("black").withName("Alice");
        Player player = new Player().withColor("white").withName("Bob");
        Game game = new Game();
        AlphaBeta<Logic, Board, Move, Player> mm = new AlphaBeta<>();
        game.init(aiPlayer, player, tokenLayout);
        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 2);
        game.getLogic().executeMove(bestMove);
        game.getBoard().printBoard();
        assertEquals(null, game.getPlayers().get(1).getTokens().get(0).getField());
    }

}
