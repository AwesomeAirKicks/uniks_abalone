package de.uniks.tictactoe;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import de.uniks.ai.AlphaBeta;
import de.uniks.ai.MiniMax;
import de.uniks.tictactoe.model.AIPlayer;
import de.uniks.tictactoe.model.Board;
import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Logic;
import de.uniks.tictactoe.model.Move;
import de.uniks.tictactoe.model.Player;

public class CompareMinimaxAlphabetaTTTTest {
    @Test
    public void CompareMmAbTest() {
        /*
         * AI = X Player = O start config: | | | X | | X | X | O | | O | | O | Expected result: | | | X | | X | X | O |
         * | O | X | O |
         */

        int depth = 3;

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        AIPlayer aiPlayer = new AIPlayer();
        Player player = new Player();

        game.init(aiPlayer, player);

        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<Logic, Board, Move, Player>();
        AlphaBeta<Logic, Board, Move, Player> ab = new AlphaBeta<Logic, Board, Move, Player>();

        Move move1 = new Move().withField(game.getBoard().getFields().get(2)).withPlayer(aiPlayer);
        Move move2 = new Move().withField(game.getBoard().getFields().get(3)).withPlayer(aiPlayer);
        Move move3 = new Move().withField(game.getBoard().getFields().get(4)).withPlayer(aiPlayer);
        Move move4 = new Move().withField(game.getBoard().getFields().get(5)).withPlayer(player);
        Move move5 = new Move().withField(game.getBoard().getFields().get(6)).withPlayer(player);
        Move move6 = new Move().withField(game.getBoard().getFields().get(8)).withPlayer(player);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);
        logic.executeMove(move4);
        logic.executeMove(move5);
        logic.executeMove(move6);

        Move bestMMMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), depth);
        Move bestABMove = ab.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), depth);

        assertNotNull(bestMMMove);
        assertNotNull(bestABMove);

        // Get the first move of the strategy
        Move mMMove = (Move) mm.getBestStrategy().getMoveList().get(0);
        Move aBMove = (Move) ab.getBestStrategy().getMoveList().get(0);

        // Compare the score of both strategies
        assertEquals(mm.getBestStrategy().getScore(), ab.getBestStrategy().getScore());

        // Compare the field of the first move of the strategy list, which is the "best" move
        assertEquals(mMMove.getField().getX(), aBMove.getField().getX());
        assertEquals(mMMove.getField().getY(), aBMove.getField().getY());

        // Get the second move of the strategy
        mMMove = (Move) mm.getBestStrategy().getMoveList().get(1);
        aBMove = (Move) ab.getBestStrategy().getMoveList().get(1);

        // Compare
        assertEquals(mMMove.getField().getX(), aBMove.getField().getX());
        assertEquals(mMMove.getField().getY(), aBMove.getField().getY());

        // Get the third move of the strategy
        mMMove = (Move) mm.getBestStrategy().getMoveList().get(2);
        aBMove = (Move) ab.getBestStrategy().getMoveList().get(2);

        // Compare
        assertEquals(mMMove.getField().getX(), aBMove.getField().getX());
        assertEquals(mMMove.getField().getY(), aBMove.getField().getY());
    }
}
