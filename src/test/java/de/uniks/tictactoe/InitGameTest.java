package de.uniks.tictactoe;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Player;

/**
 * @author Thorsten Otto
 * @date 16.04.2017
 */

public class InitGameTest {
    @Test
    public void createPlayerTest() {

        Game game = new Game();

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        assertEquals(2, game.getPlayers().size());

        assertEquals(player1, game.getPlayers().get(0));
        assertEquals(player2, game.getPlayers().get(1));

    }

    @Test
    public void currentPlayerTest() {

        Game game = new Game();

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        // currentPlayer
        assertEquals(player1, game.getCurrentPlayer());

    }

    @Test
    public void nextPlayerTest() {

        Game game = new Game();

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        assertEquals(player2, game.getCurrentPlayer().getNext());

    }

    @Test
    public void buildFieldsTest() {

        Game game = new Game();

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        // the board has 3x3 = 9 fields
        assertEquals(9, game.getBoard().getFields().size());

    }

}
