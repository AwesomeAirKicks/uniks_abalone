package de.uniks.tictactoe;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Logic;
import de.uniks.tictactoe.model.Move;
import de.uniks.tictactoe.model.Player;

/**
 * @author Thorsten Otto
 * @date 18.04.2017
 */

public class CheckWinnerTest {

    // 0 1 2
    // |O|X|X| 0
    // | | | | 1
    // | | | | 2

    @Test
    public void checkWinnerTestNoWinner() {

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move move1 = new Move().withField(game.getBoard().getFields().get(0)).withPlayer(player2);
        Move move2 = new Move().withField(game.getBoard().getFields().get(1)).withPlayer(player1);
        Move move3 = new Move().withField(game.getBoard().getFields().get(2)).withPlayer(player1);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);

        assertEquals(null, logic.checkWinner(game.getBoard()));

    }

    // 0 1 2
    // |X|X|X| 0
    // | | | | 1
    // | | | | 2

    @Test
    public void checkWinnerTestHorizontalZeroLine() {

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move move1 = new Move().withField(game.getBoard().getFields().get(0)).withPlayer(player1);
        Move move2 = new Move().withField(game.getBoard().getFields().get(1)).withPlayer(player1);
        Move move3 = new Move().withField(game.getBoard().getFields().get(2)).withPlayer(player1);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);

        assertEquals(player1, logic.checkWinner(game.getBoard()));

    }

    // 0 1 2
    // |O|O| | 0
    // |X|X|X| 1
    // | | | | 2

    @Test
    public void checkWinnerTestHorizontalOneLine() {

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move move1 = new Move().withField(game.getBoard().getFields().get(0)).withPlayer(player2);
        Move move2 = new Move().withField(game.getBoard().getFields().get(1)).withPlayer(player2);

        Move move3 = new Move().withField(game.getBoard().getFields().get(3)).withPlayer(player1);
        Move move4 = new Move().withField(game.getBoard().getFields().get(4)).withPlayer(player1);
        Move move5 = new Move().withField(game.getBoard().getFields().get(5)).withPlayer(player1);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);
        logic.executeMove(move4);
        logic.executeMove(move5);

        assertEquals(player1, logic.checkWinner(game.getBoard()));

    }

    // 0 1 2
    // |O|O| | 0
    // | | | | 1
    // |X|X|X| 2

    @Test
    public void checkWinnerTestHorizontalTwoLine() {

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move move1 = new Move().withField(game.getBoard().getFields().get(0)).withPlayer(player2);
        Move move2 = new Move().withField(game.getBoard().getFields().get(1)).withPlayer(player2);

        Move move3 = new Move().withField(game.getBoard().getFields().get(6)).withPlayer(player1);
        Move move4 = new Move().withField(game.getBoard().getFields().get(7)).withPlayer(player1);
        Move move5 = new Move().withField(game.getBoard().getFields().get(8)).withPlayer(player1);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);
        logic.executeMove(move4);
        logic.executeMove(move5);

        assertEquals(player1, logic.checkWinner(game.getBoard()));

    }

    // 0 1 2
    // |X|O|O| 0
    // |X| | | 1
    // |X| | | 2

    @Test
    public void checkWinnerTestVerticalZeroLine() {

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move move1 = new Move().withField(game.getBoard().getFields().get(1)).withPlayer(player2);
        Move move2 = new Move().withField(game.getBoard().getFields().get(2)).withPlayer(player2);

        Move move3 = new Move().withField(game.getBoard().getFields().get(0)).withPlayer(player1);
        Move move4 = new Move().withField(game.getBoard().getFields().get(3)).withPlayer(player1);
        Move move5 = new Move().withField(game.getBoard().getFields().get(6)).withPlayer(player1);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);
        logic.executeMove(move4);
        logic.executeMove(move5);

        assertEquals(player1, logic.checkWinner(game.getBoard()));

    }

    // 0 1 2
    // |O|X|O| 0
    // | |X| | 1
    // | |X| | 2

    @Test
    public void checkWinnerTestVerticalOneLine() {

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move move1 = new Move().withField(game.getBoard().getFields().get(0)).withPlayer(player2);
        Move move2 = new Move().withField(game.getBoard().getFields().get(2)).withPlayer(player2);

        Move move3 = new Move().withField(game.getBoard().getFields().get(1)).withPlayer(player1);
        Move move4 = new Move().withField(game.getBoard().getFields().get(4)).withPlayer(player1);
        Move move5 = new Move().withField(game.getBoard().getFields().get(7)).withPlayer(player1);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);
        logic.executeMove(move4);
        logic.executeMove(move5);

        assertEquals(player1, logic.checkWinner(game.getBoard()));

    }

    // 0 1 2
    // |O|X|O| 0
    // | |X| | 1
    // | |X| | 2

    @Test
    public void checkWinnerTestVerticalTwoLine() {

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move move1 = new Move().withField(game.getBoard().getFields().get(0)).withPlayer(player2);
        Move move2 = new Move().withField(game.getBoard().getFields().get(2)).withPlayer(player2);

        Move move3 = new Move().withField(game.getBoard().getFields().get(1)).withPlayer(player1);
        Move move4 = new Move().withField(game.getBoard().getFields().get(4)).withPlayer(player1);
        Move move5 = new Move().withField(game.getBoard().getFields().get(7)).withPlayer(player1);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);
        logic.executeMove(move4);
        logic.executeMove(move5);

        assertEquals(player1, logic.checkWinner(game.getBoard()));

    }

    // 0 1 2
    // |X|O|O| 0
    // | |X| | 1
    // | | |X| 2

    @Test
    public void checkWinnerTestDiagonallyOne() {

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move move1 = new Move().withField(game.getBoard().getFields().get(1)).withPlayer(player2);
        Move move2 = new Move().withField(game.getBoard().getFields().get(2)).withPlayer(player2);

        Move move3 = new Move().withField(game.getBoard().getFields().get(0)).withPlayer(player1);
        Move move4 = new Move().withField(game.getBoard().getFields().get(4)).withPlayer(player1);
        Move move5 = new Move().withField(game.getBoard().getFields().get(8)).withPlayer(player1);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);
        logic.executeMove(move4);
        logic.executeMove(move5);

        assertEquals(player1, logic.checkWinner(game.getBoard()));

    }

    // 0 1 2
    // |O|O|X| 0
    // | |X| | 1
    // |X| | | 2

    @Test
    public void checkWinnerTestDiagonallyTwo() {

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move move1 = new Move().withField(game.getBoard().getFields().get(0)).withPlayer(player2);
        Move move2 = new Move().withField(game.getBoard().getFields().get(1)).withPlayer(player2);

        Move move3 = new Move().withField(game.getBoard().getFields().get(2)).withPlayer(player1);
        Move move4 = new Move().withField(game.getBoard().getFields().get(4)).withPlayer(player1);
        Move move5 = new Move().withField(game.getBoard().getFields().get(6)).withPlayer(player1);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);
        logic.executeMove(move4);
        logic.executeMove(move5);

        assertEquals(player1, logic.checkWinner(game.getBoard()));

    }

}
