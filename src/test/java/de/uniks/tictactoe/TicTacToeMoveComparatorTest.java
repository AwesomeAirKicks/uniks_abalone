package de.uniks.tictactoe;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Test;

import de.uniks.tictactoe.model.AIPlayer;
import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Logic;
import de.uniks.tictactoe.model.Move;
import de.uniks.tictactoe.model.Player;

public class TicTacToeMoveComparatorTest {

    @Test
    public void ticTacToeMoveComparatorTest() {
        /*
         * AI = X Player = O start config: | | X | X | | X | | O | | O | | O |
         */

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        AIPlayer aiPlayer = new AIPlayer();
        Player player = new Player();

        game.init(aiPlayer, player);

        Move move1 = new Move().withField(game.getBoard().getFields().get(1)).withPlayer(aiPlayer);
        Move move2 = new Move().withField(game.getBoard().getFields().get(2)).withPlayer(aiPlayer);
        Move move3 = new Move().withField(game.getBoard().getFields().get(3)).withPlayer(aiPlayer);
        Move move4 = new Move().withField(game.getBoard().getFields().get(5)).withPlayer(player);
        Move move5 = new Move().withField(game.getBoard().getFields().get(6)).withPlayer(player);
        Move move6 = new Move().withField(game.getBoard().getFields().get(8)).withPlayer(player);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);
        logic.executeMove(move4);
        logic.executeMove(move5);
        logic.executeMove(move6);

        ArrayList<Move> allMoves = logic.getAllMoves(aiPlayer, game.getBoard());

        // @SuppressWarnings("rawtypes")
        // MoveComparator ticTacToeMoveComparator = new MoveComparator();
        // Collections.sort(allMoves, ticTacToeMoveComparator);

        Collections.sort(allMoves, (m1, m2) -> m1.compareTo(m2));

        assertEquals(allMoves.get(0).getField().getX(), 1);
        assertEquals(allMoves.get(0).getField().getY(), 1);

        assertEquals(allMoves.get(1).getField().getX(), 0);
        assertEquals(allMoves.get(1).getField().getY(), 0);

        assertEquals(allMoves.get(2).getField().getX(), 2);
        assertEquals(allMoves.get(2).getField().getY(), 1);
    }
}
