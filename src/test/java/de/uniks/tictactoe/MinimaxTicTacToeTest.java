package de.uniks.tictactoe;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.uniks.ai.MiniMax;
import de.uniks.tictactoe.model.AIPlayer;
import de.uniks.tictactoe.model.Board;
import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Logic;
import de.uniks.tictactoe.model.Move;
import de.uniks.tictactoe.model.Player;

/**
 * @author Thorsten Otto
 * @date 26.04.2017
 */

public class MinimaxTicTacToeTest {
    @Test
    public void player1WinTest() {

        /*
         * AI = X Player = O Startconfig: | O | | | | | O | | | X | X | | Expected result: | O | | | | | O | | | X | X |
         * X |
         */

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<Logic, Board, Move, Player>();

        AIPlayer aiPlayer = new AIPlayer();
        Player player = new Player();

        game.init(aiPlayer, player);

        Move move1 = new Move().withField(game.getBoard().getFields().get(0)).withPlayer(player);
        Move move4 = new Move().withField(game.getBoard().getFields().get(4)).withPlayer(player);
        Move move2 = new Move().withField(game.getBoard().getFields().get(6)).withPlayer(aiPlayer);
        Move move3 = new Move().withField(game.getBoard().getFields().get(7)).withPlayer(aiPlayer);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);
        logic.executeMove(move4);

        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), 1);

        assertEquals(2, bestMove.getField().getX());
        assertEquals(2, bestMove.getField().getY());
    }

    @Test
    public void player2WinTest() {

        /*
         * AI = X Player = O start config: | O | | | | | O | | | X | X | | Expected result: | O | | | | | O | | | X | X
         * | O |
         */

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<Logic, Board, Move, Player>();

        AIPlayer aiPlayer = new AIPlayer();
        Player player = new Player();

        game.init(aiPlayer, player);

        Move move1 = new Move().withField(game.getBoard().getFields().get(0)).withPlayer(player);
        Move move4 = new Move().withField(game.getBoard().getFields().get(4)).withPlayer(player);
        Move move2 = new Move().withField(game.getBoard().getFields().get(6)).withPlayer(aiPlayer);
        Move move3 = new Move().withField(game.getBoard().getFields().get(7)).withPlayer(aiPlayer);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);
        logic.executeMove(move4);

        Move bestMove = mm.calculateBestMove(game.getLogic(), player, aiPlayer, game.getBoard(), 1);

        assertEquals(2, bestMove.getField().getX());
        assertEquals(2, bestMove.getField().getY());
    }

    @Test
    public void preventLoseDepth1Test() {
        /*
         * AI = X Player = O start config: | | | X | | X | X | O | | O | | O | Expected result: | X | | X | | X | X | O
         * | | O | | O |
         */

        int depth = 1;

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<Logic, Board, Move, Player>();

        AIPlayer aiPlayer = new AIPlayer();
        Player player = new Player();

        game.init(aiPlayer, player);

        Move move1 = new Move().withField(game.getBoard().getFields().get(2)).withPlayer(aiPlayer);
        Move move2 = new Move().withField(game.getBoard().getFields().get(3)).withPlayer(aiPlayer);
        Move move3 = new Move().withField(game.getBoard().getFields().get(4)).withPlayer(aiPlayer);
        Move move4 = new Move().withField(game.getBoard().getFields().get(5)).withPlayer(player);
        Move move5 = new Move().withField(game.getBoard().getFields().get(6)).withPlayer(player);
        Move move6 = new Move().withField(game.getBoard().getFields().get(8)).withPlayer(player);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);
        logic.executeMove(move4);
        logic.executeMove(move5);
        logic.executeMove(move6);

        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), depth);

        assertEquals(0, bestMove.getField().getX());
        assertEquals(0, bestMove.getField().getY());
    }

    @Test
    public void preventLoseDepth2Test() {
        /*
         * AI = X Player = O start config: | | | X | | X | X | O | | O | | O | Expected result: | | | X | | X | X | O |
         * | O | X | O |
         */

        int depth = 2;

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<Logic, Board, Move, Player>();

        AIPlayer aiPlayer = new AIPlayer();
        Player player = new Player();

        game.init(aiPlayer, player);

        Move move1 = new Move().withField(game.getBoard().getFields().get(2)).withPlayer(aiPlayer);
        Move move2 = new Move().withField(game.getBoard().getFields().get(3)).withPlayer(aiPlayer);
        Move move3 = new Move().withField(game.getBoard().getFields().get(4)).withPlayer(aiPlayer);
        Move move4 = new Move().withField(game.getBoard().getFields().get(5)).withPlayer(player);
        Move move5 = new Move().withField(game.getBoard().getFields().get(6)).withPlayer(player);
        Move move6 = new Move().withField(game.getBoard().getFields().get(8)).withPlayer(player);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);
        logic.executeMove(move4);
        logic.executeMove(move5);
        logic.executeMove(move6);

        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), depth);

        assertEquals(2, bestMove.getField().getX());
        assertEquals(1, bestMove.getField().getY());
    }

    @Test
    public void player1PreventLoseDepth3Test() {

        /*
         * AI = X Player = O start config: | | O | | | | O | | | X | | | Expected result: | | O | | | | O | | | X | X |
         * |
         */

        int depth = 3;

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        MiniMax<Logic, Board, Move, Player> mm = new MiniMax<Logic, Board, Move, Player>();

        AIPlayer aiPlayer = new AIPlayer();
        Player player = new Player();

        game.init(aiPlayer, player);

        Move move1 = new Move().withField(game.getBoard().getFields().get(1)).withPlayer(player);
        Move move2 = new Move().withField(game.getBoard().getFields().get(4)).withPlayer(player);
        Move move3 = new Move().withField(game.getBoard().getFields().get(6)).withPlayer(aiPlayer);

        logic.executeMove(move1);
        logic.executeMove(move2);
        logic.executeMove(move3);

        Move bestMove = mm.calculateBestMove(game.getLogic(), aiPlayer, player, game.getBoard(), depth);

        assertEquals(2, bestMove.getField().getX());
        assertEquals(1, bestMove.getField().getY());
    }
}
