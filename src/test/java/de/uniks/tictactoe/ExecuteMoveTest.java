package de.uniks.tictactoe;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import de.uniks.tictactoe.model.Game;
import de.uniks.tictactoe.model.Logic;
import de.uniks.tictactoe.model.Move;
import de.uniks.tictactoe.model.Player;

/**
 * @author Thorsten Otto
 * @date 18.04.2017
 */

public class ExecuteMoveTest {

    @Test
    public void executeMoveTest() {

        Game game = new Game();

        Logic logic = new Logic();
        logic.withGame(game);

        Player player1 = new Player().withName("Alice").withSymbole("X").withScore(0);
        Player player2 = new Player().withName("Bob").withSymbole("O").withScore(0);

        game.init(player1, player2);

        Move move = new Move();
        move.withField(game.getBoard().getFields().get(0));
        move.withPlayer(player1);

        logic.executeMove(move);

        assertNotEquals(null, game.getBoard().getFields().get(0).getToken());
        assertEquals(player1, game.getBoard().getFields().get(0).getToken().getPlayer());
    }

}
