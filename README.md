# README #
Dieses Projekt ist Bestandteil des Bachelor-Studiengangs "Informatik" an der Universität Kassel und wird im Fachgebiet "Programmiersprachen/-methodik" von Prof. Dr. Fohry absolviert. Im Mittelpunkt stehen die Programmierung des Spiels *Abalone*, sowie die Entwicklung einer KI, die gegen einen Spieler antreten kann.

### Teilnehmer ###
* Alexander Hirsch
* Thorsten Otto
* Olaf Versteeg

### Betreuer ###
* M.Sc. Marco Bungart

### Projektumfang ###
* Grafische Oberfläche des Spielfeldes
* Spiellogik 
* KI
* Spielmodus "Spieler vs. Spieler"
* Spielmodus "Spieler vs. KI"
* Vollständiger JUnit-Test

### Abalone-Spielregeln ###
Abalone ist ein Strategiespiel für zwei (in anderen Varianten bis zu 6) Spieler. Gespielt wird auf einem sechseckigen Spielfeld mit 61 Löchern. Die Spieler ziehen abwechselnd mit ihren weißen bzw. schwarzen Kugeln und versuchen, die gegnerischen Kugeln vom Spielfeld zu stoßen. Wer zuerst 6 gegnerische Kugeln vom Brett geschoben, oder seinen Gegner so blockiert hat, dass er keine Züge mehr ausführen kann, hat gewonnen.

**Startaufstellung:** In der Standardgrundstellung befinden sich alle 14 Kugeln eines Spielers auf der ihm zugewandten Seite des Spielfeldes. In der untersten Reihe liegen 5, in der darüber liegenden Reihe 6 und in der dritten Reihe mittig die verbleibenden 3 Kugeln. In der sog. *Belgian Daisy*-Stellung befinden sich vor jedem Spieler jeweils 7 eigene und 7 gegnerische Kugeln. Diese liegen als sechseckige Formation (eine Kugel in der Mitte, sechs weitere außen herum) in der linken bzw. rechten unteren Ecke des Spielfeldes. Anders als beim Schach, beginnt Schwarz das Spiel.

**Gültige Züge:** Grundsätzlich dürfen in einem Zug 1 bis 3 eigene Kugeln in eine der bis zu 6 möglichen Richtungen bewegt werden. Die Kugeln müssen jedoch in einer zusammenhängen Reihe liegen. Wenn die Kugeln parallel in eine benachbarte Reihe versetzt werden sollen, müssen alle Zielfelder frei sein. Beim Verschieben in eine der beiden Richtungen entlang der Kugelreihe können auch gegnerische Kugeln mit verschoben werden. Dies ist jedoch nur möglich, wenn die eigenen Kugeln in der Überzahl sind. Eine Kugel alleine kann keine gegnerische Kugel bewegen. Mit zwei eigenen Kugeln kann nur eine und mit drei eigenen Kugeln können eine oder zwei gegnerische Kugeln bewegt werden. Diese Zugvariante ist also die einzige Möglichkeit, gegnerische Kugeln über den Rand zu schieben.